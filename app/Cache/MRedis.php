<?php namespace App\Cache;
use App\Models\User;
use App\Models\Content;
use App\Models\Program;
use App\Models\Topic;
use App\Models\Comment;
use Log;
class MRedis extends MRedisLib{
    
    
    public $TABLE = array(
        'user'=>'USER',
        'content'=>'CONTENT',
        'comment'=>'COMMENT',
        'topic'=>'TOPIC',
        'program'=>'PROGRAM',
    );

    /**
     * 用户点赞内容UPC
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P 内容创作作者X
     * @param $action  add添加、delete删除、deleteAll删除全部
     * @param int  $userId     用户id
     * @param int  $contentId  内容id
     * @return bool
     */
    public function userPraiseContent($action,$userId,$contentId){
        //存redis
        return $this->setHandle($action,'UPC',array('UID'=>$userId),array('CID'=>$contentId));
    }
    
    /**
     * 用户点赞评论UPW
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P 内容创作作者X
     * @param $action  add添加、delete删除、deleteAll删除全部
     * @param int  $userId     用户id
     * @param int  $commentId  评论id
     * @return bool
     */
    public function userPraiseComment($action,$userId,$commentId){
       //存redis
       return $this->setHandle($action,'UPW',array('UID'=>$userId),array('WID'=>$commentId));
    }
    
    /**
     * 用户收藏内容UFC
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param $action  add 添加、delete 删除、deleteAll 删除全部
     * @param int  $userId     用户id
     * @param int  $contentId  内容id
     * @return bool
     */
    public function userFavoriteContent($action,$userId,$contentId){
       return $this->setHandle($action,'UFC',array('UID'=>$userId),array('CID'=>$contentId));
    }
    
    /**
     * 用户关注人UAU
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param $action  add添加、delete删除、deleteAll删除全部
     * @param int  $userId     用户id
     * @param int  $uId        用户id
     * @return bool
     */
    public function userFollowUser($action,$userId,$uId){
       return $this->setHandle($action,'UAX',array('UID'=>$userId),array('XID'=>$uId));
    }
    
    /**
     * 用户关注话题UAT
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param $action  add添加、delete删除、deleteAll删除全部
     * @param int  $userId     用户id
     * @param int  $topicId    话题id
     * @return bool
     */
    public function userAttentionTopic($action,$userId,$topicId){
       return $this->setHandle($action,'UAT',array('UID'=>$userId),array('TID'=>$topicId));
    }
    
    /**
     * 用户关注节目视频UAV
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param $action  add添加、delete删除、deleteAll删除全部
     * @param int  $userId     用户id
     * @param int  $programId  节目id
     * @return bool
     */
    public function userAttentionProgram($action,$userId,$programId){
       return $this->setHandle($action,'UAV',array('UID'=>$userId),array('VID'=>$programId));
    }
    
    /**
     * 用户评论内容UWC
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param $action  add添加、delete删除、deleteAll删除全部
     * @param int  $userId     用户id
     * @param int  $contentId  内容id
     * @param int  $commentId  评论id
     * @return bool
     */
    public function userCommentContent($action,$userId,$contentId,$comment_Id){
       return $this->setHandle($action,'UWC',array('UID'=>$userId),array('CID'=>$contentId),$comment_Id);
    }
    
    /**
     * 用户评论评论UWW
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param $action  add添加、delete删除、deleteAll删除全部
     * @param int  $userId     用户id
     * @param int  $commentId  评论id
     * @param int  $comment_Id  评论评论id
     * @return bool
     */
    public function userCommentComment($action,$userId,$commentId,$comment_Id){
       return $this->setHandle($action,'UWW',array('UID'=>$userId),array('WID'=>$commentId),$comment_Id);
    }
    
    /**
     * 获取用户点赞内容数组(我点赞过的内容)
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $userId     用户id
     * @param int  $page       页面从0开始
     * @param int  $size       每页数量
     * @return array 内容id数组
     */
    public function getPraiseContentIdsByUserId($userId,$page=0,$size=10,$order='desc'){
       return $this->getListHandle('UPC',array('UID'=>$userId),$page,$size,$order);
    }
    
    /**
     * 获取点赞内容下的用户数组(内容下所有点赞的用户)
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $contentId     内容id
     * @param int  $page       页面从0开始
     * @param int  $size       每页数量
     * @return array 用户id数组
     */
    public function getPraiseUserIdsBycontentId($contentId,$page=0,$size=10,$order='desc'){
        return $this->getListHandle('UPC',array('CID'=>$contentId),$page,$size,$order);
    }
    
    /**
     * 获取内容点赞用户总数(内容点赞总数)
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $contentId  内容id
     * @return int 
     */
    public function getContentPraiseUserTotal($contentId){
       return $this->getTotalHandle('UPC',array('CID'=>$contentId));
    }
    
    /**
     * 获取用户点赞内容总数
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $userId     用户id
     * @return bool
     */
    public function getUserPraiseContentTotal($userId){
       return $this->getTotalHandle('UPC',array('UID'=>$userId));
    }
    
    /**
     * 获取用户点赞评论数组(我点赞过的评论(包括二级评论))userId_commentId=>time()
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $userId     用户id
     * @param int  $page       页面从0开始
     * @param int  $size       每页数量
     * @return array 评论id数组
     */
    public function getPraiseCommentIdsByUserId($userId,$page=0,$size=10,$order='desc'){
       return $this->getListHandle('UPW',array('UID'=>$userId),$page,$size,$order);
    }
    
    /**
     * 获取评论点赞的用户数组(某评论点赞的用户)
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $commentId   评论id
     * @param int  $page       页面从0开始
     * @param int  $size       每页数量
     * @return array 用户id数组
     */
    public function getPraiseUserIdsByCommentId($commentId,$page=0,$size=10,$order='desc'){
       return $this->getListHandle('UPW',array('WID'=>$commentId),$page,$size,$order);
    }
    
    /**
     * 获取评论点赞总数
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $commentId  评论id
     * @return int 
     */
    public function getCommentPraiseUserTotal($commentId){
       return $this->getTotalHandle('UPW',array('WID'=>$commentId));
    }
    
    /**
     * 获取用户点赞评论总数
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $userId     用户id
     * @return int
     */
    public function getUserPraiseCommentTotal($userId){
       return $this->getTotalHandle('UPW',array('UID'=>$userId));
    }
    
    
    /**
     * 获取用户收藏的内容的数组
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $userId     用户id
     * @param int  $page       页面从0开始
     * @param int  $size       每页数量
     * @return array 收藏内容id数组
     */
    public function getFavoriteContentIdsByUserId($userId,$page=0,$size=10,$order='desc'){
       return $this->getListHandle('UFC',array('UID'=>$userId),$page,$size,$order);
    }
    
    /**
     * 获取内容的收藏用户id数组
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $contentId  内容id
     * @param int  $page       页面从0开始
     * @param int  $size       每页数量
     * @return array 用户id数组
     */
    public function getFavoriteUserIdsByContentId($contentId,$page=0,$size=10,$order='desc'){
       return $this->getListHandle('UFC',array('CID'=>$contentId),$page,$size,$order);
    }
    
    /**
     * 获取内容被收藏总数
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $contentId  内容id
     * @return int 
     */
    public function getFavoriteUserTotal($contentId){
       return $this->getTotalHandle('UFC',array('CID'=>$contentId));
    }
    
    /**
     * 获取用户收藏的内容总数
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $userId     用户id
     * @return int
     */
    public function getFavoriteContentTotal($userId){
       return $this->getTotalHandle('UFC',array('UID'=>$userId));
    }
    
    /**
     * 获取用户关注的人的Id数组
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $userId     用户id
     * @param int  $page       页面从0开始
     * @param int  $size       每页数量
     * @return array 被关注人id数组
     */
    public function getFollowerByUserId($userId,$page=0,$size=10,$order='desc'){
       return $this->getListHandle('UAX',array('UID'=>$userId),$page,$size,$order);
    }
    
    /**
     * 获取用户粉丝id数组
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $userId     用户id
     * @param int  $page       页面从0开始
     * @param int  $size       每页数量
     * @return array 粉丝id数组
     */
    public function getFansByUserId($userId,$page=0,$size=10,$order='desc'){
       return $this->getListHandle('UAX',array('XID'=>$userId),$page,$size,$order);
    }
    
    /**
     * 获取用户的关注(人)数
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $userId     用户id
     * @return int 
     */
    public function getFollowerTotal($userId){
       return $this->getTotalHandle('UAX',array('UID'=>$userId));
    }
    
    /**
     * 获取用户的粉丝数
     * 
     * @param int  $userId     用户id
     * @return int
     */
    public function getFansTotal($userId){
       return $this->getTotalHandle('UAX',array('XID'=>$userId));
    }
    
    /**
     * 获取用户关注的话题id数组
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $userId     用户id
     * @param int  $page       页面从0开始
     * @param int  $size       每页数量
     * @return array 被关注话题id数组
     */
    public function getAttentionTopicIdsByUserId($userId,$page=0,$size=10,$order='desc'){
       return $this->getListHandle('UAT',array('UID'=>$userId),$page,$size,$order);
    }
    
    /**
     * 获取关注话题的用户id数组
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $topicId    话题id
     * @param int  $page       页面从0开始
     * @param int  $size       每页数量
     * @return array 用户id数组
     */
    public function getAttentionUserIdsByTopicId($topicId,$page=0,$size=10,$order='desc'){
       return $this->getListHandle('UAT',array('TID'=>$topicId),$page,$size,$order);
    }
    
    /**
     * 获取用户关注的话题id总数
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $userId     用户id
     * @return int 
     */
    public function getAttentionTopicTopicTotal($userId){
       return $this->getTotalHandle('UAT',array('UID'=>$userId));
    }
    
    /**
     * 获取关注话题的用户id总数
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $topicId    话题id
     * @return int
     */
    public function getAttentionTopicUserTotal($topicId){
        return $this->getTotalHandle('UAT',array('TID'=>$topicId));
    }
    
    /**
     * 获取用户关注的视频节目id数组
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $userId     用户id
     * @param int  $page       页面从0开始
     * @param int  $size       每页数量
     * @return array 被关注视频节目id数组
     */
    public function getAttentionProgramIdsByUserId($userId,$page=0,$size=10,$order='desc'){
       return $this->getListHandle('UAV',array('UID'=>$userId),$page,$size,$order);
    }
    
    /**
     * 获取关注视频节目的用户Id数组
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $programId    视频节目id
     * @param int  $page       页面从0开始
     * @param int  $size       每页数量
     * @return array 用户id数组
     */
    public function getAttentionUserIdsByProgramId($programId,$page=0,$size=10,$order='desc'){
       return $this->getListHandle('UAV',array('VID'=>$programId),$page,$size,$order);
    }
    
    /**
     * 获取用户关注的视频节目总数
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $userId     用户id
     * @return int 
     */
    public function getAttentionProgramProgramTotal($userId){
       return $this->getTotalHandle('UAV',array('UID'=>$userId));
    }
    
    /**
     * 获取关注视频节目的用户总数
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $programId    视频节目id
     * @return int
     */
    public function getAttentionProgramUserTotal($programId){
       return $this->getTotalHandle('UAV',array('VID'=>$programId));
    }
    
    //获取用户的评论内容id数组 contentId_commentId=>time
    /**其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * 获取用户评论内容数组
     * @param int  $userId     用户id
     * @param int  $page       页面从0开始
     * @param int  $size       每页数量
     * @return array 内容id数组
     */
    public function getCommentContentIdsByUserId($userId,$page=0,$size=10,$order='desc'){
       return $this->getListHandle('UWC',array('UID'=>$userId),$page,$size,$order);
    }
    
    /**
     * 获取评论内容的用户数组(内容下的所有评论的用户)userid_commentId => time()
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $contentId  内容id
     * @param int  $page       页面从0开始
     * @param int  $size       每页数量
     * @return array 用户id数组
     */
    public function getCommentUserIdsByContentId($contentId,$page=0,$size=10,$order='desc'){
       return $this->getListHandle('UWC',array('CID'=>$contentId),$page,$size,$order);
    }
    
    /**
     * 获取用户评论内容评论总数(用户所有评论)
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $userId     用户id
     * @return int 
     */
    public function getUserCommentContentTotal($userId){
       return $this->getTotalHandle('UWC',array('UID'=>$userId));
    }
    
    /**
     * 获取内容评论评论总数(内容下的评论总数)
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int   $contentId  内容id
     * @return int
     */
    public function getContentCommentUserTotal($contentId){
       return $this->getTotalHandle('UWC',array('CID'=>$contentId));
    }
    
    /**
     * 获取用户评论评论数组(我的二级评论数组)commentId_commentId=>time()
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $userId     用户id
     * @param int  $page       页面从0开始
     * @param int  $size       每页数量
     * @return array 评论id数组
     */
    public function getCommentCommentIdsByUserId($userId,$page=0,$size=10,$order='desc'){
        return $this->getListHandle('UWW',array('UID'=>$userId),$page,$size,$order);
    }
    
    /**
     * 获取评论评论的用户数组(评论下的二级评论id数组)userId_commentId=>time()
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $commentId  评论id
     * @param int  $page       页面从0开始
     * @param int  $size       每页数量
     * @return array 用户id数组
     */
    public function getCommentUserIdsByCommentId($commentId,$page=0,$size=10,$order='desc'){
        return $this->getListHandle('UWW',array('WID'=>$commentId),$page,$size,$order);
    }
    
    /**
     * 获取用户评论 评论总数(我的二级评论总数)
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $userId     用户id
     * @return int 
     */
    public function getUserCommentCommentTotal($userId){
        return $this->getTotalHandle('UWW',array('UID'=>$userId));
    }
    
    /**
     * 获取评论评论 评论总数(评论的二级评论总数)userId_commmentId=>time()
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $commentId  评论id
     * @return int
     */
    public function getCommentCommentUserTotal($commentId){
        return $this->getTotalHandle('UWW',array('WID'=>$commentId));
    }
    
    /**
     * 是否点赞内容 UPC
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P 内容创作作者X
     * @param int  $userId     用户id
     * @param int  $contentId  内容id
     * @return bool
     */
    public function IsPraiseContent($userId,$contentId){
        //存redis
        return $this->IsRelation('UPC',array('UID'=>$userId),$contentId);
    }
    
    /**
     * 是否点赞点赞评论UPW
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P 内容创作作者X
     * @param int  $userId     用户id
     * @param int  $commentId  评论id
     * @return bool
     */
    public function IsPraiseComment($userId,$commentId){
       //存redis
       return $this->IsRelation('UPW',array('UID'=>$userId),$commentId);
    }
    
    /**
     * 是否收藏内容UFC
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $userId     用户id
     * @param int  $contentId  内容id
     * @return bool
     */
    public function IsFavoriteContent($userId,$contentId){
       return $this->IsRelation('UFC',array('UID'=>$userId),$contentId);
    }
    
    /**
     * 是否关注关注人UAU
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $userId     用户id
     * @param int  $uId        用户id
     * @return bool
     */
    public function IsFollowUser($userId,$uId){
       return $this->IsRelation('UAX',array('UID'=>$userId),$uId);
    }
    
    /**
     * 是否关注话题UAT
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $userId     用户id
     * @param int  $topicId    话题id
     * @return bool
     */
    public function IsAttentionTopic($userId,$topicId){
       return $this->IsRelation('UAT',array('UID'=>$userId),$topicId);
    }
    
    /**
     * 是否关注节目视频UAV
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $userId     用户id
     * @param int  $programId  节目id
     * @return bool
     */
    public function IsAttentionProgram($userId,$programId){
       return $this->IsRelation('UAV',array('UID'=>$userId),$programId);
    }
    
    /**
     * 是否评论内容UWC
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $userId     用户id
     * @param int  $contentId  内容id
     * @param int  $commentId  评论id
     * @return bool
     */
    public function IsCommentContent($userId,$contentId,$comment_Id){
       return $this->IsRelation('UWC',array('UID'=>$userId),$contentId,$comment_Id);
    }
    
    /**
     * 是否评论评论UWW
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $userId     用户id
     * @param int  $commentId  评论id
     * @param int  $commentId  评论id
     * @return bool
     */
    public function IsCommentComment($userId,$commentId,$comment_Id){
       return $this->IsRelation('UWW',array('UID'=>$userId),$commentId,$comment_Id);
    }
    
    /********话题-内容 人-内容 节目-内容start*******/
    
    /** 添加删除内容
     * 人-内容-话题-节目
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P 内容创作作者X  R关系
     * @param $action  add添加、delete删除、deleteAll删除全部
     * @param int  $userId       作者id
     * @param int  $contentId     内容id
     * @param int  $topicId       话题id
     * @param int  $programId     节目id
     * @return bool
     */
    public function XCTVRelation($action,$userId,$contentId,$topicId,$programId){
        if($action == 'deleteAll'){
            return;
        }
        $this->setHandle($action,'TRC',array('TID'=>$topicId),array('CID'=>$contentId));
        $this->setHandle($action,'XRC',array('XID'=>$userId),array('CID'=>$contentId));
        $this->setHandle($action,'VRC',array('VID'=>$programId),array('CID'=>$contentId));
        $this->setHandle($action,'VRT',array('VID'=>$programId),array('TID'=>$topicId));//节目-话题
        $this->setHandle($action,'TRV',array('TID'=>$topicId),array('VID'=>$programId));//话题-节目
        return TRUE;
    }
    
    /**
     * 话题-内容 
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P 内容创作作者X  R关系
     * @param $action  add添加、delete删除、deleteAll删除全部
     * @param int  $topicId       话题id
     * @param int  $contentId     内容id
     * @return bool
     */
    public function topicRelationContent($action,$topicId,$contentId){
        return $this->setHandle($action,'TRC',array('TID'=>$topicId),array('CID'=>$contentId));
    }
    
    /**
     * 人-内容 
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P 内容创作作者X  R关系
     * @param $action  add添加、delete删除、deleteAll删除全部
     * @param int  $xId        内容创作作者id
     * @param int  $contentId     内容id
     * @return bool
     */
    public function userRelationContent($action,$xId,$contentId){
        return $this->setHandle($action,'XRC',array('XID'=>$xId),array('CID'=>$contentId));
    }
    
    /**
     * 节目-内容
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P 内容创作作者X  R关系
     * @param $action  add添加、delete删除、deleteAll删除全部
     * @param int  $xId        内容创作作者id
     * @param int  $contentId     内容id
     * @return bool
     */
    public function programRelationContent($action,$contentId,$programId){
        return $this->setHandle($action,'VRC',array('VID'=>$programId),array('CID'=>$contentId));
    }
    
    /**
     * 获取话题节目列表
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $topicId    话题id
     * @param int  $page       页面从0开始
     * @param int  $size       每页数量
     * @return array 数组
     */
    public function getProgramRByTopicId($topicId,$page=0,$size=10,$order='desc'){
       return $this->getListHandle('TRV',array('TID'=>$topicId),$page,$size,$order);
    }
    
    /**
     * 获取节目话题列表
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $programId    节目id
     * @param int  $page       页面从0开始
     * @param int  $size       每页数量
     * @return array 数组
     */
    public function getTopicRByProgramId($programId,$page=0,$size=10,$order='desc'){
       return $this->getListHandle('VRT',array('VID'=>$programId),$page,$size,$order);
    }
    
    /**
     * 获取话题内容列表
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $topicId    话题id
     * @param int  $page       页面从0开始
     * @param int  $size       每页数量
     * @return array 数组
     */
    public function getContentRByTopicId($topicId,$page=0,$size=10,$order='desc'){
       return $this->getListHandle('TRC',array('TID'=>$topicId),$page,$size,$order);
    }
    
    /**
     * 获取作者下的内容列表
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $userId     用户id
     * @param int  $page       页面从0开始
     * @param int  $size       每页数量
     * @return array 数组
     */
    public function getContentRByUserId($userId,$page=0,$size=10,$order='desc'){
       return $this->getListHandle('XRC',array('XID'=>$userId),$page,$size,$order);
    }
    
    /**
     * 获取节目下的内容列表
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $programId     用户id
     * @param int  $page       页面从0开始
     * @param int  $size       每页数量
     * @return array 数组
     */
    public function getContentRByProgramId($programId,$page=0,$size=10,$order='desc'){
       return $this->getListHandle('VRC',array('VID'=>$programId),$page,$size,$order);
    }
    
    /**
    * 获取话题内容总数
    * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
    * @param int  $topicId    话题id
    * @return int
     */
    public function getTopicContentRTotal($topicId){
       return $this->getTotalHandle('TRC',array('TID'=>$topicId));
    }
    
    /**
     * 获取作者下的内容总数
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $userId     用户id
     * @return int
     */
    public function getUserContentRTotal($userId){
       return $this->getTotalHandle('XRC',array('XID'=>$userId),$page,$size);
    }
    
    /**
     * 获取节目下的内容列表
     * 其他：用户U 话题T 节目V 内容C 评论W 收藏F 关注A 点赞P  内容创作作者X
     * @param int  $programId     用户id
     * @return array 数组
     */
    public function getProgramContentRTotal($programId){
       return $this->getTotalHandle('VRC',array('VID'=>$programId));
    }
    
    /********内容-话题 内容-人 内容节目end*******/
    
    /**
     * 获取实体
     * @param string  $table   
     * @param int     $id         
     * @return bool
     */
    public function getEntity($table,$id){
        return $this->getEty($this->TABLE[$table].'_'.$id);
    }

    /**
     * 处理实体
     * @param string  $action     add、delete、update
     * @param string  $table   
     * @param int     $id         
     * @return bool
     */
    public function handleEntity($action,$table,$id){
        //处理每张表
        $table_key = '';
        switch ($table){
            case 'user':
                $object = User::where('id','=',$id)
                            ->where('status','=',1)
                            ->where('is_delete','=',0)
                            ->first();
                $table_key = $this->TABLE['user'].'_'.$id;
                //删关系
                if(!$object){
                    $this->userAttentionProgram('deleteAll',$id,0);
                    $this->userAttentionTopic('deleteAll',$id,0);
                    $this->userCommentComment('deleteAll',$id,0);
                    $this->userCommentContent('deleteAll',$id,0);
                    $this->userFavoriteContent('deleteAll',$id,0);
                    $this->userFollowUser('deleteAll',$id,0);
                    $this->userPraiseComment('deleteAll',$id,0);
                    $this->userPraiseContent('deleteAll',$id,0);
                    //删除内容
                    $data = $this->getContentRByUserId($id, 0, 0);
                    if(is_array($data)){
                        foreach ($data as $key => $value) {
                            $this->handleEntity('delete','content',$key);//删除内容
                        }
                    }
                    return $this->handleEty('delete',$table_key,$id); 
                } 
                break;
            case 'content':
                $object = Content::where('id','=',$id)
                        ->first();
                $table_key = $this->TABLE['content'].'_'.$id;
                $cacheObject = $this->getEty($table_key);
                //删除内容
                if(!$object || $object->status == 0 || $object->is_delete == 1){
                    if($object){
                        $this->XCTVRelation('delete', $object->user_id, $id, $object->topic_id, $object->program_id);
                    }else if ($cacheObject) {
                        $this->XCTVRelation('delete', $cacheObject->user_id, $id, $cacheObject->topic_id, $cacheObject->program_id);
                    } 
                    $cacheData =$this->getCommentUserIdsByContentId($id, 0, 0);
                    foreach ($cacheData as $key => $value) {
                        $key_arr = explode('_', $key);
                        if(isset($key_arr[0]) && isset($key_arr[1]) ){
                            $this->userCommentContent('delete', $key_arr[0], $id, $key_arr[1]);
                            $this->handleEntity('delete','comment',$key_arr[1]);//删除评论
                        }
                    }
                    $cacheData =$this->getFavoriteUserIdsByContentId($id, 0, 0);
                    foreach ($cacheData as $key => $value) {
                        $this->userFavoriteContent('delete',$key, $id);
                    }
                    $cacheData =$this->getPraiseUserIdsBycontentId($id, 0, 0);
                    foreach ($cacheData as $key => $value) {
                        $this->userPraiseContent('delete',$key, $id);
                    }
                    //删内容
                    return $this->handleEty('delete',$table_key,$id);
                } else if($object && $object->status == 1 && $object->is_delete == 0 && $action == 'add'){
                    $this->XCTVRelation('add', $object->user_id, $id, $object->topic_id, $object->program_id);
                }
                break;
            case 'comment':
                $object = Comment::where('id','=',$id)
                        ->first();
                $table_key = $this->TABLE['comment'].'_'.$id;
                $cacheObject = $this->getEty($table_key);
                //删除评论
                if(!$object || $object->status == 0 || $object->is_delete == 1 || $action == 'delete'){
                    //$object->user_id $object->content_id $object->parent_id
                    if($object->parent_id){
                        // 评论的评论
                        $this->userCommentComment('delete', $object->user_id, $object->parent_id, $id);
                    }else{
                        // 评论的评论-获取评论下的评论
                        $cacheData = $this->getCommentUserIdsByCommentId($id, 0, 0);
                        foreach ($cacheData as $key => $value) {
                            $key_arr = explode('_', $key);
                            if(isset($key_arr[0]) && isset($key_arr[1]) ){
                                $this->handleEntity('delete','comment',$key_arr[1]);//删除评论
                            }
                        }
                    }
                    $this->userCommentContent('delete', $object->user_id, $object->content_id, $id);//内容-评论关系
                    
                    $cacheData =$this->getPraiseUserIdsByCommentId($id, 0, 0);
                    foreach ($cacheData as $key => $value) {
                        $this->userPraiseComment('delete',$key, $id);
                    }
                    //删除评论实体
                    return $this->handleEty('delete',$table_key,$id);
                }else if($object && $object->status == 1 && $object->is_delete == 0 && $action == 'add'){
                    $this->userCommentContent('add', $object->user_id, $object->content_id, $id);//内容-评论关系
                    if($object->parent_id){
                         $this->userCommentComment('add', $object->user_id, $object->parent_id, $id);
                    }
                }
                break;
            case 'topic':
                //删除话题
                $object = Topic::where('id','=',$id)
                        ->first();
                $table_key = $this->TABLE['topic'].'_'.$id;
                $cacheObject = $this->getEty($table_key);
                if(!$object || $object->status == 0 || $object->is_delete == 1){
                    $cacheData =$this->getAttentionUserIdsByTopicId($id, 0, 0);
                    foreach ($cacheData as $key => $value) {
                        $this->userAttentionTopic('delete',$key, $id);
                    }
                    //删除话题下的内容
                    $cacheData = $this->getContentRByTopicId($id, 0, 0);
                    foreach ($cacheData as $key => $value) {
                        $this->handleEntity('delete','content',$key);//删除内容
                        $this->topicRelationContent('delete', $id, $key);//话题内容关系
                    }
                    
                    //删除评论实体
                    return $this->handleEty('delete',$table_key,$id);
                }
                break;
            case 'program':
                $object = Program::where('id','=',$id)
                        ->first();
                $table_key = $this->TABLE['program'].'_'.$id;
                $cacheObject = $this->getEty($table_key);
                if(!$object || $object->is_delete == 1){
                    $cacheData =$this->getAttentionUserIdsByProgramId($id, 0, 0);
                    foreach ($cacheData as $key => $value) {
                        $this->userAttentionProgram('delete',$key, $id);
                    }
                    //删除视频节目
                    return $this->handleEty('delete',$table_key,$id);
                } else {
                    
                }
                break;
            default :
                break;
        }
        //redis实体增删改查
        if($object && $table_key){
            return $this->handleEty($action,$table_key,$object->toArray());
        }
    }
    
    
    /** 
     * reboot Redis 未完成
     */
    public function rebootRedis(){
        //清空redis
        
        //praise user_id item_id type is_delete
        //comment user_id parent_id content_id  status is_delete
        //
        
        return $this->reboot();
    }
    /** 
     * 获取首页关注的内容列表
     * @param string  $userId
     * @param string  $page   
     * @param int     $size         
     * @return contentId数组(contentId=>time())
     */
    public function getFollowerContentByUserId($userId, $page, $size){
        $mRedis = new MRedis();
        //用户id数组
        $ids = $mRedis->getFollowerByUserId($userId, $page, 0);
        $retData = [];
        if(count($ids) > 0){
            foreach ($ids as $key=>$val){
                $cIds = $mRedis->getContentRByUserId($key, $page, 0);//用户的内容数组
                if($cIds){
                    $retData = $retData + $cIds;
                }
            }
            arsort($retData);
            $temp =  array_slice($retData, $page*$size, $size, true);
            return $temp;
        }
    }
    
    //定时刷新关系至数据库
    public function syncFromRedisToDB(){
        //刷新 点赞 关注 收藏关系
        with(new MRedisLib())->syncFromRedisToDB();
    }
}

