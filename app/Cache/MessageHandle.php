<?php namespace App\Cache;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use App\Cache\MRedis;
class MessageHandle{
    
    /**
    * 处理后台发来的消息：json
    * @param message  {"type":"database","data":[{"table":"user","action":"add","id":"1"},]}
    * @return bool
    */
    public function handle($message){
        $message_json = json_decode($message,TRUE);
        if(isset($message_json['type'])){
            $type = $message_json['type'];
            switch ($type){
                case "database" : 
                    if(isset($message_json['data'])){
                        $this->databaseHandle($message_json['data']);
                    }
                    break;
                default : return FALSE;
            }
        }
        return TRUE;
    }
    
    /**
    * 处理数据库类型的消息
    * @param $data  数组[{"table":"user","action":"add","id":"1"},]
    * @return bool
    */
    public function databaseHandle($data){
        $index = array();
        if(isset($data['table'])){
            $index[] = $data;
        }else if(is_array($data)){
            $index = $data;
        }else{
            return;
        }
        foreach ($index as $value) {
            if(isset($value['table']) && isset($value['action']) && isset($value['id'])){
                $mRedis = new MRedis();
                $mRedis->handleEntity($value['action'],$value['table'],$value['id']);
            }
        }
        return TRUE;
    }
    
    
}
