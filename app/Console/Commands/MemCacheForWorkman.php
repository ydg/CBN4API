<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Utils\CacheForWorkman;
class MemCacheForWorkman extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'MemCacheForWorkman:putcache';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'workman cache controller';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
            $Obj = new CacheForWorkman();
            $Obj->clearCacheVisitor();
            $Obj->clearCachePraise();
            $Obj->clearCacheViewHistory();
            return 0;
	}

}
