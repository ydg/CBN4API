<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Utils\CacheForWorkman;
class WorkmanLog extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'WorkmanLog:change';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'workman log';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
            $Obj = new CacheForWorkman();
            $Obj->changeWorkmanLog();
            return 0;
	}

}
