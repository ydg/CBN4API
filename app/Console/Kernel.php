<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use Log;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
                \App\Console\Commands\MemCacheForWorkman::class,
                \App\Console\Commands\WorkmanLog::class,

	];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //读workman缓存任务定时
        //方法一:
//            $schedule->call(function () {
//                Log::info('dsfhsdlkf');
//            })->everyMinute();
        //方法二:
        $schedule->command('MemCacheForWorkman:putcache')->everyMinute();
        
        $schedule->command('WorkmanLog:change')->daily();
    }
}
