<?php namespace App\Http\Controllers\Cbn;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Utils\ReturnUtils;
use App\Models\Comment;
class CommentController extends Controller {

    //获取内容下的评论(Mobile)
    public function getContentCommentList(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Comment($request))->getContentCommentList();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_SOME_GET']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data['data'],$data['paging']);
    }
    //获取内容下的评论(Mobile)
    public function getUserCommentList(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Comment($request))->getUserCommentList();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_SOME_GET']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data['data'],$data['paging']);
    }
    //发表评论(Mobile)
    public function createComment(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Comment($request))->createComment();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_COMMENT_ADD']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS']);
    }

    //删除评论(Mobile)
    public function deleteComment(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Comment($request))->deleteComment();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_COMMENT_DELETE']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS']);
    }

/*******************************************SMARTCONTROLLER**********************************************/
}
