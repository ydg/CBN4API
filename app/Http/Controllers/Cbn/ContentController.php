<?php namespace App\Http\Controllers\Cbn;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Utils\ReturnUtils;
use App\Models\Content;
use App\Utils\CommonUtils;
use App\Models\Comment;
use App\Cache\BaseCache;
class ContentController extends Controller {

    //获取分类下的内容列表
    public function getContentList(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Content($request))->getContentList();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_SOME_GET']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data['data'],$data['paging']);
    }

    //发布内容
    public function createContent(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Content($request))->createContent();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_CONTENT_ADD']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data);
    }

    //获取关注人们的内容列表(TV)
    public function getFollowerContentList(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Content($request))->getFollowerContentList();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_SOME_GET']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data['data'],$data['paging']);
    }
    //    获取用户的内容列表
    public function getUserContentList(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Content($request))->getUserContentList();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_SOME_GET']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data['data'],$data['paging']);
    }

    //获取推荐下内容列表(Mobile)
    public function getRecommendContentList(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Content($request))->getRecommendContentList();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_SOME_GET']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data['data'],$data['paging']);
    }

    //获取搜索结果(话题.用户.视频.内容)(Mobile)
    public function getSearchList(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Content($request))->getSearchList();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_SOME_GET']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data['data'],$data['paging']);
    }

    //获取话题下的内容(包含话题详情)(Mobile)
    public function getTopicContentList(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Content($request))->getTopicContentList();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_SOME_GET']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data['data'],$data['paging']);
    }

    //获取电影下的内容(包含电影详情)(Mobile)
    public function getProgramContentList(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Content($request))->getProgramContentList();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_SOME_GET']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data['data'],$data['paging']);
    }

    //获取内容详情(Mobile)
    public function getContentDetail(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Content($request))->getContentDetail();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_SOME_GET']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data);
    }
    
    //
    public function isPraise(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Content($request))->isPraise();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_PRASIE_ADD']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS']);
    }
    
    //内容/评论点赞(Mobile)
    public function createPraise(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Content($request))->addPraise();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_PRASIE_ADD']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS']);
    }

    //内容/评论取消点赞(Mobile)
    public function cancelPraise(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Content($request))->cancelPraise();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_PRASIE_DELETE']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS']);
    }
    
    //内容分享页
    public function getShareContentDetail(Request $request) {
        
        $contentId = $request->input('contentId');
        $cache = new BaseCache();
        $cache->method = 'get';
        $cache->cacheName = 'sharePage';//分享页面
        $cache->cachePar = array('content',$contentId); //集合
        $retCache = $cache->HandleCache();
        if($retCache !== 'NO_CACHE' && $retCache !== 'ERROR_CACHE'){
//            return $retCache;
        }
        $content = with(new Content($request))->getContentDetail();
        if(!$content){
            $html = view('errors.404')->render();
            $cache->method = 'put';
            $cache->cacheValue = $html;
            $retCache = $cache->HandleCache();
            return $html ;
        }
        $content['topicId'] = with(new CommonUtils())->getUuid($content['topicId']);
        $content['programId'] = with(new CommonUtils())->getUuid($content['programId'] );
        //构造参数
        $request->merge(array('page' => '0'));
        $request->merge(array('size' => '20'));
        $comment = with(new Comment($request))->getContentCommentList();
        if($content['endTime'] - $content['startTime'] <= 0){
           $content['startTime'] = 0; 
           $content['endTime'] = 90;
        }
        $data = array(
            'content'=>$content,
            'comments'=> isset($comment['data'])?$comment['data']:array(),
            'shareUrl'=>$content['shareUrl'],
            'shareTitle'=>$content['programName'],
            'shareDescription'=>$content['content'],
            'shareIcon'=>$content['contentImg'],
        );
        $html = view('share.share_content',$data)->render();
        $cache->method = 'put';
        $cache->cacheValue = $html;
        $retCache = $cache->HandleCache();
        return $html ;
    }

/*******************************************SMARTCONTROLLER**********************************************/
}
