<?php namespace App\Http\Controllers\Cbn;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Utils\ReturnUtils;
use App\Models\Program;
use App\Models\Topic;
use App\Models\Content;
use App\Cache\BaseCache;
use App\Utils\CommonUtils;

class ProgramController extends Controller {

    //同步视频节目
    public function syncProgram(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Program($request))->syncProgram();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_SOME_GET']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data);
    }
    //获取关注的电影(Mobile)
    public function getProgramDetail(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Program($request))->getProgramDetail();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_SOME_GET']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data);
    }
    //获取关注的电影(Mobile)
    public function getUserProgramList(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Program($request))->getUserProgramList();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_SOME_GET']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data['data'],$data['paging']);
    }

    //电影分享页
    public function getShareProgramDetail(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $programId = $request->input('programId');
        $cache = new BaseCache();
        $cache->method = 'get';
        $cache->cacheName = 'sharePage';//分享页面
        $cache->cachePar = array('program',$programId); //集合
        $retCache = $cache->HandleCache();
        if($retCache !== 'NO_CACHE' && $retCache !== 'ERROR_CACHE'){
            return $retCache;
        }
        $program = with(new Program($request))->getProgramDetail();
        if(!$program){
            $html = view('errors.404')->render();
            $cache->method = 'put';
            $cache->cacheValue = $html;
            $retCache = $cache->HandleCache();
//            return $html ;
        }
        $program['programId'] = with(new CommonUtils())->getUuid($program['programId'] );
        //构造参数
        $request->merge(array('page' => '0'));
        $request->merge(array('size' => '20'));
        $content = with(new Content($request))->getProgramContentList();
        $commonUtils = new CommonUtils();
        if(isset($content['data'])){
            foreach ($content['data'] as $key => $value) {
                $content['data'][$key]['contentId'] = $commonUtils->getUuid($value['contentId']);
                $content['data'][$key]['programId'] = $commonUtils->getUuid($value['programId']);
                $content['data'][$key]['topicId'] = $commonUtils->getUuid($value['topicId']);
            }
        }
        $topic = with(new Topic($request))->getProgramTopicList();
        if(isset($topic['data'])){
            foreach ($topic['data'] as $key => $value) {
                $topic['data'][$key]['topicId'] = $commonUtils->getUuid($value['topicId']);
            }
        }
        $data = array(
            'program'=>$program,
            'contents'=> isset($content['data'])?$content['data']:array(),
            'topics'=> isset($topic['data'])?$topic['data']:array(),
            'shareUrl'=>$program['shareUrl'],
            'shareTitle'=>$program['programName'],
            'shareDescription'=>$program['introduction'],
            'shareIcon'=>$program['img'],
        );
        $html = view('share.share_program',$data)->render();
        $cache->method = 'put';
        $cache->cacheValue = $html;
        $retCache = $cache->HandleCache();
        return $html ; 
    }
/*******************************************SMARTCONTROLLER**********************************************/
}
