<?php namespace App\Http\Controllers\Cbn;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Utils\ReturnUtils;
use App\Models\AppVersion;
use App\Models\Accuse;
use App\Http\Controllers\SysController;
use App\Models\Common;
class SystemController extends Controller {

    //获取配置
    public function getConfig(Request $request) {
        $returnUtils = new ReturnUtils($request);
        
        // 获取七牛配置
        $accessKey = env('QINIU_ACCESS_KEY');
        $secretKey = env('QINIU_SECRET_KEY');
        $bucket = env('QINIU_BUCKET');
        $callbackBody = env('QINIU_CALLBACK_BODY');
        $callbackUrl = env('HTTP_HOST') . env('QINIU_CALLBACK');
        $data = array();
        $data['aboutUs'] = $this->getAbout();
        $data['qiNiuInfo']['bucket'] = $bucket;
        $data['qiNiuInfo']['qiNiuHost'] = env('QINIU_HOST_NO_HTTP');
        $updateInfo = array();
        $updateInfo['version'] = '1.0.0';
        $updateInfo['mustUpdate'] = 0; //0 不需要更新 1 需要更新
        $updateInfo['description'] = '默认值';
        $updateInfo['url'] = '';
//        $accessKey = env('QINIU_ACCESS_KEY');
//        $secretKey = env('QINIU_SECRET_KEY');
//        $callbackBody = env('QINIU_CALLBACK_BODY');
//        $callbackUrl = env('HTTP_HOST') . env('QINIU_CALLBACK');
//        
        if ($request->input('ty') == 'i') {
            $updateInfoIndex = with(new AppVersion($request))->getIosAppVersion();
            if ($updateInfoIndex) {
                $updateInfo = $updateInfoIndex;
            }
        } else if ($request->input('ty') == 'a'  ) {
            $updateInfoIndex = with(new AppVersion($request))->getAndroidMobleAppVersion();
            if ($updateInfoIndex) {
                $updateInfo = $updateInfoIndex;
            }
        } else if ($request->input('ty') == 'tv' ) {
            $updateInfoIndex = with(new AppVersion($request))->getAndroidTvAppVersion();
            if ($updateInfoIndex) {
                $updateInfo = $updateInfoIndex;
            }
        } 
        $data['versionInfo'] = $updateInfo;
        $data['qiNiuInfo']['upToken'] = with(new SysController())->getUptoken($accessKey, $secretKey, $bucket, $callbackUrl, $callbackBody);
//        $data['shareUrl'] = $this->getShareURL();
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'], $data);
    }

    //用户举报反馈
    public function createAccuse(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Accuse($request))->createAccuse();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_ACCUSE_ADD']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS']);
    }
    
    //获取关于
    public function getAbout() {
        return with(new Common())->getAbout();
    }
/*******************************************SMARTCONTROLLER**********************************************/
}
