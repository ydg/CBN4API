<?php namespace App\Http\Controllers\Cbn;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Utils\ReturnUtils;
use App\Models\Topic;
use App\Models\Category;
use App\Models\RecommendItem;
use App\Cache\BaseCache;
use App\Utils\CommonUtils;
use App\Models\Content;
class TopicController extends Controller {

    
    //获取分类
    public function getCategoryV2(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Category($request))->getCategory();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_SOME_GET']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data['data']);
    }
    //获取分类
    public function getCategory(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Category($request))->getCategory();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_SOME_GET']);
        }
//        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data['data']);
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data);
    }

    //获取话题
    public function getTopic(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Topic($request))->getTopic();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_SOME_GET']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data);
    }
    
    //获取话题
    public function getProgramTopicList(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Topic($request))->getProgramTopicList();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_SOME_GET']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data['data'],$data['paging']);
    }

    //获取推荐话题(发现和搜索页)(Mobile)
    public function getRecommendItemList(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new RecommendItem($request))->getRecommendItemList();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_SOME_GET']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data);
    }
    //获取话题详情
    public function getTopicDetail(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Topic($request))->getTopicDetail();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_SOME_GET']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data);
    }
    //获取全部话题(一期不带分类)(Mobile)
    public function getAllTopic(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Topic($request))->getAllTopic();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_SOME_GET']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data['data'],$data['paging']);
    }

    //话题分享页
    public function getShareTopicDetail(Request $request) {
        
        $returnUtils = new ReturnUtils($request);
        $topicId= $request->input('topicId');
        $cache = new BaseCache();
        $cache->method = 'get';
        $cache->cacheName = 'sharePage';//分享页面
        $cache->cachePar = array('topic',$topicId); //集合
        $retCache = $cache->HandleCache();
        if($retCache !== 'NO_CACHE' && $retCache !== 'ERROR_CACHE'){
//            return $retCache;
        }
        $topic =with(new Topic($request))->getTopicDetail();
        if(!$topic){
            $html = view('errors.404')->render();
            $cache->method = 'put';
            $cache->cacheValue = $html;
            $retCache = $cache->HandleCache();
            return $html ;
        }
        $topic['topicId'] = with(new CommonUtils())->getUuid($topic['topicId'] );
        //构造参数
        $request->merge(array('page' => '0'));
        $request->merge(array('size' => '20'));
        $content = with(new Content($request))->getTopicContentList();
        $commonUtils = new CommonUtils();
        if(isset($content['data'])){
            foreach ($content['data'] as $key => $value) {
                $content['data'][$key]['contentId'] = $commonUtils->getUuid($value['contentId']);
                $content['data'][$key]['programId'] = $commonUtils->getUuid($value['programId']);
                $content['data'][$key]['topicId'] = $commonUtils->getUuid($value['topicId']);
            }
        }
        $data = array(
            'topic'=>$topic,
            'contents'=> isset($content['data'])?$content['data']:array(),
            'shareUrl'=>$topic['shareUrl'],
            'shareTitle'=>$topic['title'],
            'shareDescription'=>$topic['introduction'],
            'shareIcon'=>$topic['img'],
        );
        $html = view('share.share_topic',$data)->render();
        $cache->method = 'put';
        $cache->cacheValue = $html;
        $retCache = $cache->HandleCache();
        return $html ; 
    }
/*******************************************SMARTCONTROLLER**********************************************/
}
