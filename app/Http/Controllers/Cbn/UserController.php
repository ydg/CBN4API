<?php namespace App\Http\Controllers\Cbn;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Utils\ReturnUtils;
use App\Models\User;
use App\Models\Followers;
use App\Models\Favorite;
use App\Models\QRcode;

class UserController extends Controller {

    //同步用户信息
    public function syncUser(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new User($request))->syncUser();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_SOME_GET']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data);
    }

    //关注
    public function attention(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new User($request))->attention();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_OTHERUSER_ADDATTENTION']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS']);
    }

    //取消关注
    public function deleteAttention(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new User($request))->deleteAttention();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_OTHERUSER_DELETEATTENTION']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS']);
    }

    //取消全部关注
    public function deleteALLAttention(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new User($request))->deleteALLAttention();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_OTHERUSER_DELETEATTENTION']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS']);
    }
    //是否收藏
    public function isFavarite(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Favorite($request))->isFavarite();
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data);
    }
    
    //收藏
    public function favarite(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Favorite($request))->favarite();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_FAVORITE_ADD']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS']);
    }

    //取消收藏
    public function deleteFavarite(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Favorite($request))->deleteFavarite();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_FAVORITE_DELETE']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS']);
    }

    //取消全部收藏
    public function deleteAllFavarite(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Favorite($request))->deleteAllFavarite();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_FAVORITE_DELETE']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS']);
    }

    //获取用户关注的列表
    public function getAttentionList(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new User($request))->getAttentionList();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_SOME_GET']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data['data'],$data['paging']);
    }

    //获取用户收藏的列表
    public function getFavariteList(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Favorite($request))->getFavariteList();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_SOME_GET']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data['data'],$data['paging']);
    }

//    获取用户的内容列表
//    public function getUserContentList(Request $request) {
//        $returnUtils = new ReturnUtils($request);
//        $data = with(new User($request))->getUserContentList();
//        if(!$data){
//            return $returnUtils->returnError($returnUtils->return['ERROR_SOME_GET']);
//        }
//        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data);
//    }

    //是否关注人(TV)
    public function isFollow(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Followers($request))->isFollow();
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data);
    }
    
    //关注人(TV)
    public function follow(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Followers($request))->follow();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_OTHERUSER_ADDATTENTION']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data);
    }

    //取消关注人(TV)
    public function deleteFollow(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Followers($request))->deleteFollow();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_OTHERUSER_DELETEATTENTION']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data);
    }

    //取消全部关注的人
    public function deleteAllFollow(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Followers($request))->deleteAllFollow();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_OTHERUSER_DELETEATTENTION']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data);
    }

    //获取用户关注人的列表(TV)
    public function getFollowerList(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Followers($request))->getFollowerList();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_SOME_GET']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data['data'],$data['paging']);
    }
    
    //获取用户的粉丝列表(TV)
    public function getFansList(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new Followers($request))->getFansList();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_SOME_GET']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data['data'],$data['paging']);
    }
    
    //用户登录
    public function login(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new User($request))->login();
        if(!is_array($data)){
            switch($data){
                case 1://验证码有误
                    return $returnUtils->returnError($returnUtils->return['ERROR_MSMCODE_ERROR']);
                case 2://超过设备上限
                    return $returnUtils->returnError($returnUtils->return['ERROR_DEVICE_LIMIT']);
            }
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data);
    }

    //用户登出
    public function logout(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new User($request))->logout();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_LOGOUT']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS']);
    }

    //获取验证码
    public function verifyCode(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new User($request))->verifyCode();
        switch($data){
            case 0://成功
                return $returnUtils->returnOk($returnUtils->return['SUCCESS']);
            case 1://格式错误
                return $returnUtils->returnError($returnUtils->return['ERROR_PARAMETER_VLAUE_PHONE']);
            case 2://超过每日上限
                return $returnUtils->returnError($returnUtils->return['ERROR_MSMCODE_LIMIT']);
            case 3://获取错误
                return $returnUtils->returnError($returnUtils->return['ERROR_MSMCODE_GET']);
            default://获取错误
                return $returnUtils->returnError($returnUtils->return['ERROR_MSMCODE_GET']);
        }
    }

    //更新用户信息
    public function updateUserInfo(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new User($request))->updateUserInfo();
        if($data == FALSE){
            return $returnUtils->returnError($returnUtils->return['ERROR_USER_UPDATEINFO']);
        }else if($data == 1){
            return $returnUtils->returnError($returnUtils->return['ERROR_USER_UNLOGIN']);
        } else if($data == 2){
            return $returnUtils->returnError($returnUtils->return['ERROR_USER_TOKEN']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data);
    }

    //获取用户信息
    public function getUserInfo(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new User($request))->getUserInfo();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_SOME_GET']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data);
    }
    
    //获取用户信息
    public function getQRcode(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new QRcode($request))->getQRcode();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_SOME_GET']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data);
    }
    //获取扫码状态
    public function getQRcodeStatus(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new QRcode($request))->getQRcodeStatus();
        if(is_numeric($data)){
            switch ($data){
                case 1:
                    return $returnUtils->returnError($returnUtils->return['ERROR_SCAN']);
                case 2:
                    return $returnUtils->returnError($returnUtils->return['ERROR_DEVICE_LIMIT']);
                case 3:
                    return $returnUtils->returnError($returnUtils->return['ERROR_QRCODE']);
                case 4:
                    return $returnUtils->returnError($returnUtils->return['ERROR_QRCODE_INVALID']);
            }
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS'],$data);
    }
    //获取扫码状态
    public function scanQRcode(Request $request) {
        $returnUtils = new ReturnUtils($request);
        $data = with(new QRcode($request))->scanQRcode();
        if(!$data){
            return $returnUtils->returnError($returnUtils->return['ERROR_SOME_GET']);
        }
        return $returnUtils->returnOk($returnUtils->return['SUCCESS']);
    }
/*******************************************SMARTCONTROLLER**********************************************/
}
