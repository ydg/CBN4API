<?php namespace App\Http\Middleware;

use Closure;
use Log;
use Route;
use DB;
use App\Utils\CommonUtils;

class AfterMiddleware {

    public function handle($request, Closure $next)
    {
        // Perform action
		//异常捕获
		try {
			$response = $next($request);
            with(new CommonUtils($request))->debugQueryLog();
		} catch (\Exception $exc) {
			Log::info($exc->getTraceAsString());
			$response ='';
		}
        return $response;
    }
}