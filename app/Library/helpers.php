<?php
if ( ! function_exists('show_img')){
	//显示图片
	function show_img($img, $default = ''){
		$img_url = '';
		$qiniu_url = env('QINIU_HOST','http://img.paas.onairm.cn/');
		if(!$img){//地址不存在
			if(strpos($default, 'http://') === 0){//默认地址为绝对地址
				$img_url = $default;
			}elseif(strpos($default, '/') === false){//默认地址为七牛地址
				$img_url = $qiniu_url.$default;
			}else{//默认地址为相对地址
				$img_url = URL::to($default);
			}
		}elseif(strpos($img, 'http://') === 0){//绝对地址
			$img_url = $img;
		}else{
			if(strpos($img, '/') === false){//七牛地址
				$img_url = $qiniu_url.$img;
			}else{//相对地址
				$img_url = URL::to($img);
			}
		}
		return $img_url;
	}
}

