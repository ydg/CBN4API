<?php  namespace App\Models;

class Accuse extends BaseModel{
    
	//表明
	protected $table = 'accuse';
	//主键
	protected $primaryKey = 'id';
        
    public function createAccuse(){
        $userId = $this->request->input('userId');
        $itemId = $this->request->input('itemId');
        $type = $this->request->input('type');
        $content = $this->request->input('content');
        $this->object_id = $itemId;
        $this->type = $type;
        $this->content = $content;
        $this->user_id = $userId;
        $ret = $this->save();
        return $ret;
    }
}