<?php  namespace App\Models;

class AppVersion extends BaseModel{
    
	//表明
	protected $table = 'app_version';
	//主键
	protected $primaryKey = 'id';
	
    public function getAndroidMobleAppVersion(){
        $updateInfo = null;
        $appVersion = AppVersion::select('id','type','version','download_url','description','update_status')
                ->where('type','=','1')
				->where('status','=','1')
				->where('is_delete','=','0')
				->orderBy('sort','asc')
				->orderBy('id','desc')
				->first();
        if($appVersion){
            $updateInfo['version'] = $appVersion->version;
            $updateInfo['mustUpdate'] = $appVersion->update_status;//0 不需要更新 1 需要更新
            $updateInfo['description'] = $appVersion->description;
            $updateInfo['url'] =  $appVersion->download_url;
        }
        return $updateInfo;
    }
    public function getAndroidTvAppVersion(){
        $updateInfo = null;
        $appVersion = AppVersion::select('id','type','version','download_url','description','update_status')
                ->where('type','=','3')
				->where('status','=','1')
				->where('is_delete','=','0')
				->orderBy('sort','asc')
				->orderBy('id','desc')
				->first();
        if($appVersion){
            $updateInfo['version'] = $appVersion->version;
            $updateInfo['mustUpdate'] = $appVersion->update_status;//0 不需要更新 1 需要更新
            $updateInfo['description'] = $appVersion->description;
            $updateInfo['url'] =  $appVersion->download_url;
        }
        return $updateInfo;
    }
    
    public function getIosAppVersion(){
        $updateInfo = null;
        $appVersion = AppVersion::select('id','type','version','download_url','description','update_status')
                ->where('type','=','2')
				->where('status','=','1')
				->where('is_delete','=','0')
				->orderBy('sort','asc')
				->orderBy('id','desc')
				->first();
        if($appVersion){
            $updateInfo['version'] = $appVersion->version;
            $updateInfo['mustUpdate'] = $appVersion->update_status;//0 不需要更新 1 需要更新
            $updateInfo['description'] = $appVersion->description;
            $updateInfo['url'] =  $appVersion->download_url;
        }
        return $updateInfo;
    }
    
    
}