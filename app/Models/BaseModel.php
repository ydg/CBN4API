<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Log;

class BaseModel extends Eloquent {
    
    protected $TAKE_NUM = 60;
    
    protected $request =null;
    // 验证请求参数格式参数
    public $connection = '';
    
    // 验证请求参数格式参数
    public function __construct( $request = null) {
        $this->connection = env('MYSQL_CONNECTION','mysql');
        $this->request = $request;
    }
    
    public function getArea($str){
        $arr = explode(',', $str);
        $area = '';
        foreach ($arr as $key => $value) {
            switch($value){
                case '1':
                    if($key == 0){
                        $area = '内地';
                    }else{
                        $area .= '内地';
                    }
                    break;
                case '2':
                    if($key == 0){
                        $area = '香港';
                    }else{
                        $area .= '香港';
                    }
                    break;
                case '3':
                    if($key == 0){
                        $area = '美国';
                    }else{
                        $area .= '美国';
                    }
                    break;
                case '4':
                    if($key == 0){
                        $area = '欧洲';
                    }else{
                        $area .= '欧洲';
                    }
                    break;
                case '5':
                    if($key == 0){
                        $area = '日本';
                    }else{
                        $area .= '日本';
                    }
                    break;
                case '6':
                    if($key == 0){
                        $area = '印度';
                    }else{
                        $area .= '印度';
                    }
                    break;
                case '7':
                    if($key == 0){
                        $area = '韩国';
                    }else{
                        $area .= '韩国';
                    }
                    break;
                case '8':
                    if($key == 0){
                        $area = '英国';
                    }else{
                        $area .= '英国';
                    }
                    break;
                case '9':
                    if($key == 0){
                        $area = '泰国';
                    }else{
                        $area .= '泰国';
                    }
                    break;
                case '10':
                    if($key == 0){
                        $area = '其他';
                    }else{
                        $area .= '其他';
                    }
                    break;
                default :
                    if($key == 0){
                        $area = '其他';
                    }else{
                        $area .= '其他';
                    }
            }
        }
        return $area;    
    }
    
    public function showImg($img){
        //显示图片
        if(!$img){
            return '';
        }
		$img_url = '';
		$qiniu_url = env('QINIU_HOST');
        $img= trim($img);
		if(strpos($img, 'http://') === 0){//绝对地址
			$img_url = $img;
		}else{ 
            $img = trim($img,'?');
            $img_url = $qiniu_url.$img;
		}
		return $img_url;
    }
    
    
    //重写模型自动维护时间格式
	protected function getDateFormat(){
        return 'U';
    }
    //arr1为实体数据,arr2为redis获取的key = >time $idx为实体下标
    public function getAdjustData($arr1, $idx, $arr2){
        foreach ($arr1 as $key => $val){
            foreach ($arr2 as $k => $v){
                if($val[$idx] == $k){
                    $arr1[$key]['score'] = $v;
                    break;
                }
            }
        }
        return $arr1;
    }
}
