<?php  namespace App\Models;

class Category extends BaseModel{
    
	//表明
	protected $table = 'category';
	//主键
	protected $primaryKey = 'id';
	
    public function getCategoryObject(){
        $object = array();
        $object['categoryId'] = $this->id;
        $object['name'] = $this->name;
        $object['keywords'] = $this->keywords;
//        $object['introduction'] = $this->introduction;
//        $object['createdBy'] = $this->created_by;
//        $object['createdAt'] = $this->created_at;
//        $object['updatedAt'] = $this->updated_at;
        return $object;
    }
    //获取分类
    public function getCategory() {
        $categorys = $this->select('id','name','keywords','introduction','sort','created_by','created_at','updated_at')
                ->where('status','=',1)
                ->where('is_delete','=',0)
                ->orderBy('sort','asc')
                ->get();
        $data = [];
        $data['data'] = [];
        foreach ($categorys as $category) {
            $data['data'][] = $category->getCategoryObject();
        }
        return $data;
    }
}