<?php  namespace App\Models;

use App\Cache\MRedis;
use App\Models\User;
use Log;

class Common extends BaseModel{
    
	//表明
	protected $table = 'common';
	//主键
	protected $primaryKey = 'id';
    public function getAbout(){
        $obj = $this->select('id','type','title','content','is_delete')
                ->where('is_delete','=',0)
                ->where('type','=',1)
                ->first();
        if($obj){
            return $obj->content;
        }else{
            return FALSE;
        }
    }
    
}