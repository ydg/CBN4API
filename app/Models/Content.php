<?php  namespace App\Models;
use DB;
use App\Models\Topic;
use App\Models\User;
use App\Models\Program;
use App\Models\Content;
use App\Models\Praise;
use App\Models\ContentStats;
use App\Cache\MRedis;
use App\Utils\CommonUtils;
use Log;
class Content extends BaseModel{
    
	//表明
	protected $table = 'content';
	//主键
	protected $primaryKey = 'id';
	
    public function getContentObject($Obj=null){
        $object = array();
        $id;
        $userId;
        if($Obj){
            $id = $Obj['id'];
            $conUser = $Obj['user_id'];
            $object['contentId'] = $Obj['id'];
            $object['content'] = $Obj['content'];
            $object['contentImg'] = $Obj['img']?$Obj['img']:'3c17091d03bc8d6e5cc02eaed6235d01';

            $object['startTime'] = $Obj['start_time'];
            $object['endTime'] = $Obj['end_time'];
            $object['releaseTime'] = $Obj['release_time'];

            $object['topicId'] = $Obj['topic_id'];
            $object['topicName'] = $Obj['topic_name'];

            $object['programId'] = $Obj['program_id'];
            $object['programName'] = $Obj['program_name'];
            $object['programUrl'] = $Obj['program_url']?$Obj['program_url']:'b48eebfae89ea0b1b100d07e7c104220';

            $object['userId'] = $Obj['user_id']?$Obj['user_id']:'';
            
            //获取用户实体
            $userObj = with(new User($this->request))->getRedisUserObject($Obj['user_id']);
            $object['userName'] = isset($userObj['nickname'])?$userObj['nickname']:'';
            $object['userImg'] = isset($userObj['userIcon'])?$userObj['userIcon']:'';
            $object['userType'] = isset($userObj['userType'])?$userObj['userType']:'';
//            $mRedis = new MRedis();
//            $userObj = $mRedis->getEntity('user', $Obj['user_id']);
//            if(!$userObj){
//                $u = User::select('id','uid','img','name','phone','slogan','birthday','gender','type','status','is_delete')
//                        ->where('user.id','=',$Obj['user_id'])
//                        ->first();
//                $u->request = $this->request;
//                $objUser = $u->getUserObject();
//                $object['userName'] = isset($objUser['nickname'])?$objUser['nickname']:'';
//                $object['userImg'] = isset($objUser['userIcon'])?$objUser['userIcon']:'';
//                $object['userType'] = isset($objUser['userType'])?$objUser['userType']:'';
//            }else{
//                $user = with(new User($this->request))->getUserObject($userObj);
//                $object['userName'] = isset($user->name)?$user->name:'';
//                $object['userImg'] = isset($user->user_img)?$user->user_img:'';
//                $object['userType'] = isset($user->user_type)?$user->user_type:'';
//            }

            $object['commentTotal'] = 0;
            $object['praiseTotal'] = 0;
            $object['isAttention'] = 0;
            $object['isFavorite'] = 0;
            $object['isPraise'] = 0;
        }else{
            $id = $this->id;
            $conUser = $this->user_id;
            $object['contentId'] = $this->id;
            $object['content'] = $this->content;
            $object['contentImg'] = $this->content_img?$this->content_img:'3c17091d03bc8d6e5cc02eaed6235d01';

            $object['startTime'] = $this->start_time;
            $object['endTime'] = $this->end_time;
            $object['releaseTime'] = $this->release_time;

            $object['topicId'] = $this->topic_id;
            $object['topicName'] = $this->topic_name;

            $object['programId'] = $this->program_id;
            $object['programName'] = $this->program_name;
            $object['programUrl'] = $this->program_url?$this->program_url:'b48eebfae89ea0b1b100d07e7c104220';

            $object['userId'] = $this->user_id?$this->user_id:'';
            $object['userName'] = $this->name?$this->name:'';
            $object['userImg'] = $this->user_img?$this->user_img:'';
            $object['userType'] = $this->user_type?$this->user_type:'';

            $object['commentTotal'] = 0;
            $object['praiseTotal'] = 0;
            $object['isAttention'] = 0;
            $object['isFavorite'] = 0;
            $object['isPraise'] = 0;
        }
        if(env('OPENCACHE', '0')){
            $userId = $this->request->input('userId');
            $mRedis = new MRedis();
            if($userId){
                $ret1 = $mRedis->IsFavoriteContent($userId, $id);
                if($ret1){
                    $object['isFavorite'] = 1;
                }
                $ret2 = $mRedis->IsPraiseContent($userId, $id);
                if($ret2){
                    $object['isPraise'] = 1;
                }
                $ret3 = $mRedis->IsFollowUser($userId, $conUser);
                if($ret3){
                    $object['isAttention'] = 1;
                }
            }
            $num1 = $mRedis->getContentPraiseUserTotal($id);
            if($num1){
                $object['praiseTotal'] = $num1;
            }
            $num2 = $mRedis->getContentCommentUserTotal($id);
            if($num2){
                $object['commentTotal'] = $num2;
            }
        }
        $cUid = with(new CommonUtils())->getUuid($id);
        $shareUrl = env('SHARE_URL','api.cbn.onairm.cn');
        if(FALSE === stripos($shareUrl, 'https://') && FALSE === stripos($shareUrl, 'http://')){
            $shareUrl = 'http://'.$shareUrl;
        }
        $object['shareUrl'] = $shareUrl . '/cbn/getShareContentDetail?ty=web&contentId='.$cUid;
        return $object;
    }
    
    //获取分类下面的内容
    public function getContentList(){
        $categoryId = $this->request->input('categoryId');
        $page = $this->request->input('page');
        $size = $this->request->input('size');
        $contentList = $this->select('content.id','content.img as content_img','content.topic_id','content.program_id','content.user_id','user.name','user.img as user_img','user.type as user_type','content.topic_name','content.program_name','content.content','content.program_url','content.start_time','content.end_time','content.release_time')
                ->join('category_topic','category_topic.topic_id','=','content.topic_id')
                ->join('user','user.id','=','content.user_id')
                ->where('category_topic.category_id', '=', $categoryId)
                ->where('content.status','=',1)
                ->where('content.is_delete','=',0)
                ->orderBy('content.release_time','desc')
                ->groupBy('content.id')
                ->skip($page*$size)
                ->take($size)
                ->get();
        $data = [];
        $data['data'] = [];
        $data['paging']['size'] = 0;
        foreach ($contentList as $content) {
            $content->request = $this->request;
            $data['data'][] = $content->getContentObject();
        }
        
        //临时打乱顺序
        shuffle($data['data']);
        
        if(is_array($data['data'])){
            $data['paging']['size'] = count($data['data']);
        }
        return $data;
    }
    //获取关注人们的内容列表(TV)
    public function getFollowerContentList(){
        $userId = $this->request->input('userId');
        $page = $this->request->input('page');
        $size = $this->request->input('size');
        if(env('OPENCACHE', '0')){
            $mRedis = new MRedis();
            //获取首页关注的内容列表
            $ids = $mRedis->getFollowerContentByUserId($userId, $page, $size);
            $size = count($ids);
            $data = [];
            $data['data'] = [];
            $data['paging']['size'] = 0;
            if($size > 0){
                foreach ($ids as $key => $val){
                    $temp = $this->getRedisContentObject($key);
                    if($temp){
                        $data['data'][] = $temp;
                    }
                }
                if(is_array($data['data'])){
                    $data['paging']['size'] = $size;
                }
            }
            return $data;
        }else{
            $contentList = $this->select('content.id','content.img as content_img','content.topic_id','content.program_id','content.user_id','user.name','user.img as user_img','user.type as user_type','content.topic_name','content.program_name','content.content','content.program_url','content.start_time','content.end_time','content.release_time')
                    ->join('user','user.id','=','content.user_id')
                    ->join('followers','followers.follow_id','=','content.user_id')
                    ->where('followers.user_id', '=', $userId)
                    ->where('content.is_delete','=',0)
                    ->where('content.status','=',1)
                    ->where('user.status','=',1)
                    ->where('user.is_delete','=',0)
                    ->orderBy('content.release_time','desc')
                    ->skip($page*$size)
                    ->take($size)
                    ->get();
            $data = [];
            $data['data'] = [];
            $data['paging']['size'] = 0;
            foreach ($contentList as $content) {
                $content->request = $this->request;
                $data['data'][] = $content->getContentObject();
            }
            if(is_array($data['data'])){
                $data['paging']['size'] = count($data['data']);
            }
            return $data;
        }
    }
    //获取某人的内容列表(TV)
    public function getUserContentList(){
        $userId = $this->request->input('userId');
        $uId = $this->request->input('uId');
        $page = $this->request->input('page');
        $size = $this->request->input('size');
        if(env('OPENCACHE', '0')){
            $mRedis = new MRedis();
            //用户id数组
            $ids = $mRedis->getContentRByUserId($uId, $page, $size);
            $size = count($ids);
            $data = [];
            $data['data'] = [];
            $data['paging']['size'] = 0;
            if($size > 0){
                foreach ($ids as $key => $val){
                    $temp = $this->getRedisContentObject($key);
                    if($temp){
                        $data['data'][] =$temp;
                    }
                }
                if(is_array($data['data'])){
                    $data['paging']['size'] = $size;
                }
            }
            return $data;
        }else{
            $contentList = $this->select('content.id','content.img as content_img','content.topic_id','content.program_id','content.user_id','user.name','user.img as user_img','user.type as user_type','content.topic_name','content.program_name','content.content','content.program_url','content.start_time','content.end_time','content.release_time')
                    ->join('user','user.id','=','content.user_id')
                    ->where('user.id', '=', $uId)
                    ->where('user.status','=',1)
                    ->where('user.is_delete','=',0)
                    ->where('content.status','=',1)
                    ->where('content.is_delete','=',0)
                    ->where('content.status','=',1)
                    ->orderBy('content.release_time','desc')
                    ->skip($page*$size)
                    ->take($size)
                    ->get();
            $data = [];
            $data['data'] = [];
            $data['paging']['size'] = 0;
            foreach ($contentList as $content) {
                $content->request = $this->request;
                $data['data'][] = $content->getContentObject();
            }
            if(is_array($data['data'])){
                $data['paging']['size'] = count($data['data']);
            }
            return $data;
        }
    }
    //发布内容
    //        topicId	是	string		话题ID
    //        topic	是	string		话题名称
    //        programId	是	string		节目id
    //        programName	是	string		节目名称
    //        url	是	string		节目链接
    //        seekTime	是	number		暂停时间秒数
    //        img	是	string		视频截图
    //        content	是	string		内容
    public function createContent(){
        $userId = $this->request->input('userId');
        $topicId = $this->request->input('topicId');
        $topic = $this->request->input('topic');
        $programId = $this->request->input('programId');
        $programName = $this->request->input('programName');
        $url = $this->request->input('url');
        $starTime = $this->request->input('starTime');
        $endTime = $this->request->input('endTime');
        $content = $this->request->input('content');
        $img = $this->request->input('img');
        $videoTime = 30;
//        XCTVRelation
        DB::beginTransaction();
        try {
            $obj = new Content();
            $obj->topic_id = $topicId;
            $obj->program_id = $programId;
            $obj->user_id = $userId;
            $obj->topic_name = $topic;
            $obj->program_name = $programName;
            $obj->content = $content;
            $obj->program_url = $url;
            $obj->img = $img;
            $obj->start_time = $starTime;
            $obj->end_time = $endTime;
//            $obj->end_time = $videoTime;
            $obj->release_time = time();
            $obj->status = 1;
            $obj->is_delete = 0;
            $obj->save();
            DB::commit();
            if(env('OPENCACHE', '0')){
                $mRedis = new MRedis();
                $ret = $mRedis->handleEntity('add', 'content', $obj->id);
                if(!$ret){
                    return FALSE;
                }
                $temp = $this->getRedisContentObject($obj->id);
                $data = [];
                if($temp){
                    $data = $temp;
                }
                return $data;
            }
        } catch (\Exception $ex) {
            DB::rollBack();
            return FALSE;
        }
        return TRUE;
    }
    //获取检索结果 1:话题,2:用户,3:视频,4:内容
    public function getSearchList(){
        $type = $this->request->input('type');
        switch ($type){
            case 1:
                $topic = new Topic();
                $topic->request = $this->request;
                $data = $topic->getSearchTopic();
                break;
            case 2:
                $user = new User();
                $user->request = $this->request;
                $data = $user->getSearchUser();
                break;
            case 3:
                $program = new Program();
                $program->request = $this->request;
                $data = $program->getSearchProgram();
                break;
            case 4:
                $data = $this->getSearchContent();
                break;
        }
        return $data;
    }
    //获取检索内容结果
    public function getSearchContent(){
        $keywords = $this->request->input('keywords');
        $keywords = trim($keywords);
        $page = $this->request->input('page');
        $size = $this->request->input('size');
        $dataList = $this->select('content.id','content.img as content_img','content.topic_id','content.program_id','content.user_id','user.name','user.img as user_img','user.type as user_type','content.topic_name','content.program_name','content.content','content.program_url','content.start_time','content.end_time','content.release_time')
                ->join('user','user.id','=','content.user_id')
                ->where( function($query) use($keywords){
                    $keywords_arr = explode(',', $keywords);
                    foreach ($keywords_arr as $key => $value) {
                        $value = trim($value);
                        if($key === 0){
                            $query->where('content.topic_name','like','%'.$value.'%')
                                ->orWhere('content.program_name','like','%'.$value.'%')
                                ->orWhere('user.name','like','%'.$value.'%');
                        }else{
                            $query->orWhere('content.topic_name','like','%'.$value.'%')
                                ->orWhere('content.program_name','like','%'.$value.'%')
                                ->orWhere('user.name','like','%'.$value.'%');
                        }
                    }
                })
                 ->where('user.status','=',1)
                ->where('user.is_delete','=',0)
                ->where('content.status','=',1)
                ->where('content.is_delete','=',0)
                ->orderBy('release_time','desc')
                ->skip($page*$size)
                ->take($size)
                ->get();
        $data = [];
        $data['data'] = [];
        $data['paging']['size'] = 0;
        if($dataList){
            foreach ($dataList as $val) {
                $val->request = $this->request;
                $data['data'][] = $val->getContentObject();
            }
            if(is_array($data['data'])){
                $data['paging']['size'] = count($data['data']);
            }
        }
        return $data;
    }
    //获取话题下的内容列表
    public function getTopicContentList(){
        $topicId = $this->request->input('topicId');
        $page = $this->request->input('page');
        $size = $this->request->input('size');
        if(env('OPENCACHE', '0')){
            $mRedis = new MRedis();
            //用户id数组
            $ids = $mRedis->getContentRByTopicId($topicId, $page, $size);
            $size = count($ids);
            $data = [];
            $data['data'] = [];
            $data['paging']['size'] = 0;
            if($size > 0){
                foreach ($ids as $key => $val){
                    $temp = $this->getRedisContentObject($key);
                    if($temp){
                        $data['data'][] =$temp;
                    }
                }
                if(is_array($data['data'])){
                    $data['paging']['size'] = $size;
                }
            }
            return $data;
        }else{
            $contentList = $this->select('content.id','content.img as content_img','content.topic_id','content.program_id','content.user_id','user.name','user.img as user_img','user.type as user_type','content.topic_name','content.program_name','content.content','content.program_url','content.start_time','content.end_time','content.release_time')
                    ->join('user', 'user.id','=','content.user_id')
                    ->where('user.status','=',1)
                    ->where('user.is_delete','=',0)
                    ->where('content.status','=',1)
                    ->where('content.is_delete','=',0)
                    ->where('content.topic_id','=',$topicId)
                    ->orderBy('content.release_time','desc')
                    ->skip($page*$size)
                    ->take($size)
                    ->get();
            $data = [];
            $data['data'] = [];
            $data['paging']['size'] = 0;
            foreach ($contentList as $content) {
                $content->request = $this->request;
                $data['data'][] = $content->getContentObject();
            }
            if(is_array($data['data'])){
                $data['paging']['size'] = count($data['data']);
            }
            return $data;
        }
    }
    //获取电影下的内容列表
    public function getProgramContentList(){
        $programId = $this->request->input('programId');
        $page = $this->request->input('page');
        $size = $this->request->input('size');
        if(env('OPENCACHE', '0')){
            $mRedis = new MRedis();
            //用户id数组
            $ids = $mRedis->getContentRByProgramId($programId, $page, $size);
            $size = count($ids);
            $data = [];
            $data['data'] = [];
            $data['paging']['size'] = 0;
            if($size > 0){
                foreach ($ids as $key => $val){
                    $temp = $this->getRedisContentObject($key);
                    if($temp){
                        $data['data'][] =$temp;
                    }
                }
                if(is_array($data['data'])){
                    $data['paging']['size'] = $size;
                }
            }
            return $data;
        }else{
            $contentList = $this->select('content.id','content.img as content_img','content.topic_id','content.program_id','content.user_id','user.name','user.img as user_img','user.type as user_type','content.topic_name','content.program_name','content.content','content.program_url','content.start_time','content.end_time','content.release_time')
                    ->join('user', 'user.id','=','content.user_id')
                    ->where('user.status','=',1)
                    ->where('user.is_delete','=',0)
                    ->where('content.status','=',1)
                    ->where('content.is_delete','=',0)
                    ->where('content.program_id','=',$programId)
                    ->orderBy('content.release_time','desc')
                    ->skip($page*$size)
                    ->take($size)
                    ->get();
            $data = [];
            $data['data'] = [];
            $data['paging']['size'] = 0;
            foreach ($contentList as $content) {
                $content->request = $this->request;
                $data['data'][] = $content->getContentObject();
            }
            if(is_array($data['data'])){
                $data['paging']['size'] = count($data['data']);
            }
            return $data;
        }
    }
    //获取内容详情页
    public function getContentDetail(){
        $contentId = $this->request->input('contentId');
        if(env('OPENCACHE', '0')){
            return $this->getRedisContentObject($contentId);
        }
        $content = $this->select('content.id','content.img as content_img','content.topic_id','content.program_id','content.user_id','user.name','user.img as user_img','user.type as user_type','content.topic_name','content.program_name','content.content','content.program_url','content.start_time','content.end_time','content.release_time')
                ->join('user', 'user.id','=','content.user_id')
                ->where('user.status','=',1)
                ->where('user.is_delete','=',0)
                ->where('content.status','=',1)
                ->where('content.is_delete','=',0)
                ->where('content.id','=',$contentId)
                ->first();
        if($content){
            $content->request = $this->request;
            return $content->getContentObject();
        }
        return FALSE;
    }
    //是否点赞
    public function isPraise(){
        $userId = $this->request->input('userId');
        $type = $this->request->input('type');
        $itemId = $this->request->input('itemId');
        if(env('OPENCACHE', '0')){
            $mRedis = new MRedis();
            if($type ==1){//内容
                $ret = $mRedis->IsPraiseContent($userId, $itemId);
                if($ret){
                    return ['status'=> 0,'msg'=>"已点赞"];
                }
                return ['status'=> 1,'msg'=>"未点赞"];
            }else if($type ==2){//评论
                $ret = $mRedis->IsPraiseComment($userId, $itemId);
                if($ret){
                    return ['status'=> 0,'msg'=>"已点赞"];
                }
                return ['status'=> 1,'msg'=>"未点赞"];
            }
        }else{
            $praise = Praise::select('id','is_delete')
                ->where('type','=',$type)
                ->where('is_delete','=',0)
                ->where('item_id','=',$itemId)
                ->first();
            if($praise){
                return ['status'=> 0,'msg'=>"已点赞"];
            }
            return ['status'=> 1,'msg'=>"未点赞"];
        }
    }
    //点赞
    public function addPraise(){
        $userId = $this->request->input('userId');
        $type = $this->request->input('type');
        $itemId = $this->request->input('itemId');
        
        if(env('OPENCACHE', '0')){
            $mRedis = new MRedis();
            if($type ==1){//内容
                $ret = $mRedis->userPraiseContent('add',$userId, $itemId);
                return $ret;
            }else if($type ==2){//评论
                $ret = $mRedis->userPraiseComment('add',$userId, $itemId);
                return $ret;
            }
        }else{
            //内容点赞
            $contentStats = new ContentStats();
            $contentStats->request = $this->request;
            $ret = $contentStats->creatPraise();
            if($ret){
                $praise = Praise::select('id','is_delete')
                    ->where('type','=',$type)
                    ->where('item_id','=',$itemId)
    //                ->where('is_delete','=',0)
                    ->first();
                if($praise){
                    if($praise->is_delete == 1){
                        $praise->is_delete = 0;
                        $praise->save();
                    }
                }else{
                    $praise = new Praise();
                    $praise->user_id = $userId;
                    $praise->item_id = $itemId;
                    $praise->type = $type;
                    $praise->save();
                }
                return TRUE;
            }else{
                return FALSE;
            }
        }
    }
    //取消点赞
    public function cancelPraise(){
        $userId = $this->request->input('userId');
        $type = $this->request->input('type');
        $itemId = $this->request->input('itemId');
        if(env('OPENCACHE', '0')){
            $mRedis = new MRedis();
            if($type ==1){//内容
                $ret = $mRedis->userPraiseContent('delete',$userId, $itemId);
                return $ret;
            }else if($type ==2){//评论
                $ret = $mRedis->userPraiseComment('delete',$userId, $itemId);
                return $ret;
            }
        }else{
            //内容点赞
            $contentStats = new ContentStats();
            $contentStats->request = $this->request;
            $ret = $contentStats->cancelPraise();
            if($ret){
                $praise = Praise::select('id','is_delete')
                    ->where('type','=',$type)
                    ->where('item_id','=',$itemId)
    //                ->where('is_delete','=',0)
                    ->first();
                if($praise){
                    if($praise->is_delete == 0){
                        $praise->is_delete = 1;
                        $praise->save();
                    }
                }
                return TRUE;
            }else{
                return FALSE;
            }
        }
    }
    //获取推荐下的内容列表
    public function getRecommendContentList(){
        $page = $this->request->input('page');
        $size = $this->request->input('size');
        $contentList = $this->select('content.id','content.img as content_img','content.topic_id','content.program_id','content.user_id','user.name','user.img as user_img','user.type as user_type','content.topic_name','content.program_name','content.content','content.program_url','content.start_time','content.end_time','content.release_time')
                ->join('user', 'user.id','=','content.user_id')
                ->where('user.status','=',1)
                ->where('user.is_delete','=',0)
                ->where('content.is_delete','=',0)
                ->orderBy('content.sort','asc')
                ->orderBy('content.release_time','desc')
                ->skip($page*$size)
                ->take($size)
                ->get();
        $data = [];
        $data['data'] = [];
        $data['paging']['size'] = 0;
        if($contentList){
            foreach ($contentList as $content) {
                $content->request = $this->request;
                $data['data'][] = $content->getContentObject();
            }
            if(is_array($data['data'])){
                $data['paging']['size'] = count($data['data']);
            }
        }
        return $data;
    }
    
    //获取redis的实体数据
    public function getRedisContentObject($Id){
        $mRedis = new MRedis();
        $Obj = $mRedis->getEntity('content', $Id);//获取实体
        if(!$Obj && $Id){
            Log::info("getRedisCommentObject:去数据库,id=".$Id);
            $ObjData = $this->select('content.id','content.img as content_img','content.topic_id','content.program_id','content.user_id','user.name','user.img as user_img','user.type as user_type','content.topic_name','content.program_name','content.content','content.program_url','content.start_time','content.end_time','content.release_time')
                    ->join('user','user.id','=','content.user_id')
                    ->where('content.id', '=', $Id)
                    ->where('user.status','=',1)
                    ->where('user.is_delete','=',0)
                    ->where('content.status','=',1)
                    ->first();
            if($ObjData){
                $ObjData->request = $this->request;
                return $ObjData->getContentObject();
            }else{
                return FALSE;
            }
        }else{
//            Log::info("查缓存:".$cId);
//            $c = new Content();
//            $c->request = $this->request;
            return $this->getContentObject($Obj);
        }
    }
    
    //写入redis的实体数据
    public function setRedisContentObject(){
//        $ObjData = Comment::select('*')
//                ->where('is_delete','=',0)
//                ->where('status','=',1)
//                ->get();
//        $ObjData = Content::select('*')
//                ->where('is_delete','=',0)
//                ->where('status','=',1)
//                ->get();
//        $ObjData = User::select('*')
//                ->where('is_delete','=',0)
//                ->where('status','=',1)
//                ->get();
//        $ObjData = Topic::select('*')
//                ->where('is_delete','=',0)
//                ->where('status','=',1)
//                ->get();
        $ObjData = Program::select('*')
//                ->whereIn('id',['12','6'])
                ->where('is_delete','=',0)
                ->get();
        $mRedis = new MRedis();
        $itemArr = [];
        foreach ($ObjData as $key=>$val){
            $ret = $mRedis->handleEntity('add', 'program', $val->id);
            $itemArr[] = $val->id;
        }
        Log::info("加入redis缓存:". json_encode($itemArr));
        return TRUE;
    }
    
}