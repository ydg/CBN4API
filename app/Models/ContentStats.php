<?php  namespace App\Models;

class ContentStats extends BaseModel{
    
	//表明
	protected $table = 'content_stats';
	//主键
	protected $primaryKey = 'id';
    public function creatPraise(){
        $userId = $this->request->input('userId');
        $type = $this->request->input('type');
        $itemId = $this->request->input('itemId');
//        userPraiseContent
        $obj = $this->select('id','item_id','comment_total','play_total','praise_total','share_total','type','is_delete')
            ->where('item_id','=',$itemId)
            ->where('type','=',$type)
//            ->where('is_delete','=',0)
            ->first();
        if($obj){
            $obj->praise_total += 1;
            $obj->is_delete = 0;
            $obj->save();
        }else{
            $newobj = new ContentStats();
            $newobj->praise_total = 1;
            $newobj->type = $type;
            $newobj->save();
        }
        return TRUE;
    }
    public function cancelPraise(){
        $userId = $this->request->input('userId');
        $type = $this->request->input('type');
        $itemId = $this->request->input('itemId');
        $obj = $this->select('id','item_id','comment_total','play_total','praise_total','share_total','type','is_delete')
            ->where('item_id','=',$itemId)
            ->where('type','=',$type)
            ->first();
        if($obj){
            $obj->praise_total -= 1;
            if($obj->praise_total < 0){
                $obj->praise_total = 1;
            }
            $obj->is_delete = 0;
            $obj->save();
        }else{
            $newobj = new ContentStats();
            $newobj->praise_total = 0;
            $newobj->type = $type;
            $newobj->save();
        }
        return TRUE;
    }
}