<?php  namespace App\Models;

use App\Cache\MRedis;
use Log;
class Favorite extends BaseModel{
    
	//表明
	protected $table = 'favorite';
	//主键
	protected $primaryKey = 'id';
	
    //是否收藏
    public function isFavarite(){
        $id = $this->request->input('id');
        $userId = $this->request->input('userId');
        if(env('OPENCACHE', '0')){
            $mRedis = new MRedis();
            $ret = $mRedis->IsFavoriteContent($userId, $id);
            if($ret){
                return ['status'=> 1,'msg'=>'已收藏'];
            }
            return ['status'=> 0,'msg'=>'未收藏'];
        }else{
            $model = $this->select('id','user_id','content_id','is_delete','updated_at')
                    ->where('user_id','=',$userId)
                    ->where('content_id','=',$id)
                    ->first();
            if($model){
                if($model->is_delete == 0){
                    return ['status'=> 1];
                }
            }
            return ['status'=> 0];
        }
    }
    
    //收藏
    public function favarite() {
        $userId = $this->request->input('userId');
        $id = $this->request->input('id');
        if(!$userId || !$id){
            return FALSE;
        }
        if(env('OPENCACHE', '0')){
            $mRedis = new MRedis();
            $ret = $mRedis->userFavoriteContent('add', $userId, $id);
            return $ret;
        }else{
            $model = $this->select('id','user_id','content_id','is_delete','updated_at')
                    ->where('user_id','=',$userId)
                    ->where('content_id','=',$id)
                    ->first();
            $ret = FALSE;
            if($model){
                if($model->is_delete == 1){
                    $model->is_delete = 0;
                    $ret = $model->save();
                }
            }else{
                $model = new Favorite();
                $model->user_id = $userId;
                $model->content_id = $id;
                $ret = $model->save();
            }
            return $ret;
        }
    }

    //取消收藏
    public function deleteFavarite() {
        $userId = $this->request->input('userId');
        $id = $this->request->input('id');
        if(env('OPENCACHE', '0')){
            $mRedis = new MRedis();
            $ret = $mRedis->userFavoriteContent('delete', $userId, $id);
            return $ret;
        }else{
            $model = $this->select('id','user_id','content_id','is_delete','updated_at')
                    ->where('user_id','=',$userId)
                    ->where('content_id','=',$id)
                    ->first();
            $ret = FALSE;
            if($model){
                if($model->is_delete == 0){
                    $model->is_delete = 1;
                    $ret = $model->save();
                }
            }
            return $ret;
        }
    }

    //取消全部收藏
    public function deleteAllFavarite() {
        $userId = $this->request->input('userId');
        if(env('OPENCACHE', '0')){
            $mRedis = new MRedis();
            $ret = $mRedis->userFavoriteContent('deleteAll', $userId, 0);
            return $ret;
        }else{
            $model = $this->select('id','user_id','content_id','is_delete')
                    ->where('user_id','=',$userId)
                    ->update(['is_delete'=>1]);
            if(!$model){
                return FALSE;
            }
            return TRUE;
        }
    }
    
    //获取用户收藏的列表
    public function getFavariteList() {
        $userId = $this->request->input('userId');
        $uId = $this->request->input('uId');
        $page = $this->request->input('page');
        $size = $this->request->input('size');
        if(env('OPENCACHE', '0')){
            $mRedis = new MRedis();
            $ids = $mRedis->getFavoriteContentIdsByUserId($uId, $page, $size);
            $size = count($ids);
            $data = [];
            $data['data'] = [];
            $data['paging']['size'] = 0;
            if($size > 0){
                foreach ($ids as $key => $val){
                    $temp = with(new Content($this->request))->getRedisContentObject($key);
                    if($temp){
                        $data['data'][] = $temp;
                    }
                }
                if(is_array($data['data'])){
                    $data['paging']['size'] = $size;
                }
            }
            return $data;
        }else{
            $followers = Content::select('content.id','content.img as content_img','content.topic_id','content.program_id','content.user_id','user.name','user.img as user_img','user.type as user_type','content.topic_name','content.program_name','content.content','content.program_url','content.start_time','content.video_time','content.release_time')
                    ->where('content.is_delete','=',0)
                    ->join('favorite','favorite.content_id','=','content.id')
    //                ->join('category_topic','category_topic.topic_id','=','content.topic_id')
                    ->join('user','user.id','=','content.user_id')
                    ->where('favorite.user_id','=',$uId)
                    ->where('favorite.is_delete','=',0)
                    ->orderBy('favorite.updated_at','desc')
                    ->skip($page*$size)
                    ->take($size)
                    ->get();
            $data = array();
            $data['data'] = array();
            $data['paging']['size'] = 0;
            foreach ($followers as $key => $follower) {
                $follower->request = $this->request;
                $data['data'][] = $follower->getContentObject();
            }
            if(isset($data['data'])){
                $data['paging']['size'] = count($data['data']);
            }
            return $data;
        }
    }
}