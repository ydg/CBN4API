<?php  namespace App\Models;

use App\Cache\MRedis;
use Log;

class Followers extends BaseModel{
    
	//表明
	protected $table = 'followers';
	//主键
	protected $primaryKey = 'id';
	
    //是否关注人
    public function isFollow(){
        $uId = $this->request->input('uId');
        $userId = $this->request->input('userId');
        if(env('OPENCACHE', '0')){
            $mRedis = new MRedis();
            $ret = $mRedis->IsFollowUser($userId, $uId);
            if($ret){
                return ['status'=> 1,'msg'=>'已关注'];
            }else{
                return ['status'=> 0,'msg'=>'未关注'];
            }
        }else{
            $model = $this->select('id','user_id','follow_id','is_delete','updated_at')
                    ->where('user_id','=',$userId)
                    ->where('follow_id','=',$uId)
                    ->first();
            if($model){
                if($model->is_delete == 0){
                    return ['status'=> 1];
                }
            }
            return ['status'=> 0];
        }
    }
    
    //关注人
    public function follow(){
        $uId = $this->request->input('uId');
        $userId = $this->request->input('userId');
        if($uId == $userId){
            return FALSE;
        }
        if(!$userId || !$uId){
            return FALSE;
        }
        $uIdArr = explode(',',$uId); 
        if(env('OPENCACHE', '0')){
            foreach ($uIdArr as $key=>$val){
                $mRedis = new MRedis();
                $ret = $mRedis->userFollowUser('add', $userId, $val);
                if(!$ret){
//                    return ['status'=> 1,'msg'=>'出错了'];
                    return FALSE;
                }
            }
            $sum = $mRedis->getFollowerTotal($userId);
            if($sum){
                return ['followTotal'=> $sum];
            }
        }else{
            foreach ($uIdArr as $key=>$val){
                $model = $this->select('id','user_id','follow_id','is_delete','updated_at')
                        ->where('user_id','=',$userId)
                        ->where('follow_id','=',$val)
                        ->first();
                $ret = FALSE;
                if($model){
                    if($model->is_delete == 1){
                        $model->is_delete = 0;
                        $ret = $model->save();
                    }
                }else{
                    $model = new Followers();
                    $model->user_id = $userId;
                    $model->follow_id = $val;
                    $ret = $model->save();
                }
                 if(!$ret){
                    return FALSE;
                }
            }
            return TRUE;
        }
    }
    
    //取消关注人
    public function deleteFollow(){
        $uId = $this->request->input('uId');
        $userId = $this->request->input('userId');
        if(env('OPENCACHE', '0')){
            $mRedis = new MRedis();
            $ret = $mRedis->userFollowUser('delete', $userId, $uId);
            if($ret){
                $sum = $mRedis->getFollowerTotal($userId);
                return ['followTotal'=> $sum];
            }
            return FALSE;
        }else{
            $model = $this->select('id','user_id','follow_id','is_delete','updated_at')
                    ->where('user_id','=',$userId)
                    ->where('follow_id','=',$uId)
                    ->first();
            $ret = TRUE;
            if($model){
                if($model->is_delete == 0){
                    $model->is_delete = 1;
                    $ret = $model->save();
                }
            }
            if(!$ret){
                return FALSE;
            }
            return TRUE;
        }
    }
    
    //取消所有关注人
    public function deleteAllFollow(){
        $userId = $this->request->input('userId');
        if(env('OPENCACHE', '0')){
            $mRedis = new MRedis();
            $ret = $mRedis->userFollowUser('deleteAll', $userId, 0);
            if(!$ret){
                return ['status'=> 1,'msg'=>'出错了'];
            }
            $sum = $mRedis->getFollowerTotal($userId);
            return ['followTotal'=> $sum];
//            return ['status'=> 0,'msg'=>'已取消关注'];
        }else{
            $model = $this->select('id','user_id','follow_id','is_delete','updated_at')
                    ->where('user_id','=',$userId)
                    ->update(['is_delete'=>1]);
            if(!$model){
                return FALSE;
            }
            return TRUE;
        }
    }
    
    //获取用户关注人的列表(TV)
    public function getFollowerList() {
        $userId = $this->request->input('userId');
        $uId = $this->request->input('uId');
        $page = $this->request->input('page');
        $size = $this->request->input('size');
        if(env('OPENCACHE', '0')){
            $mRedis = new MRedis();
            //判断取列表
            $ids = $mRedis->getFollowerByUserId($uId, $page, $size);
            $size = count($ids);
            $data = [];
            $data['data'] = [];
            $data['paging']['size'] = 0;
            if($size > 0){
                foreach ($ids as $key => $val){
                    $temp = with(new User($this->request))->getRedisUserObject($key);
                    if($temp){
                        $data['data'][] = $temp;
                    }
                }
                if(is_array($data['data'])){
                    $data['paging']['size'] = $size;
                }
            }
            return $data;
        }else{
            $followers = User::select('user.id','user.name','user.type','user.img','user.phone')
                    ->where('user.is_delete','=',0)
                    ->where('user.status','=',1)
                    ->join('followers','followers.follow_id','=','user.id')
                    ->where('followers.is_delete','=',0)
                    ->where('followers.user_id','=',$uId)
                    ->orderBy('followers.updated_at','desc')
                    ->skip($page*$size)
                    ->take($size)
                    ->get();
            $data = array();
            $data['data'] = array();
            $data['paging']['size'] = 0;
            foreach ($followers as $key => $follower) {
                $follower->request = $this->request;
                $data['data'][] = $follower->getUserObject();
            }
            if(isset($data['data'])){
                $data['paging']['size'] = count($data['data']);
            }
            return $data;
        }
    }
    
    //获取用户的粉丝列表(TV)
    public function getFansList() {
        $userId = $this->request->input('userId');
        $uId = $this->request->input('uId');
        $page = $this->request->input('page');
        $size = $this->request->input('size');
        if(env('OPENCACHE', '0')){
            $mRedis = new MRedis();
            //判断取列表
            $ids = $mRedis->getFansByUserId($uId, $page, $size);
            $size = count($ids);
            $data = [];
            $data['data'] = [];
            $data['paging']['size'] = 0;
            if($size > 0){
                foreach ($ids as $key => $val){
                    $temp = with(new User($this->request))->getRedisUserObject($key);
                    if($temp){
                        $data['data'][] = $temp;
                    }
                }
                if(is_array($data['data'])){
                    $data['paging']['size'] = $size;
                }
            }
            return $data;
        }else{
            $fans = User::select('user.id','user.name','user.type','user.img','user.phone')
                    ->where('user.is_delete','=',0)
                    ->where('user.status','=',1)
                    ->join('followers','followers.follow_id','=','user.id')
                    ->where('followers.is_delete','=',0)
                    ->where('followers.follow_id','=',$uId)
                    ->orderBy('followers.updated_at','desc')
                    ->skip($page*$size)
                    ->take($size)
                    ->get();
            $data = array();
            $data['data'] = array();
            $data['paging']['size'] = 0;
            foreach ($fans as $key => $fan) {
                $fan->request = $this->request;
                $data['data'][] = $fan->getUserObject();
            }
            if(isset($data['data'])){
                $data['paging']['size'] = count($data['data']);
            }
            return $data;
        }
    }
   
}