<?php  namespace App\Models;

use App\Models\User;
use Log;
class QRcode extends BaseModel{
    
	//表明
	protected $table = 'qrcode';
	//主键
	protected $primaryKey = 'id';
	
    public function getUserObject(){
        $object = array();
        $object['userId'] = $this->id;
        $object['nickname'] = $this->name;
        $object['userType'] = $this->type;
        $object['userIcon'] = $this->img;
        $object['slogan'] = $this->slogan;
        $object['birthday'] = $this->birthday;
        $object['gender'] = $this->gender;
        $object['isFollow'] = 0; //是否关注 0 未关注 1 关注
        $object['fansTotal'] = 0;
        $object['followTotal'] = 0;
        return $object;
    }
    
    public function getQRcode(){
        $deviceId = $this->request->input('dk');
        $uniqueId = "CBN4API=".$this->generateNum();
        $qrcode = $this->select('id','uniqueId','user_id','status','is_delete')
               ->where('is_delete','=',0)
               ->where('device_id','like',$deviceId)
               ->first();
        if($qrcode){
            $qrcode->uniqueId = $uniqueId;
            $qrcode->device_id = $deviceId;
            $qrcode->status = 0;
            $ret = $qrcode->save();
            if(!$ret){
                return FALSE;
            }
            return ['uniqueId'=>$uniqueId];
        }else{
            $qrcode = new QRcode();
            $qrcode->uniqueId = $uniqueId;
            $qrcode->device_id = $deviceId;
            $qrcode->status = 0;
            $ret = $qrcode->save();
            if(!$ret){
                return FALSE;
            }
            return ['uniqueId'=>$uniqueId];
        }
    }
    
    public function getQRcodeStatus(){
        $uniqueId = $this->request->input('uniqueId');
        $deviceId = $this->request->input('dk');
        if(FALSE === strpos($uniqueId, "CBN4API=")){
            return FALSE;
        }
        $qr = $this->select('id','uniqueId','user_id','status','is_delete','updated_at')
                ->where('is_delete','=',0)
                ->where('uniqueId','like',$uniqueId)
                ->where('device_id','like',$deviceId)
                ->first();
        if($qr){
            if(time() - $qr->updated_at->timestamp > 5*60){
                return 4;//二维码超时重新获取
            }
            if($qr->status == 1){//已扫码
                $user = User::select('id','uid','token','img','name','phone','slogan','birthday','gender','type','status','is_delete')
                        ->where('status','=',1)
                        ->where('is_delete','=',0)
                        ->where('id','=',$qr->user_id)
                        ->first();
                if($user){
                    $token = md5(uniqid(time()));
                    $ret = FALSE;
                    if($user->token){//有数据
                        $ret = $this->isLoginLimit($user->token);//是否登录设备超过限制
                    }
                    if($ret){
                        return 2;//超过设备数量限制
                    }
                    $obj = json_decode($user->token);
                    if(isset($obj->$deviceId)){
                        $obj->$deviceId->tn = $token;
                        $obj->$deviceId->time = time();
                    }else{
                        $obj->$deviceId = array(
                            "tn"=>$token,   //32位串
                            "time"=>time(),
                        );
                    }
                    $user->token = json_encode($obj);
                    $user->save();
                    $qr->status = 2;
                    $qr->save();
                    return $user->getMyUserObject($deviceId);
                }else{
                    return 1;//扫码未成功继续监听
                }
            }else{
                return 1;//扫码未成功继续监听
            }
        }
        return 4;//二维码错误
    }
    //手机端扫码
    public function scanQRcode() {
        $uniqueId = $this->request->input('uniqueId');
        $userId = $this->request->input('userId');
        $deviceId = $this->request->input('dk');
        $qr = $this->select('id','uniqueId','user_id','status','is_delete')
               ->where('is_delete','=',0)
               ->where('uniqueId','like',$uniqueId)
               ->first();
        if($qr){
            $qr->status = 1;
            $qr->user_id = $userId;
            $ret = $qr->save();
            return $ret;
        }
        return FALSE;
    }
    //获取唯一序列号
    public static function generateNum() {
        //strtoupper转换成全大写的
        $charid = strtoupper(md5(uniqid(mt_rand(), true)));
        $uuid = substr($charid, 0, 8).substr($charid, 8, 4).substr($charid,12, 4).substr($charid,16, 4).substr($charid,20,12);
        return $uuid;
    }
    //是否登录设备超过限制
    protected function isLoginLimit($jsonstr) {
        $obj = json_decode($jsonstr);
        $t = array();
        if($obj){
            $t = time() - 30*24*60*60;//一个月
            $n = 0;
            foreach ($obj as $key=>$val){
                if($val->time > $t && $val->tn){
                    $n++;
                }
            }
            if($n > 5){
                return 1;//超过登录设备数
            }
            return 0;
        }
        return 0;
    }
}