<?php  namespace App\Models;

use App\Models\Followers;
use App\Models\User;
use App\Models\Topic;
use App\Models\Program;
use App\Models\Content;
use Log;
class RecommendItem extends BaseModel{
    
	//表明
	protected $table = 'recommend_item';
	//主键
	protected $primaryKey = 'id';
    //1 无链接、2 内容、3 话题，4 节目 5 用户
    public function getRecommendObject($type){
        $object = array();
        switch ($type){
            case 3://话题实体
                $obj = new Topic();
                $obj->request = $this->request;
                $object = $obj->getTopicObject();
                break;
            case 5://user实体
                $obj = new User();
                $obj->request = $this->request;
                $object = $obj->getUserObject();
                break;
            case 2://内容实体
                $obj = new Content();
                $obj->request = $this->request;
                $object = $obj->getContentObject();
                break;
            case 1://内容
                $object['recommendId'] = $this->recommend_id;
                $object['title'] = $this->title;
                $object['keywords'] = $this->tags;
                $object['img'] = $this->img;
                $object['linkType'] = $this->link_type;
                $object['link'] = $this->link;
                break;
        }
        return $object;
    }
    //获取推荐位话题
    public function getRecommendItemList(){
        $tag = $this->request->input('tag');
        $type = $this->request->input('type');
        $recommend = Recommend::select('id','title')
                    ->where('type','=',$tag)
                    ->where('status','=',1)
                    ->where('is_delete','=',0)
                    ->first();
        $recommendId = 0;
        if($recommend){
            $recommendId = $recommend->id;
            $recommendTitle = $recommend->title;
        }
        $dataList = '';
        switch ($type){
            case 3://话题
                $dataList = Topic::select('topic.id','topic.title','topic.img','topic.introduction','topic.keywords','topic.created_by','topic.sort','topic.status','user.name as userName','user.id as userId','recommend_item.link')
                    ->join('recommend_item','recommend_item.link','=','topic.id')
                    ->where('recommend_item.link_type','=',$type)
                    ->where('topic.status', '=', 1)
                    ->where('topic.is_delete', '=', 0)
                    ->where('recommend_item.recommend_id','=',$recommendId)
                    ->where('recommend_item.status','=',1)
                    ->where('recommend_item.is_delete','=',0)
                    ->orderBy('recommend_item.sort','asc')
                    ->get();
                break;
            case 5://用户
                $dataList = User::select('user.id', 'user.img','user.name','user.phone','user.slogan','user.birthday','user.gender','user.type','user.status','user.is_delete','recommend_item.link')
                    ->join('recommend_item','recommend_item.link','=','user.id')
                    ->where('recommend_item.link_type','=',$type)
                    ->where('user.is_delete','=',0)
                    ->where('recommend_item.recommend_id','=',$recommendId)
                    ->where('recommend_item.status','=',1)
                    ->where('recommend_item.is_delete','=',0)
                    ->orderBy('recommend_item.sort','asc')
                    ->get();
                break;
            case 4://节目
                $dataList = Program::select('id','name','keywords','video_url','img_broad','img_tall')
                    ->join('recommend_item','recommend_item.link','=','user.id')
                    ->where('recommend_item.link_type','=',$type)
                    ->where('is_delete','=',0)
                    ->where('recommend_item.recommend_id','=',$recommendId)
                    ->where('recommend_item.status','=',1)
                    ->where('recommend_item.is_delete','=',0)
                    ->orderBy('recommend_item.sort','asc')
                    ->get();
                break;
            case 2://内容
                $dataList = Content::select('content.id','content.img as content_img','content.topic_id','content.program_id','content.user_id','user.name','user.img as user_img','user.type as user_type','content.topic_name','content.program_name','content.content','content.program_url','content.start_time','content.end_time','content.release_time')
                    ->join('recommend_item','recommend_item.link','=','content.id')
                    ->join('user','user.id','=','content.user_id')
                    ->where('content.status','=',1)
                    ->where('user.status','=',1)
                    ->where('user.is_delete','=',0)
                    ->where('recommend_item.link_type','=',$type)
                    ->where('content.is_delete','=',0)
                    ->where('recommend_item.recommend_id','=',$recommendId)
                    ->where('recommend_item.status','=',1)
                    ->where('recommend_item.is_delete','=',0)
                    ->orderBy('recommend_item.sort','asc')
                    ->get();
                break;
            case 1://文字
                $dataList = $this->select('id','recommend_id','title','tags','img','link_type','link')
                    ->where('link_type','=',$type)
                    ->where('recommend_id','=',$recommendId)
                    ->where('status','=',1)
                    ->where('is_delete','=',0)
                    ->orderBy('sort','asc')
                    ->get();
                break;
        }
        $data = [];
        $data['title'] = $recommendTitle;
        $data['type'] = (int)$type;
        $data['data'] = [];
        if($dataList){
            foreach ($dataList as $val) {
                switch ($type){
                    case 3:
                        $val->request = $this->request;
                        $data['data'][] = $val->getTopicObject();
                        break;
                    case 5:
                        $val->request = $this->request;
                        $data['data'][] = $val->getUserObject();
                        break;
                    case 2:
                        $val->request = $this->request;
                        $data['data'][] = $val->getContentObject();
                        break;
                    case 1:
                        $val->request = $this->request;
                        $data['data'][] = $val->getRecommendObject($type);
                        break;
                }
            }
            $data['size'] = count($data['data']);
        }
        return $data;
    }
}