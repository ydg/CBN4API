<?php  namespace App\Models;

use App\Cache\MRedis;
use App\Utils\CommonUtils;
use Log;
class Topic extends BaseModel{
    
	//表明
	protected $table = 'topic';
	//主键
	protected $primaryKey = 'id';
    public function getTopicObject($Obj=null){
        $object = array();
        $id;
        if($Obj){
            $id = $Obj['id'];
            $object['topicId'] = $Obj['id'];
            $object['title'] = $Obj['title'];
            
            //一期没有创建者
            $object['userName'] = isset($Obj['userName'])?$Obj['userName']:'';
            $object['userId'] = isset($Obj['userId'])?$Obj['userId']:'';
            
            $object['img'] = $Obj['img']?$Obj['img']:'22ce4b3ee6293652d07d91e30ac0929b';
            $object['introduction'] = $Obj['introduction'];
            $object['keywords'] = $Obj['keywords'];
        }else{
            $id = $this->id;
            $object['topicId'] = $this->id;
            $object['title'] = $this->title;
            $object['userName'] = isset($this->userName)?$this->userName:'';
            $object['userId'] = isset($this->userId)?$this->userId:'';
            $object['img'] = $this->img?$this->img:'22ce4b3ee6293652d07d91e30ac0929b';
            $object['introduction'] = $this->introduction;
            $object['keywords'] = $this->keywords;
        }
        $object['fansTotal'] = 0;
        $object['discussTotal'] = 0;//
        $object['isAttention'] = 0;
        if(env('OPENCACHE', '0')){
            $mRedis = new MRedis();
            $userId = $this->request->input('userId');
            if($userId){
                $ret = $mRedis->IsAttentionTopic($userId, $id);
                if($ret){
                    $object['isAttention'] = 1;
                }
            }
            $sum = $mRedis->getAttentionTopicUserTotal($id);
            if($sum){
                $object['fansTotal'] = $sum;
            }
        }
        $tUid = with(new CommonUtils())->getUuid($id);
        $shareUrl = env('SHARE_URL','api.cbn.onairm.cn');
        if(FALSE === stripos($shareUrl, 'https://') && FALSE === stripos($shareUrl, 'http://')){
            $shareUrl = 'http://'.$shareUrl;
        }
        $object['shareUrl'] = $shareUrl.'/cbn/getShareTopicDetail?ty=web&topicId='.$tUid;
        return $object;
    }
    //获取话题详情
    public function getTopicDetail(){
        $topicId = $this->request->input('topicId');
        if(env('OPENCACHE', '0')){
            return $this->getRedisTopicObject($topicId);
        }
        $topic = $this->select('topic.id','topic.title','topic.img','topic.introduction','topic.keywords','topic.created_by','topic.sort','topic.status')
                ->where('topic.id', '=', $topicId)
                ->where('topic.status', '=', 1)
                ->where('topic.is_delete', '=', 0)
                ->first();
        $data = array();
        if($topic){
            $topic->request = $this->request;
            $data = $topic->getTopicObject();
        }
        return $data;
    }
    //获取
    public function getTopic() {
        $programId = $this->request->input('programId');
        $keywords = $this->request->input('keywords');
        $topicList = $this->select('topic.id','topic.title','topic.img','topic.introduction','topic.keywords','topic.created_by','topic.sort','topic.status','topic.is_delete')
                ->where( function($query) use($keywords){
                    $keywords_arr = explode(',', $keywords);
                    foreach ($keywords_arr as $key => $value) {
                        if($key === 0){
                            $query->where('topic.title','like','%'.$value.'%')
                                ->orWhere('topic.keywords','like','%'.$value.'%');
                        }else{
                            $query->orWhere('topic.title','like','%'.$value.'%')
                                ->orWhere('topic.keywords','like','%'.$value.'%');
                        }
                    }
                })
                ->where('topic.status','=',1)
                ->where('topic.is_delete','=',0)
                ->orderBy('topic.sort','asc')
                ->get();
        $data = [];
        if(count($topicList) <= 0){
//            取不到的话,取前十个话题
            $topicList = $this->select('topic.id','topic.title','topic.img','topic.introduction','topic.keywords','topic.created_by','topic.sort','topic.status','topic.is_delete')
                ->where('topic.status','=',1)
                ->where('topic.is_delete','=',0)
                ->orderBy('topic.sort','asc')
                ->take(10)
                ->get();
        }
        if($topicList){
            foreach ($topicList as $topic) {
                $topic->request = $this->request;
                $data[] = $topic->getTopicObject();
            }
        }
        return $data;
    }
    
    public function getProgramTopicList(){
        $programId = $this->request->input('programId');
        $page = $this->request->input('page');
        $size = $this->request->input('size');
        if(!env('OPENCACHE', '0')){
            $mRedis = new MRedis();
            //获取首页关注的内容列表
            $ids = $mRedis->getTopicRByProgramId($programId, $page, $size);
            $size = count($ids);
            $data = [];
            $data['data'] = [];
            $data['paging']['size'] = 0;
            if($size > 0){
                foreach ($ids as $key => $val){
                    $temp = $this->getRedisTopicObject($key);
                    if($temp){
                        $data['data'][] = $temp;
                    }
                }
                if(is_array($data['data'])){
                    $data['paging']['size'] = $size;
                }
            }
            return $data;
        }else{
            $topicList = $this->select('topic.id','topic.title','topic.img','topic.introduction','topic.keywords','topic.created_by','topic.sort','topic.status','topic.is_delete')
                    ->join('content', 'content.topic_id','=','topic.id')
                    ->where('topic.status','=',1)
                    ->where('topic.is_delete','=',0)
                    ->where('content.status','=',1)
                    ->where('content.is_delete','=',0)
                    ->where('content.program_id','=',$programId)
                    ->groupBy('content.topic_id')
                    ->orderBy('content.release_time','desc')
                    ->skip($page*$size)
                    ->take($size)
                    ->get();
            $data = [];
            $data['data'] = [];
            $data['paging']['size'] = 0;
            foreach ($topicList as $topic) {
                $topic->request = $this->request;
                $data['data'][] = $topic->getTopicObject();
            }
            if(is_array($data['data'])){
                $data['paging']['size'] = count($data['data']);
            }
            return $data;
        }
    }
    
    //获取所有话题 getAllTopic
    public function getAllTopic(){
        $page = $this->request->input('page');
        $size = $this->request->input('size');
        $topicList = $this->select('id','title','img','introduction','keywords','created_by','sort','status','is_delete')
                ->where('status', '=', 1)
                ->where('is_delete', '=', 0)
                ->orderBy('sort','asc')
                ->skip($page*$size)
                ->take($size)
                ->get();
        $data = array();
        $data['data'] = array();
        $data['paging']['size'] = 0;
        if($topicList){
            foreach ($topicList as $key => $val) {
                $val->request = $this->request;
                $data['data'][] = $val->getTopicObject();
            }
            if(isset($data['data'])){
                $data['paging']['size'] = count($data['data']);
            }
        }
        return $data;
    }
    // 获取搜索结果
    public function getSearchTopic() {
        $keywords = $this->request->input('keywords');
        $page = $this->request->input('page');
        $size = $this->request->input('size');
        $contentList = $this->select('topic.id','topic.title','topic.img','topic.introduction','topic.keywords','topic.created_by','topic.sort')
                ->where( function($query) use($keywords){
                    $keywords_arr = explode(',', $keywords);
                    foreach ($keywords_arr as $key => $value) {
                        if($key === 0){
                            $query->where('topic.title','like','%'.$value.'%')
                                ->orWhere('topic.keywords','like','%'.$value.'%');
                        }else{
                            $query->orWhere('topic.title','like','%'.$value.'%')
                                ->orWhere('topic.keywords','like','%'.$value.'%');
                        }
                    }
                })
                ->where('topic.status','=',1)
                ->orderBy('topic.sort','asc')
                ->skip($page*$size)
                ->take($size)
                ->get();
        $data = [];
        $data['data'] = [];
        $data['paging']['size'] = 0;
        if($contentList){
            foreach ($contentList as $content) {
                $content->request = $this->request;
                $data['data'][] = $content->getTopicObject();
            }
            if(is_array($data['data'])){
                $data['paging']['size'] = count($data['data']);
            }
        }
        return $data;
    }
    //获取redis的实体数据
    public function getRedisTopicObject($Id){
        $mRedis = new MRedis();
        $Obj = $mRedis->getEntity('topic', $Id);//获取实体
        if(!$Obj){
            Log::info("getRedisTopicObject:去数据库,id=".$Id);
            $ObjData = $this->select('topic.id','topic.title','topic.img','topic.introduction','topic.keywords','topic.created_by','topic.sort')
                    ->where('topic.id','=',$Id)
                    ->where('topic.status','=',1)
                    ->first();
            if($ObjData){
                $ObjData->request = $this->request;
                return $ObjData->getTopicObject();
            }else{
                return FALSE;
            }
        }else{
            return $this->getTopicObject($Obj);
        }
    }
}