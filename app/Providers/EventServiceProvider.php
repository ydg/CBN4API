<?php

namespace App\Providers;
use App\Models\User;
use App\Models\Favorite;
use App\Models\BookRecord;
use App\Models\Attention;
use App\Models\ViewHistory;
use App\Models\Live;
use App\Models\GiftRecord;
use Log;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        
    ];
    public function boot()
    {
        //以下模型的方法都可以做类似处理
        //creating,  created, updating, updated, saving, saved, deleting, deleted, restoring, restored.
//        User::saved(function($model)//created - updated
//        {
//            $model->saveModel();
//        });
//        User::deleted(function($model)
//        {
//            $model->deleteModel();
//        });
//        
//        Favorite::saved(function($model)//created - updated
//        {
//            $model->saveModel();
//        });
//        Favorite::deleted(function($model)
//        {
//            $model->deleteModel();
//        });
//        
//        BookRecord::saved(function($model)//created - updated
//        {
//            $model->saveModel();
//        });
//        BookRecord::deleted(function($model)
//        {
//            $model->deleteModel();
//        });
//        
//        Attention::saved(function($model)//created - updated
//        {
//            $model->saveModel();
//        });
//        Attention::deleted(function($model)
//        {
//            $model->deleteModel();
//        });
//        
//        ViewHistory::saved(function($model)//created - updated
//        {
//            $model->saveModel();
//        });
//        
//        Live::saved(function($model)//created - updated
//        {
//            $model->saveModel();
//        });
//        
//        GiftRecord::saved(function($model)//created - updated
//        {
//            $model->saveModel();
//        });
//        
        
    }
}
