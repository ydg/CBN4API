<?php
/**
 * This file is part of workerman.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author walkor<walkor@workerman.net>
 * @copyright walkor<walkor@workerman.net>
 * @link http://www.workerman.net/
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * 主逻辑
 * 主要是处理 onMessage onClose 三个方法
 */

use \GatewayWorker\Lib\Gateway;
use \GatewayWorker\Lib\Store;
use \GatewayWorker\Lib\Db;
use GatewayWorker\Lib\StoreDriver;
use Workerman\Lib\Timer;
use Workerman\Worker;
//require_once __DIR__ . '/../../config/Db.php';
//require_once __DIR__ . '/../config/Store.php';
class Events
{
    public static  $stopwordses = '';
    public static  $keyWords_Time = 0;
    public static  $auth_timer_id = 0;
    public static $pTimeKey = 'Raylive_workman_Praise_Time';
    public static $pDataKey = 'Raylive_workman_Praise_Data';
//*******************以下为缓存的Key*******************
//    1.$pTimeKey;//点赞的相关数据
//    2.$pDataKey;//每隔多久就把缓存的数据写入数据库的时间
    
//*******************缓存的Key*******************   
    
    /**
     * 当客户端连上时触发
     * @param int $client_id
     */
    public static function onConnect($client_id)
    {
        $_SESSION['id'] = time();
        global $mem;
        // 临时给$connection对象添加一个auth_timer_id属性存储定时器id
        // 定时30秒关闭连接，需要客户端30秒内发送验证删除定时器
//        self::$auth_timer_id = Timer::add(30, function()use($client_id){
//            self::onClose($client_id);
//        }, null, false);
        echo "连接成功!".$client_id."client:{$_SERVER['REMOTE_ADDR']}:{$_SERVER['REMOTE_PORT']} time:".date("Y-m-d h:i:sa")."\n";
        Gateway::sendToCurrentClient('{"type":"auth","auth_key":'.$_SESSION['id'].'}');
    }
    
    public static function replace($target){
        if(!self::$stopwordses){
                echo 'stopwordses null /n';
                return $target;
        }
        foreach(self::$stopwordses as $stopwords){
                foreach (explode(',', $stopwords['words']) as $word){
                        $replace = $stopwords['rep'] == '*' ? str_repeat('*', mb_strlen($word, 'utf-8')) : $stopwords['rep'];
                        $target = str_replace($word, $replace, $target);
                }
        }
        return $target;
    }
    /**
     * 有消息时
     * @param int $client_id
     * @param string $message
     */
    public static function onMessage($client_id, $message)
    {	
        echo "onMessage开始\n";
//        $Chat_db = Db::instance('Chat_db');
        global $Chat_db;
        $Chat_db->query("set names 'utf8mb4'");
        global $mem;
//        echo "client:{$_SERVER['REMOTE_ADDR']}:{$_SERVER['REMOTE_PORT']} gateway:{$_SERVER['GATEWAY_ADDR']}:{$_SERVER['GATEWAY_PORT']}  client_id:$client_id session:".json_encode($_SESSION)." onMessage:".$message."\n";
        echo "接受数据:client_id:".$client_id." onMessage:".$message." time:".date("Y-m-d h:i:sa")."\n";
        // 客户端传递的是json数据
        $message_data = json_decode($message, true);
        if(!$message_data)
        {
            return ;
        }
//        if(!isset($_SESSION['user_id'])){
//            if(isset($message_data['userId']) && ($message_data['type']=="say" || $message_data['type']=="praise")){
//                $user_info= $Chat_db->select('user_id')->from('user')->where("uuid= '".$message_data['userId']."'")->row();
//                if($user_info){
//                    $_SESSION['user_id'] = $user_info['user_id'];
//                }else{
//                    $_SESSION['user_id'] = '-1';
//                }
//            }
//        }
//        //判断是否为有效信息,type:login和relogin时,可以通过,其他必须有session的room_id即登录过
//        if(!isset($_SESSION['room_id'])){
//            if(($message_data['type'] != "login" && $message_data['type'] != "re_login") )
//            {
//                echo "非法消息,未登录!\n";
//                self::onClose($client_id);
//                echo "\$message_data['room_id'] not set. client_ip:{$_SERVER['REMOTE_ADDR']} \$message:$message time:".date("Y-m-d h:i:sa");
//                return;
//            }
//        }
        //点赞数缓存
        $praiseTime = $mem->get(self::$pTimeKey);
        $praisData = $mem->get(self::$pDataKey);
        if($praiseTime && $praisData){
            if(time() - $praiseTime >= 60){
                echo "超时::!!\n";
                $mem->delete(self::$pDataKey);
                $mem->delete(self::$pTimeKey);
                self::insertIntoLive($praisData);
            }
        }
//        $all_clients = self::addClientToRoom($_SESSION['room_id'], $client_id, "恢复健康都是废话京东客服好");
        // 根据类型执行不同的业务
        switch($message_data['type'])
        {
            // 客户端回应服务端的心跳
            case 'ping':
                return;
            // 客户端登录 message格式: {type:login, name:xx, room_id:1} ，添加到客户端，广播给所有客户端xx进入聊天室
            case 'login':
            case 're_login':
                echo "登录或重登录!\n";
                // 判断是否有房间号
                if(!isset($message_data['room_id']))
                {
                    self::onClose($client_id);
                    echo "\$message_data['room_id'] not set. client_ip:{$_SERVER['REMOTE_ADDR']} \$message:$message time:".date("Y-m-d h:i:sa");
                }
//                if($_SESSION['id'] != $message_data['auth_key']){
//                    echo "authKey错误:".$client_id." time:".date("Y-m-d h:i:sa")."\n";
//                    self::onClose($client_id);
//                    return ;
//                }
                echo "验证成功!\n";
//                // 验证成功，删除定时器，防止连接被关闭
//                Timer::del(self::$auth_timer_id); 
                if(!isset($message_data['live_id']))
                {
                    self::onClose($client_id);
                    echo "\$message_data['live_id'] not set. client_ip:{$_SERVER['REMOTE_ADDR']} \$message:$message time:".date("Y-m-d h:i:sa");
                    return;
                }
               echo "验证123123213成功!\n";
                $room_id = $message_data['room_id'];
                $client_name = isset($message_data['client_name'])?$message_data['client_name']:"悦媒网友".intval(substr($client_id,-7));
                $_SESSION['room_id'] = $room_id;
                $_SESSION['live_id'] = $message_data['live_id'];
                $_SESSION['client_name'] = $client_name;//用户名
                echo "获取客户端列表!\n";
                // 存储到当前房间的客户端列表
                $all_clients = self::addClientToRoom($room_id, $client_id, $client_name);
                echo "客户list";
//                print_r($all_clients);
                // 整理客户端列表以便显示
                $client_list = self::formatClientsData($all_clients);
                echo "客户顿list";
                print_r($client_list);
                $login_tt = 0;// 真实数据
                $login_total = 0; // +set数据
                //在线人数的基数，默认为0
                echo "chat_theme查询开始!\n";
                $chat_theme= $Chat_db->select('set_num,login_num,organization_id,praise_total')->from('chat_theme')->where("cid= '".$room_id."'")->row();
                if(!$chat_theme){
                    echo "参数错误. client_ip:{$_SERVER['REMOTE_ADDR']} $message:".$message." time:".date("Y-m-d h:i:sa")."\n";
                    return;
                }
                if(!self::$stopwordses || time() - self::$keyWords_Time > 300) // 5分钟取一次  更新 $stopwordses
                {
                        self::$keyWords_Time = time();
                        self::$stopwordses = $Chat_db->select('words,`replace` as rep ')->from('stopwords')->where("status=1")->query();	
                }
                echo "chat_theme查询成功!\n";
                $client_tt = count($client_list); // 真实数据当前连接数
                $client_total =$client_tt;// +set数据
                if(isset($chat_theme['set_num'])){
                        $client_total  += $chat_theme['set_num'];	
                } 
                if(isset($chat_theme['login_num'])){
                        $login_tt = $chat_theme['login_num'] + 1;//真实访问量
                        $login_total  = $chat_theme['login_num'] +$chat_theme['set_num'] + 1;	
                }
                $lid = intval($message_data['live_id']);
//                $live = $Chat_db->select('live_id,praise_total')->from('live')->where("live_id=".$lid)->row();
                echo "live查询成功!\n";
                $cachedata = $mem->get(self::$pDataKey);//存放lid和点赞数
                $cachePraiseNum = 0;//缓存里面的点赞数
                $dbPraiseNum = isset($chat_theme['praise_total'])?$chat_theme['praise_total']:0;
                if( $cachedata){
                    $live_list = $cachedata['room_list'];
                    $user_list = $cachedata['user_list'];
                    $flag = 0;
                    foreach ($live_list as $key=>$val){
                        if($val['room_id'] == $message_data['room_id']){
                            $cachePraiseNum = $live_list[$key]['praise_num'];
                            $flag = 1;
                        }
                    }
                }
                echo "缓存书成功!\n";
                $praiseNum = $cachePraiseNum+$dbPraiseNum;//实际点赞数量
                try {
                    $row_count = $Chat_db->update('chat_theme')->cols(array('login_num' => $login_tt))->where("cid= '".$room_id."'")->query();
                } catch (Exception $exc) {
                    echo "更新数据库失败. client_ip:{$_SERVER['REMOTE_ADDR']} $message:".$message."time:".date("Y-m-d h:i:sa")."\n";
                    return ;
//                    self::onClose($client_id);
                }
                 echo "更新数据库成功 \n";
                // 转播给当前房间的所有客户端，xx进入聊天室 message {type:login, client_id:xx, name:xx} 
                $new_message = array( 
                                        'type'=>$message_data['type'], 
					'client_id'=>$client_id,
                                        'room_id'=>$room_id,
                                        'praise_total'=>$praiseNum,
					'client_name'=>htmlspecialchars($client_name),
					'client_list'=>$client_list, 
					'time'=>time(),
					'client_total'=>$client_total,//真实+设置连接数
					'login_total'=>$login_total,//真实+设置登入数
					'client_tt'=>$client_total - $chat_theme['set_num'],//真实连接数
					'login_tt'=>$login_total - $chat_theme['set_num'],//数据库数据登入
				);
                 echo "登录成功 \n";
                $client_id_array = array_keys($all_clients);
                Gateway::sendToAll(json_encode($new_message), $client_id_array);
                break;
            // 客户端发言 message: {type:say, to_client_id:xx, content:xx}
            case 'say':
                echo "say开始\n";
                // 非法请求
                if(!isset($_SESSION['room_id']))
                {
//                    self::onClose($client_id);
                    echo "\$message_data['room_id'] not set. client_ip:{$_SERVER['REMOTE_ADDR']} \$message:$message time:".date("Y-m-d h:i:sa");
//                    throw new \Exception("\$_SESSION['room_id'] not set. client_ip:{$_SERVER['REMOTE_ADDR']}");
                }
                $roomId = intval($_SESSION['room_id']);
                echo "数据库chat_theme\n";
                //在线人数的基数，默认为0
                $chat_theme= $Chat_db->select('set_num,login_num,organization_id')->from('chat_theme')->where("cid= ".$roomId)->row();
                if(!$chat_theme){
                    echo "参数错误. client_ip:{$_SERVER['REMOTE_ADDR']} $message:".$message." time:".date("Y-m-d h:i:sa")."\n";
                    return ;
//                    self::onClose($client_id);
                }
                echo "数据库查询chat_theme OK\n";
                if(!self::$stopwordses || time() - self::$keyWords_Time > 300) // 5分钟取一次  更新 $stopwordses
                {
                        self::$keyWords_Time = time();
                        self::$stopwordses = $Chat_db->select('words,`replace` as rep ')->from('stopwords')->where("status=1")->query();	
                }	
                $room_id = $_SESSION['room_id'];
                $client_name = $message_data['user_name']?$message_data['user_name']:"悦媒网友".intval(substr($client_id,-7));
                echo "发言人:".$client_name."\n";
                $FROM_NAME = $client_name;
                $user_type = $message_data['user_type'];
                $msg_quote = null;
                // 是否有引用
                if(isset($message_data['msg_quote']) && $message_data['msg_quote']){
                    if(!is_null(json_decode($message_data['msg_quote']))){
                        $msg_quote =  $message_data['msg_quote'] ;
                    } else{
                        $msg_quote = $message_data['msg_quote'];
                        $msg_quote = str_replace("/n","",$msg_quote);
                        $msg_quote = str_replace("/r","",$msg_quote);
                        $msg_quote = str_replace("/t","",$msg_quote);
                        $msg_quote = str_replace("/r/n","",$msg_quote);
                    }
                    
                    if(is_array($msg_quote)){
                        $msg_quote = json_encode($msg_quote);
                    }
                    echo $msg_quote;
                }
                // img
                if(isset($message_data['img']) && $message_data['img']){
                    if(is_array($message_data['img'])){
                        $msg_img = json_encode($message_data['img']);
                    }else{
                        $msg_img = $message_data['img'].str_replace("\"","");
                    }
                }
                
                //设备id
                if(!isset($message_data['device_id'])){
                        $message_data['device_id'] = '-1';
                }
                $message_data['content'] =  self::replace($message_data['content']);
                // 私聊
                if($message_data['to_client_id'] != 'all')
                {
                    echo "私聊\n";
                    $insert_id = $Chat_db->insert('chat_log')
                                    ->cols(
                                            array(
                                            'cid'=>$room_id, 
                                            'type'=>'say',
                                            'user_id'=>$_SESSION['user_id'], 
                                            'to_type'=>'1',//all
                                            'user_type'=> $message_data['user_type'],
                                            'show_block'=> isset($message_data['show_block'])?$message_data['show_block']:'2',
                                            'from_icon'=>$message_data['user_icon'],
                                            'from_name'=>$client_name,
                                            'from_uid'=>$message_data['client_id'],
                                            'to_name'=>$message_data['to_client_name'], 
                                            'to_uid'=>$message_data['to_client_id'],
                                            'play_time'=>isset($message_data['play_time'])?$message_data['play_time']:'',
                                            'content'=>$message_data['content'],
                                            'quote'=>$msg_quote,
                                            'img'=>$msg_img,
                                            'content_type'=> $message_data['msg_type'],
                                            'city'=>$message_data['bdCityName'],
                                            'device_id'=>$message_data['device_id'],
                                            'ip'=>$_SERVER['REMOTE_ADDR'],
                                            'updated_at'=>time(),
                                            'created_at'=>time(),	
                                            )
                                    )
                                    ->query();
                    $message_data['content'] = str_replace("msg_content_@#$!00!$#@", 'msg_content_'.$insert_id,$message_data['content']);
                    $new_message = array(
                        'msg_id'=>$insert_id,
                        'type'=>'say',
                        'userId'=>$message_data['userId'],
                        'user_name' =>$FROM_NAME,
                        'user_type'=>$message_data['user_type'],
                        'user_icon'=>$message_data['user_icon'],
                        'show_block' => isset($message_data['show_block'])?$message_data['show_block']:'2',
                        'to_client_id'=>$message_data['to_client_id'],
                        'to_client_name'=>$message_data['to_client_name'],
                        'msg_type' => $message_data['msg_type'],
                        'play_time'=>$message_data['play_time'],
                        'content'=>htmlspecialchars($message_data['content']),
                        'img'=>isset($message_data['img'])?$message_data['img']:'' ,
                        'video'=>isset($message_data['video'])?$message_data['video']:'' ,
                        'msg_quote'=>$message_data['msg_quote'],
                        'bdCityName'=>$message_data['bdCityName'],
                        'bdCityCode'=>$message_data['bdCityCode'],
                        'device_id'=>$message_data['device_id'],
//                        'qiniu_host'=>$message_data['qiniu_host'],
                        'time'=>time(),
                    );
                    $row_count = $Chat_db->update('chat_log')->cols(array('content'=>$message_data['content']))->where("lid= '".$insert_id."'")->query();
                    Gateway::sendToClient($message_data['to_client_id'], json_encode($new_message));
                    $new_message['content'] = nl2br(htmlspecialchars($message_data['content']));
                    return Gateway::sendToCurrentClient(json_encode($new_message));
                }
                echo "公聊\n";
                // 向大家说
                $all_clients = self::getClientListFromRoom($_SESSION['room_id']);
                $client_id_array = array_keys($all_clients);
                if($message_data['to_client_id'] != 'all'){
                    $to_type = 1;
                }else{
                    $to_type = 2;
                }
                echo "插入数据库chat_log前\n";
                $insert_id = $Chat_db->insert('chat_log')
                            ->cols(array(
                                    'cid'=>$room_id, 
                                    'type'=>'say',
                                    'user_id'=>$_SESSION['user_id'], 
                                    'to_type'=>$to_type,//all
                                    'user_type'=> isset($message_data['user_type'])?$message_data['user_type']:'',
                                    'show_block'=> isset($message_data['show_block'])?$message_data['show_block']:'2',
                                    'from_icon'=>$message_data['user_icon'],
                                    'from_name'=>$client_name,
                                    'from_uid'=>isset($message_data['client_id'])?$message_data['client_id']:'',
                                    'to_name'=>isset($message_data['to_client_name'])?$message_data['to_client_name']:'', 
                                    'to_uid'=>isset($message_data['to_client_id'])?$message_data['to_client_id']:'',
                                    'play_time'=>isset($message_data['play_time'])?$message_data['play_time']:'',
                                    'content'=>$message_data['content'],
                                    'quote'=>$msg_quote,
                                    'img'=>isset($msg_img)?$msg_img:'',
                                    'content_type'=> isset($message_data['msg_type'])?$message_data['msg_type']:'',
                                    'city'=>isset($message_data['bdCityName'])?$message_data['bdCityName']:'',
                                    'device_id'=>$message_data['device_id'],
                                    'ip'=>$_SERVER['REMOTE_ADDR'],
                                    'updated_at'=>time(),
                                    'created_at'=>time(),	
                                    ))->query();
                echo "插入数据库chat_log成功\n";
                $message_data['content'] = str_replace("msg_content_@#$!00!$#@", "msg_content_".$insert_id,$message_data['content']);
                echo "更新数据库chat_log前\n";
                $row_count = $Chat_db->update('chat_log')->cols(array('content'=>$message_data['content']))->where("lid= '".$insert_id."'")->query();
                echo "更新数据库chat_logOOK\n";
                $new_message = array(
                    'msg_id'=>$insert_id,
                    'type'=>'say', 
                    'userId'=>$message_data['userId'],
                    'client_id'=>$client_id,
                    'user_name' =>$client_name,
                    'user_type'=>$message_data['user_type'],
                    'user_icon'=>$message_data['user_icon'],
                    'show_block' => isset($message_data['show_block'])?$message_data['show_block']:'2',
                    'to_client_id'=>isset($message_data['to_client_id'])?$message_data['to_client_id']:'',
                    'to_client_name'=>isset($message_data['to_client_name'])?$message_data['to_client_name']:'',
                    'msg_type' => isset($message_data['msg_type'])?$message_data['msg_type']:'',
                    'play_time'=>isset($message_data['play_time'])?$message_data['play_time']:'',
                    'content'=>htmlspecialchars($message_data['content']),
                    'img'=>isset($message_data['img'])?$message_data['img']:'' ,
                    'video'=>isset($message_data['video'])?$message_data['video']:'' ,
                    'msg_quote'=>isset($message_data['msg_quote'])?$message_data['msg_quote']:'',
                    'bdCityName'=>isset($message_data['bdCityName'])?$message_data['bdCityName']:'',
                    'bdCityCode'=>isset($message_data['bdCityCode'])?$message_data['bdCityCode']:'',
                    'device_id'=>isset($message_data['device_id'])?$message_data['device_id']:'',
//                    'qiniu_host'=>$message_data['qiniu_host'],
                    'time'=>time(),
                );
                echo "say成功OK\n";
                Gateway::sendToAll(json_encode($new_message), $client_id_array);
                break;
            case 'set':	
                echo "set开始\n";
                    switch ($message_data['msg_type']){
                            case 'create_theme': 
                                    if($message_data['theme']){
                                            $insert_id = $Chat_db->insert('chat_theme')->cols(array('theme'=>$message_data['theme'], 'uid'=>155, 'created_at'=>time(), 'updated_at'=>time()))->query();
                                    }
                                    break;
                            case 'change_name': 
                                    if($message_data['client_name']){
                                            // 判断是否有房间号
                                            if(!isset($message_data['room_id']))
                                            {
                                                    throw new \Exception("\$message_data['room_id'] not set. client_ip:{$_SERVER['REMOTE_ADDR']} \$message:$message");
                                            }
                                            // 把房间号昵称放到session中
                                            $room_id = $message_data['room_id'];
                                            $client_name = htmlspecialchars($message_data['client_name']);
                                            $_SESSION['room_id'] = $room_id;
                                            $_SESSION['client_name'] = $client_name;
                                            // 存储到当前房间的客户端列表
                                            $all_clients = self::addClientToRoom($room_id, $client_id, $client_name);
                                            // 整理客户端列表以便显示
                                            $client_list = self::formatClientsData($all_clients);
                                            // 转播给当前房间的所有客户端，xx进入聊天室 message {type:login, client_id:xx, name:xx} 
                                            $new_message = array('type'=>$message_data['type'], 'msg_type'=>$message_data['msg_type'],'client_id'=>$client_id, 'client_name'=>htmlspecialchars($client_name), 'client_list'=>$client_list, 'time'=>date('m-d H:i:s'));
                                            $client_id_array = array_keys($all_clients);
                                            Gateway::sendToAll(json_encode($new_message), $client_id_array);
                                            return;
                                    }
                                    break;
                            case 'edit_msg':
                                echo "edit_msg开始\n";
                                    // 敏感词替换
                                    //在线人数的基数，默认为0
                                    $chat_theme= $Chat_db->select('set_num,login_num,organization_id')->from('chat_theme')->where("cid= '".$_SESSION['room_id']."'")->row();
                                    //
                                    if(!self::$stopwordses || time() - self::$keyWords_Time > 300) // 半小时取一次  更新 $stopwordses
                                    {
                                            self::$keyWords_Time = time();
                                            self::$stopwordses = $Chat_db->select('words,`replace` as rep ')->from('stopwords')->where("status=1")->query();	
                                    }	
                                    $message_data['content'] = self::replace($message_data['content']);
                                    // 把房间号昵称放到session中
                                    $room_id = $_SESSION['room_id'];
                                    $client_name = $_SESSION['client_name'];
                                    //查询出来
                                    $chat_log = $Chat_db->select('content_type,content')->from('chat_log')->where("lid= '".$message_data['msg_id']."' ")->row();
                                    echo "chat_log查询成功\n";
                                    $msg_content_after = $message_data['content'];
                                    $message_data['content'] = str_replace($message_data['msg_content_before'], $message_data['content'],$chat_log['content']);//
                                    // 更新msg
                                    $row_count = $Chat_db->update('chat_log')->cols(array('content'=>$message_data['content']))->where("lid= '".$message_data['msg_id']."'")->query();
                                    echo "chat_log更新成功\n";
                                    if($row_count > 0){
                                        echo "广播\n";
                                            // 广播成功  
                                            $all_clients = self::getClientListFromRoom($_SESSION['room_id']);
                                            $client_id_array = array_keys($all_clients);
                                            $new_message = array(
                                                    'type'=>'set', 
                                                    'from_client_id'=>$client_id,
                                                    'from_client_name' =>$client_name,
                                                    'user_type'=>$message_data['user_type'],
                                                    'user_icon'=>$message_data['user_icon'],
                                                    'show_block' => isset($message_data['show_block'])?$message_data['show_block']:'2',
                                                    'to_client_id'=>'all',
                                                    'msg_type' => $message_data['msg_type'],
                                                    'msg_id'=>$message_data['msg_id'],
                                                    'content'=>htmlspecialchars($msg_content_after),
                                                    'time'=>date('m-d H:i:s'),
                                            );
                                            echo "广播成功\n";
                                            return Gateway::sendToAll(json_encode($new_message), $client_id_array);
                                    }
                                    break;
                            case 'delete_msg':
                                echo "delete_msg开始\n";
                                    // 把房间号昵称放到session中
                                    $room_id = $_SESSION['room_id'];
                                    $client_name = $_SESSION['client_name'];
                                    //删除msg
                                    $row_count = $Chat_db->update('chat_log')->cols(array('is_delete'=>'1'))->where("lid= '".$message_data['msg_id']."'")->query();
                                    echo "chat_log更新成功\n";
                                    if($row_count > 0){
                                        echo "广播\n";
                                            //广播成功
                                            $all_clients = self::getClientListFromRoom($_SESSION['room_id']);
                                            $client_id_array = array_keys($all_clients);
                                            $new_message = array(
                                                    'type'=>'set', 
                                                    'from_client_id'=>$client_id,
                                                    'from_client_name' =>$client_name,
                                                    'user_type'=>$message_data['user_type'],
                                                    'user_icon'=>$message_data['user_icon'],
                                                    'show_block' => isset($message_data['show_block'])?$message_data['show_block']:'2',
                                                    'to_client_id'=>'all',
                                                    'msg_type' => $message_data['msg_type'],
                                                    'msg_id'=>$message_data['msg_id'],
        //								'content'=>nl2br(htmlspecialchars($message_data['content'])),
                                                    'time'=>time(),
                                            );
                                            echo "广播成功\n";
                                            return Gateway::sendToAll(json_encode($new_message), $client_id_array);
                                    }
                                    break;
                            case 'updateClientNum':
                                    //把房间号昵称放到session中
                                    $room_id = $_SESSION['room_id'];
                                    $client_name = $_SESSION['client_name'];
                                    // 改数据库基础人数
                                    $client_num = $message_data['setClientNum'];
                                    if(!is_numeric($client_num)){
                                            break;
                                    }
                                    //更改数据库
                                    $row_count = $Chat_db->update('chat_theme')->cols(array('set_num'=>$client_num))->where("cid= '".$message_data['room_id']."'")->query();//
                                    break;
                    }
                    break;
            // 视频直播点赞
            case 'praise':
                echo "praise开始\n";
                $cachetime = $mem->get(self::$pTimeKey);
                if(!$cachetime){
                    $tr = $mem->add(self::$pTimeKey,time());
                    $cachetime = $mem->get(self::$pTimeKey);
                }
                echo '缓存时间$cachetime:'.$cachetime."当前时间:".time();
                $cachedata = $mem->get(self::$pDataKey);//存放room_id和点赞数
                print_r($cachedata);
                echo "room_id:".$_SESSION['room_id']."\n";
                if( $cachedata && $_SESSION['room_id']){
                    $live_list = $cachedata['room_list'];
                    $user_list = $cachedata['user_list'];
                    $flag = 0;
                    foreach ($live_list as $key=>$val){
                        if($val['room_id'] == $_SESSION['room_id']){
                            $live_list[$key]['praise_num'] = $val['praise_num'] + 1;
                            $live_list[$key]['update_time'] = time();
                            $flag = 1;
                        }
                    }
                    if($flag == 0){
                        $val = array(
                            "room_id"=>$_SESSION['room_id'],
                            "praise_num"=>1,
                            "update_time"=>time(),
                        );
                        $live_list[] = $val;
                    }
                    $cachedata['room_list'] = $live_list;
                    //用户list
                    $tmp_ = array(
                        "user_id"=>$_SESSION['user_id'],
                        "update_time"=>time(),
                    );
                    $user_list[] = $tmp_;
                    $cachedata['user_list'] = $user_list;
                    $mem->Replace(self::$pDataKey,$cachedata);
                }else {
                    echo "key不存在";
                    $alive['room_id'] = $_SESSION['room_id'];
                    $alive['praise_num'] = 1;
                    $alive['update_time'] = time();
                    $auser['user_id'] = $_SESSION['user_id'];
                    $auser['update_time'] = time();
                    $tmp_live[] = $alive;
                    $tmp_user[] = $auser;
                    $cachedata = array(
                        'room_list' => $tmp_live,
                        'user_list' => $tmp_user,
                    );
                    $tr = $mem->add(self::$pDataKey, $cachedata);
                    echo "\n添加缓存成功?:".$tr."\n";
                }
                echo "缓存数据:";
                $tr = $mem->get(self::$pDataKey);
                print_r($tr);
                $praise_total = $message_data['praise_total'] + 1;
                if(time() - $cachetime >= 60){
                    echo '超时\n';
                    $mem->delete(self::$pDataKey);
                    $mem->delete(self::$pTimeKey);
                    self::insertIntoLive($cachedata);
                }
                $new_message = array(
                    'lid'=>$message_data['live_id'],
                    'type'=>'praise', 
                    'userId'=>$message_data['userId'],
                    'client_id'=>$client_id,
                    'organization_id'=>isset($message_data['organization_id'])?$message_data['organization_id']:'9',
                    'user_name' =>$message_data['user_name'],
                    'praise_total'=>$praise_total,
                    'show_block' => isset($message_data['show_block'])?$message_data['show_block']:'2',
                    'to_client_id'=>$message_data['to_client_id']?$message_data['to_client_id']:'all',
                    'to_client_name'=>$message_data['to_client_name']?$message_data['to_client_name']:'all',
                    'bdCityName'=>$message_data['bdCityName'],
                    'bdCityCode'=>isset($message_data['bdCityCode'])?$message_data['bdCityCode']:'',
                    'device_id'=>$message_data['device_id'],
                    'time'=>time(),
                );
                echo "praise点赞成功\n";
                print_r($new_message);
                $all_clients = self::getClientListFromRoom($_SESSION['room_id']);
                print_r($all_clients);
                $client_id_array = array_keys($all_clients);
                Gateway::sendToAll(json_encode($new_message), $client_id_array);
                break;
            // 主播开始\结束
            case 'start':
            case 'finish':
                echo "开始或结束\n";
                $new_message = array(
                    'type'=>$message_data['type'],
                    'from_client_id'=>$message_data['from_client_id'],
                    'time'=>time(),
                );
                $all_clients = self::getClientListFromRoom($_SESSION['room_id']);
                $client_id_array = array_keys($all_clients);
                print_r($all_clients);
                Gateway::sendToAll(json_encode($new_message), $client_id_array);
                break;
        }
        echo "onMessage结束\n";
   }
   
    /**
     * 当用户断开连接时
     * @param integer $client_id 用户id
     */
    public static function onClose($client_id)
    {
//        $workerArr = Worker::getAllWorkers();
//        echo "要关闭的Id:".$client_id."\n";
//        echo "所有worker实例";
//        print_r($workerArr);
        $Chat_db = Db::instance('Chat_db');
        // debug
        echo "断开连接:client:{$_SERVER['REMOTE_ADDR']}:{$_SERVER['REMOTE_PORT']} gateway:{$_SERVER['GATEWAY_ADDR']}:{$_SERVER['GATEWAY_PORT']}  client_id:$client_id onClose:'' time:".date("Y-m-d h:i:sa")."\n";
        // 从房间的客户端列表中删除
        if(isset($_SESSION['room_id']))
        {
            $room_id = $_SESSION['room_id'];
            self::delClientFromRoom($room_id, $client_id);
            // 广播 xxx 退出了
            if($all_clients = self::getClientListFromRoom($room_id))
            {
                $client_list = self::formatClientsData($all_clients);
                //在线人数的基数，默认为0
                $chat_theme= $Chat_db->select('set_num,login_num')->from('chat_theme')->where("cid= '".$room_id."'")->row();
                // login_num
                $login_tt = 0;// 真实数据
                $login_total = 0; // +set数据

                $client_tt = count($client_list); // 真实数据
                $client_total =$client_tt;// +set数据

                if(isset($chat_theme['set_num'])){
                        $client_total  += $chat_theme['set_num'];	
                } 
                if(isset($chat_theme['login_num'])){
                        $login_tt = $chat_theme['set_num'];
                        $login_total  = $chat_theme['login_num'] +$chat_theme['set_num'];	
                }
                $new_message = array('type'=>'logout', 
                                    'room_id'=>$room_id, 
                                    'from_client_id'=>$client_id, 
                                    'from_client_name'=>$_SESSION['client_name'], 
                                    'client_list'=>$client_list, 
                                    'time'=>time(),
                                    'client_total'=>$client_total,
                                    'login_total'=>$login_total,
                                    'client_tt'=>$client_total - $chat_theme['set_num'],
                                    'login_tt'=>$login_total - $chat_theme['set_num'],
                );
                $client_id_array = array_keys($all_clients);
                Gateway::sendToAll(json_encode($new_message), $client_id_array);
            }
        }
        Gateway::closeClient($client_id);
    }
    /**
    * 格式化客户端列表数据
    * @param array $all_clients
    */
    public static function formatClientsData($all_clients)
    {
        $client_list = array();
        if($all_clients)
        {
            foreach($all_clients as $tmp_client_id=>$tmp_name)
            {
                $client_list[] = array('client_id'=>$tmp_client_id, 'client_name'=>$tmp_name);
            }
        }
        return $client_list;
    }
    /**
    * 获得客户端列表
    * @todo 保存有限个
    */
    public static function getClientListFromRoom($room_id)
    {
        $key = "ROOM_CLIENT_LIST-$room_id";
        $store = Store::instance('room');
        $ret = $store->get($key);
        if(false === $ret)
        {
            if(get_class($store) == 'Memcached')
            {
                if($store->getResultCode() == \Memcached::RES_NOTFOUND)
                {
                    return array();
                }
                else 
                {
                    throw new \Exception("getClientListFromRoom($room_id)->Store::instance('room')->get($key) fail " . $store->getResultMessage());
                }
            }
            return array();
        }
        return $ret;
    }
   
    /**
     * 从客户端列表中删除一个客户端
     * @param int $client_id
     */
    public static function delClientFromRoom($room_id, $client_id)
    {
        $key = "ROOM_CLIENT_LIST-$room_id";
        $store = Store::instance('room');
        // 存储驱动是memcached
        if(get_class($store) == 'Memcached')
        {
            $cas = 0;
            $try_count = 3;
            while($try_count--)
            {
                $client_list = $store->get($key, null, $cas);
                if(false === $client_list)
                {
                    if($store->getResultCode() == \Memcached::RES_NOTFOUND)
                    {
                        return array();
                    }
                    else
                    {
                         throw new \Exception("Memcached->get($key) return false and memcache errcode:" .$store->getResultCode(). " errmsg:" . $store->getResultMessage());
                    }
                }
                if(isset($client_list[$client_id]))
                {
                    unset($client_list[$client_id]);
                    if($store->cas($cas, $key, $client_list))
                    {
                        return $client_list;
                    }
                }
                else 
                {
                    return true;
                }
            }
            throw new \Exception("delClientFromRoom($room_id, $client_id)->Store::instance('room')->cas($cas, $key, \$client_list) fail" . $store->getResultMessage());
        }
        // 存储驱动是memcache或者file
        else
        {
            $handler = fopen(__FILE__, 'r');
            flock($handler,  LOCK_EX);
            $client_list = $store->get($key);
            if(isset($client_list[$client_id]))
            {
                unset($client_list[$client_id]);
                $ret = $store->set($key, $client_list);
                flock($handler, LOCK_UN);
                return $client_list;
            }
            flock($handler, LOCK_UN);
        }
        return $client_list;
    }
    /**
     * 添加到客户端列表中
     * @param int $client_id
     * @param string $client_name
     */
    public static function addClientToRoom($room_id, $client_id, $client_name)
    {
        $key = "ROOM_CLIENT_LIST-$room_id";
        $store = Store::instance('room');
//        $store->delete($key);
        // 获取所有所有房间的实际在线客户端列表，以便将存储中不在线用户删除
        $all_online_client_id = Gateway::getOnlineStatus();
        // 存储驱动是memcached
        if(get_class($store) == 'Memcached')
        {
            $cas = 0;
            $try_count = 3;
            while($try_count--)
            {
                $client_list = $store->get($key, null, $cas);
                if(false === $client_list)
                {
                    if($store->getResultCode() == \Memcached::RES_NOTFOUND)
                    {
                        $client_list = array();
                    }
                    else
                    {
                        throw new \Exception("Memcached->get($key) return false and memcache errcode:" .$store->getResultCode(). " errmsg:" . $store->getResultMessage());
                    }
                }
                if(!isset($client_list[$client_id]))
                {
                    // 将存储中不在线用户删除
                    if($all_online_client_id && $client_list)
                    {
                        $all_online_client_id = array_flip($all_online_client_id);
                        $client_list = array_intersect_key($client_list, $all_online_client_id);
                    }
                    // 添加在线客户端
                    $client_list[$client_id] = $client_name;
                    // 原子添加
                    if($store->getResultCode() == \Memcached::RES_NOTFOUND)
                    {
                        $store->add($key, $client_list);
                    }
                    // 置换
                    else
                    {
                        $store->cas($cas, $key, $client_list);
                    }
                    if($store->getResultCode() == \Memcached::RES_SUCCESS)
                    {
                        return $client_list;
                    }
                }
                else 
                {
                    if($client_list[$client_id] != $client_name){
                            // 添加在线客户端
                            $client_list[$client_id] = $client_name;
                            // 原子添加
                            if($store->getResultCode() == \Memcached::RES_NOTFOUND)
                            {
                                    $store->add($key, $client_list);
                            }
                            // 置换
                            else
                            {
                                    $store->cas($cas, $key, $client_list);
                            }
                            if($store->getResultCode() == \Memcached::RES_SUCCESS)
                            {
                                    return $client_list;
                            }
                    }
                    return $client_list;
                }
            }
            throw new \Exception("addClientToRoom($room_id, $client_id, $client_name)->cas($cas, $key, \$client_list) fail .".$store->getResultMessage());
        }
        // 存储驱动是memcache或者file
        else
        {
            $handler = fopen(__FILE__, 'r');
            flock($handler,  LOCK_EX);
            $client_list = $store->get($key);
            if(!isset($client_list[$client_id]))
            {
                // 将存储中不在线用户删除
                if($all_online_client_id && $client_list)
                {
                    $all_online_client_id = array_flip($all_online_client_id);
                    $client_list = array_intersect_key($client_list, $all_online_client_id);
                }
                // 添加在线客户端
                $client_list[$client_id] = $client_name;
                $ret = $store->set($key, $client_list);
                flock($handler, LOCK_UN);
                return $client_list;
                    }else{
                                 if($client_list[$client_id] !=  $client_name){
                                         $client_list[$client_id] = $client_name;
                                         $ret = $store->set($key, $client_list);
                                         return $client_list;
                                 }
                    }

                   flock($handler, LOCK_UN);
        }
        return $client_list;
    }

     /**
     * 更新数据库
     * @param $data live_id=>praisenum的数组
     */
    public static function insertIntoLive($data){
        echo "insertIntoLive插入数据库\n";
        if(!$data){
            return false;
        }
        global $Chat_db;
        $room_lists = $data['room_list'];
        $user_lists = $data['user_list'];
        foreach ($room_lists as $key=>$val){
            $chat_= $Chat_db->select('cid,praise_total')->from('chat_theme')->where("cid= '".$val['room_id']."'")->row();
            if(!$chat_){
                echo "参数错误. room_id:".$val['room_id']." time:".date("Y-m-d h:i:sa")."\n";
            }
            try {
                $row_count = $Chat_db->update('chat_theme')->cols(array('praise_total'=>$chat_['praise_total']+$val['praise_num'],'updated_at'=>$val['update_time']))->where("cid= '".$val['room_id']."'")->query();//

            } catch (Exception $exc) {
                echo "更新数据库失败. room_id:".$val['room_id']."time:".date("Y-m-d h:i:sa")."\n";
            }
        }
        echo "chat_theme插入数据库成功\n";
        foreach ($user_lists as $key=>$val){
            try {
                $insert_id = $Chat_db->insert('praise')
                                ->cols(
                                        array(
                                        'user_id'=>$val['user_id'], 
                                        'organization_id'=>'9',
                                        'type'=>14,	
                                        'updated_at'=>$val['update_time'],
                                        'created_at'=>$val['update_time'],
                                        )
                                )->query();
                echo "praise插入数据库成功\n";
            } catch (Exception $exc) {
                echo "插入点赞数据库失败. userId:".$val['userId']."time:".date("Y-m-d h:i:sa")."\n";
    //                    self::onClose($client_id);
            } 
        }
        echo "insertIntoLive()插入数据库成功\n";
    }
}
