<?php

/**
 * run with command 
 * php start.php start
 */
ini_set('display_errors', 'on');

use Workerman\Worker;

require_once __DIR__ . '/vendor/phpdotenv/src/Dotenv.php';
require_once __DIR__ . '/config/Store.php';
require_once __DIR__ . '/config/Db.php';
if (strpos(strtolower(PHP_OS), 'win') === 0) {
    exit("start.php not support windows, please use start_for_win.bat\n");
}

// 检查扩展
if (!extension_loaded('pcntl')) {
    exit("Please install pcntl extension. See http://doc3.workerman.net/appendices/install-extension.html\n");
}

if (!extension_loaded('posix')) {
    exit("Please install posix extension. See http://doc3.workerman.net/appendices/install-extension.html\n");
}
//加载项目env /public/../.env  配置文件
new Dotenv\Dotenv(__DIR__ . '/../../../');

\Config\Store::$name = getenv('WORKMAN_GATEWAY_NAME');
\Config\Store::$driver = getenv('WORKMAN_DRIVER');
\Config\Store::$count = getenv('WORKMAN_GATEWAY_COUNT');
\Config\Store::$gateway_ip = getenv('WORKMAN_GATEWAY_IP_PORT');
\Config\Store::$registerAddress = getenv('WORKMAN_GATEWAY_REGISTER_ADDRESS');
\Config\Store::$lan_ip = getenv('WORKMAN_GATEWAY_LAN_IP');
//\Config\Store::$room[] =  getenv('CACHE_MEMCACHED_HOST').":".getenv('CACHE_MEMCACHED_PORT');
\Config\Store::$startPort = getenv('WORKMAN_STARTPORT');
\Config\Store::$pingInterval = getenv('WORKMAN_PING_INTERVAL');
\Config\Db::$Chat_db = array(
    'host' => getenv('DB_HOST'),
    'port' => 3306,
    'user' => getenv('DB_USERNAME'),
    'password' => getenv('DB_PASSWORD'),
    'dbname' => getenv('DB_DATABASE'),
    'charset' => 'utf8mb4',
);
// 标o记是全局启动
define('GLOBAL_START', 1);
require_once __DIR__ . '/vendor/workerman/workerman/Autoloader.php';
require_once __DIR__ . '/vendor/autoload.php';

// 加载所有Applications/*/start.php，以便启动所有服务
foreach (glob(__DIR__ . '/Applications/start*.php') as $start_file) {
    require_once $start_file;
}
// 运行所有服务
Worker::runAll();
