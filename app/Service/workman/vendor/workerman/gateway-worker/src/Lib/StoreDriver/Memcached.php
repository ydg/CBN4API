<?php
/**
 * This file is part of workerman.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author walkor<walkor@workerman.net>
 * @copyright walkor<walkor@workerman.net>
 * @link http://www.workerman.net/
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace GatewayWorker\Lib\StoreDriver;

/**
 * Redis
 */

class Memcached extends \Memcached
{
    /**
     * (PECL memcached &gt;= 0.1.0)<br/>
     * Create a Memcached instance
     * @link http://php.net/manual/en/memcached.construct.php
     * @param $persistent_id [optional]
     * @param $callback [optional]
     */
    public function __construct($host, $port, $weight = 0)
    {
//        return parent::addServer('127.0.0.1', '11211');
    }
    /**
    * (PECL memcached &gt;= 0.1.0)<br/>
    * Increment numeric item's value
    * @link http://php.net/manual/en/memcached.increment.php
    * @param string $key <p>
    * The key of the item to increment.
    * </p>
    * @param int $offset [optional] <p>
    * The amount by which to increment the item's value.
    * </p>
    * @param int $initial_value [optional] <p>
    * The value to set the item to if it doesn't currently exist.
    * </p>
    * @param int $expiry [optional] <p>
    * The expiry time to set on the item.
    * </p>
    * @return int new item's value on success or <b>FALSE</b> on failure.
    */
    public function mincrement($key)
    {
        return parent::increment($key);
    }
    /**
    * (PECL memcached &gt;= 0.1.0)<br/>
    * Retrieve an item
    * @link http://php.net/manual/en/memcached.get.php
    * @param string $key <p>
    * The key of the item to retrieve.
    * </p>
    * @param callable $cache_cb [optional] <p>
    * Read-through caching callback or <b>NULL</b>.
    * </p>
    * @param float $cas_token [optional] <p>
    * The variable to store the CAS token in.
    * </p>
    * @return mixed the value stored in the cache or <b>FALSE</b> otherwise.
    * The <b>Memcached::getResultCode</b> will return
    * <b>Memcached::RES_NOTFOUND</b> if the key does not exist.
    */
    public function mget($key)
    {
        return parent::get($key);
    }
    public function madd($key)
    {
        return parent::add($key);
    }
    /**
    * (PECL memcached &gt;= 0.1.0)<br/>
    * Delete an item
    * @link http://php.net/manual/en/memcached.delete.php
    * @param string $key <p>
    * The key to be deleted.
    * </p>
    * @param int $time [optional] <p>
    * The amount of time the server will wait to delete the item.
    * </p>
    * @return bool <b>TRUE</b> on success or <b>FALSE</b> on failure.
    * The <b>Memcached::getResultCode</b> will return
    * <b>Memcached::RES_NOTFOUND</b> if the key does not exist.
    */
    public function delete ($key, $time = 0) {}
    
}
