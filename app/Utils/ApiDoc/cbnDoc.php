<?php namespace App\Utils\ApiDoc;

class cbnDoc {

    //项目介绍
    public $information = '中国广播电视网络有限公司（China Broadcasting Network Corporation Ltd.),简称CBN。因此项目前缀：cbn';
    
    //接口修改记录
    public $modificationRecord = array(
        
        '2017-12-4' => array(
            '开始撰写',
        ),
        '2017-12-6' => array(
            '撰写完成',
        ),
        '2017-12-20' => array(
            '开始撰写 撰写手机端，M意为mobile',
        ),
    );
    
    //留言信息
    public $message = array(
        '##message##',
    );

}