<?php

namespace App\Utils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CacheLists {
    //objectType实体类型#00#：1:文章2:评论3:生活4:自媒体文章5问6答7帖子8报名9投票10问卷11抽奖12活动13用户14视频直播15文字直播16直播聊天记录 ，所有实体model里面写入这个字子段
    //type操作类型#22#：1、评论 2、点赞 3、收藏 4、分享 5、关注数 6、被关注数 7、参与人数 8、浏览数
    public $time = array( 
        'IPBlack'=>60,
        'IPBlackHistory'=>2,
        'debug'=>30,
        'categoryList'=>3,
        'programList'=>0.1,
        'searchProgramList'=>0.1,
        'programDetail'=>0.1,
        'getRecommend'=>0.1,
        'getHistory'=>0.01,
    );
    
    //所有cache的规则
    public $lists = array(
        /*******************************请求类缓存****************************************/
        'categoryList'=>array(
            'type' => 6, //1常量-不经常变的东西 2变量 3实体 4列表 5model 6请求
            'SELF_PRE '=>'',
            'tag' => array(
                0=>'category',
                ),
            'key' => 'categoryList:epgId:#00#',
            'is_random_time' => TRUE,
            'time' => 2, //defualt 时间
            'before'=> 20,
            'action' => array( 
            ),
        ),
        'programList'=>array(
            'type' => 6, //1常量-不经常变的东西 2变量 3实体 4列表 5model 6请求
            'SELF_PRE '=>'',
            'tag' => array(
                0=>'program',
                ),
            'key' => 'programList:epgId:#00#:categoryId:#11#:page:#22#:size:#33#',
            'is_random_time' => TRUE,
            'time' => 2, //defualt 时间
            'before'=> 20,
            'action' => array( 
            ),
        ),
        'searchProgramList'=>array(
            'type' => 6, //1常量-不经常变的东西 2变量 3实体 4列表 5model 6请求
            'SELF_PRE '=>'',
            'tag' => array(
                0=>'program',
                ),
            'key' => 'searchProgramList:epgId:#00#:classify:#11#:keywords:#22#:page:#33#:size:#44#',
            'is_random_time' => TRUE,
            'time' => 2, //defualt 时间
            'before'=> 20,
            'action' => array( 
            ),
        ),
        'programDetail'=>array(
            'type' => 6, //1常量-不经常变的东西 2变量 3实体 4列表 5model 6请求
            'SELF_PRE '=>'',
            'tag' => array(
                0=>'program',
                ),
            'key' => 'programDetail:epgId:#00#:programId:#11#',
            'is_random_time' => TRUE,
            'time' => 2, //defualt 时间
            'before'=> 20,
            'action' => array( 
            ),
        ),
        
        'getRecommend'=>array(
            'type' => 6, //1常量-不经常变的东西 2变量 3实体 4列表 5model 6请求
            'SELF_PRE '=>'',
            'tag' => array(
                0=>'program',
                ),
            'key' => 'getRecommend:epgId:#00#:recommendId:#11#',
            'is_random_time' => TRUE,
            'time' => 2, //defualt 时间
            'before'=> 20,
            'action' => array( 
            ),
        ),
        
        'getHistory'=>array(
            'type' => 4, //1常量-不经常变的东西 2变量 3实体 4列表 5model
            'SELF_PRE '=>'',
            'tag' => array(
                0=>'program',
                ),
            'key' => 'getHistory:epgId:#00#:uid:#11#',
            'is_random_time' => TRUE,
            'time' => 0.01, //defualt 时间
            'before'=> 20,
            'action' => array( 
            ),
        ),
        
        'getFavorite'=>array(
            'type' => 4, //1常量-不经常变的东西 2变量 3实体 4列表 5model
            'SELF_PRE '=>'',
            'tag' => array(
                0=>'program',
                ),
            'key' => 'getFavorite:epgId:#00#:uid:#11#',
            'is_random_time' => TRUE,
            'time' => 0.01, //defualt 时间
            'before'=> 20,
            'action' => array( 
            ),
        ),
        
        'getFavoriteStatus'=>array(
            'type' => 4, //1常量-不经常变的东西 2变量 3实体 4列表 5model
            'SELF_PRE '=>'',
            'tag' => array(
                0=>'program',
                ),
            'key' => 'getFavoriteStatus:epgId:#00#:uid:#11#:programId:#222#',
            'is_random_time' => TRUE,
            'time' => 0.01, //defualt 时间
            'before'=> 20,
            'action' => array( 
            ),
        ),
        
        
        'getUserIdByUid'=>array(
            //objectType实体类型#00#：1:文章2:评论3:生活4:自媒体文章5问6答7帖子8报名9投票10问卷11抽奖12活动13用户  
            //id实体id #11#：
            //type操作类型#22#：1、评论 2、点赞 3、收藏 4、分享 5、关注数 6、被关注数 7、参与人数 
            'type' => 3, //1常量-不经常变的东西 2变量 3实体 4列表 5model
            'SELF_PRE '=>'',
            'tag' => array(
                ),
            'key' => 'getUserIdByUid:epgId#00#uid#11#',
            'is_random_time' => TRUE,
            'time' => 1, //defualt 时间
            'before'=>0,
            'action' => array( 
            ),
        ),
        
        //tv 少儿旅游
        'getConfig'=>array(
            //objgetRecommendectType实体类型#00#：1:文章2:评论3:生活4:自媒体文章5问6答7帖子8报名9投票10问卷11抽奖12活动13用户  
            //id实体id #11#：
            //type操作类型#22#：1、评论 2、点赞 3、收藏 4、分享 5、关注数 6、被关注数 7、参与人数 
            'type' => 6, //1常量-不经常变的东西 2变量 3实体 4列表 5model
            'SELF_PRE '=>'',
            'tag' => array(
                ),
            'key' => 'getUserIdByUid:epgId#00#',
            'is_random_time' => TRUE,
            'time' => 1, //defualt 时间
            'before'=>0,
            'action' => array( 
            ),
        ),
        
        //推荐位置详情
        'getRecommendDetail'=>array(
            'type' => 6, //1常量-不经常变的东西 2变量 3实体 4列表 5model
            'SELF_PRE '=>'',
            'tag' => array(
                ),
            'key' => 'getRecommendDetail:epgId#00#:type:#11#:objectId:#22#',
            'is_random_time' => TRUE,
            'time' => 1, //defualt 时间
            'before'=>0,
            'action' => array( 
            ),
        ),
        
        //分享页面缓存
        'sharePage'=>array(
            'type' => 6, //1常量-不经常变的东西 2变量 3实体 4列表 5model
            'SELF_PRE '=>'',
            'tag' => array(
                ),
            'key' => 'sharePage:#00#:ID:#11#',
            'is_random_time' => TRUE,
            'time' => 2, //defualt 时间
            'before'=>0,
            'action' => array( 
            ),
        ),
        
        /*******************************其他缓存****************************************/ 
        //WenXin配置
        'WenXinConfig'=>array(
            'type' => 6, //1常量-不经常变的东西 2变量 3实体 4列表 5model
            'SELF_PRE '=>'',
            'tag' => array(
                ),
            'key' => 'WenXinConfig',
            'is_random_time' => TRUE,
            'time' => 30, //defualt 时间
            'before'=>0,
            'action' => array( 
            ),
        ),
        'objectNum' => array(
            //objectType实体类型#00#：1:文章2:评论3:生活4:自媒体文章5问6答7帖子8报名9投票10问卷11抽奖12活动13用户  
            //id实体id #11#：
            //type操作类型#22#：1、评论 2、点赞 3、收藏 4、分享 5、关注数 6、被关注数 7、参与人数 
            'type' => 2, //1常量-不经常变的东西 2变量 3实体 4列表 5model
            'SELF_PRE '=>'',
            'tag' => array(
                0=>'objectobjectType',
//                1=>'objectobjectType#00#id#11#',
                ),
            'key' => 'objectobjectType#00#id#11#type#22#',
            'is_random_time' => TRUE,
            'time' => 1, //defualt 时间
            'before'=>0,
            'action' => array( 
                'pull' => array(
                    
                ),
                'put' => array(
                   '0' => array(
                        'where'=>array('#22#'=>1),
                        'method'=>'flush',
                        'cacheName' => 'commentList',
                        'cachePar' => array('#00#','#11#'),//
                    ),
                ),
                'forever' => array(
                   '0' => array(
                        'where'=>array('#22#'=>1),
                        'method'=>'flush',
                        'cacheName' => 'commentList',
                        'cachePar' => array('#00#','#11#'),//
                    ),
                ),
                'add' => array( //当对象确实被加入缓存时，使用 add 方法将会返回 true 否则会返回 false
                    
                ),
                'remember' => array(
                    
                ),
                'update' => array(
                    
                ),
                'increment'=>array(
                    '0' => array(
                        'where'=>array('#22#'=>1),
                        'method'=>'flush',
                        'cacheName' => 'commentList',
                        'cachePar' => array('#00#','#11#'),//
                    )
                ),
                'decrement'=>array(
                    '0' => array(
                        'where'=>array('#22#'=>1),
                        'method'=>'flush',
                        'cacheName' => 'commentList',
                        'cachePar' => array('#00#','#11#'),//
                    )
                ),
                'forget' => array(
                    
                ),
                'flush'=>array(
                    
                )
            ),
        ),
        
        'LIMIT_TIMES_PER_MIN'=> array(//实体缓存 #00# 评论id
            'type' => 1, //1常量-不经常变的东西 2变量 3实体 4列表 5model
            'SELF_PRE '=>'',
            'tag' => array(
                ),
            'key' => 'LIMIT_TIMES_PER_MIN#00#TOEKN#11#',
            'is_random_time' => FALSE,
            'time' => 1, //defualt 时间
            'before'=> 0,
            'action' => array( 
                
            ),
        ),
        
        'IPBlack'=> array(//实体缓存 #00# 评论id
            'type' => 1, //1常量-不经常变的东西 2变量 3实体 4列表 5model
            'SELF_PRE '=>'',
            'tag' => array(
                0=>'IPBlack',
                ),
            'key' => 'IPBlack#00#',
            'is_random_time' => FALSE,
            'time' => 1, //defualt 时间
            'before'=> 0,
            'action' => array( 
            ),
        ),
        
        'IPBlackHistory'=> array(//实体缓存 #00# 评论id
            'type' => 1, //1常量-不经常变的东西 2变量 3实体 4列表 5model
            'SELF_PRE '=>'',
            'tag' => array(
                0=>'IPBlackHistory',
                ),
            'key' => 'IPBlackHistory#00#',
            'is_random_time' => FALSE,
            'time' => 1, //defualt 时间
            'before'=> 0,
            'action' => array( 
            ),
        ),
        
        'config'=> array(//实体缓存 #00# 评论id
            'type' => 1, //1常量-不经常变的东西 2变量 3实体 4列表 5model
            'SELF_PRE '=>'',
            'tag' => array(
                0=>'config',
                ),
            'key' => 'config#00#',
            'is_random_time' => TRUE,
            'time' => 1, //defualt 时间
            'before'=> 0,
            'action' => array( 
            ),
        ),
        
        'debug'=> array(//实体缓存 #00# 评论id
            'type' => 1, //1常量-不经常变的东西 2变量 3实体 4列表 5model
            'SELF_PRE '=>'',
            'tag' => array(
                0=>'debug',
                ),
            'key' => 'debug#00#',
            'is_random_time' => FALSE,
            'time' => 10, //defualt 时间
            'before'=> 0,
            'action' => array( 
            ),
        ),
        
        'getCacheKeyList'=> array(//实体缓存 #00# 评论id
            'type' => 1, //1常量-不经常变的东西 2变量 3实体 4列表 5model
            'SELF_PRE '=>'',
            'tag' => array(
                ),
            'key' => '#00#',
            'is_random_time' => FALSE,
            'time' => 10, //defualt 时间
            'before'=> 0,
            'action' => array( 
            ),
        ),
        
    );

}
