<?php
namespace App\Utils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class RouteLists {
	//所有route的规则
	public $lists = array(
//		'xxx' => array(// 
//			'method' => 'post',
//			'route'=>'/news/getHotComment',
//			'version' => array(
//				'1.0.0' => '1',
//				'max' => '1', //版本号对应的rules数值
//			),
//			'rules' => array(
//				'1' => array(
//					'isStrict' => 0, //0 不严格，可自己增加参数 1 严格，可以减少参数  2非常严格 参数个数不能变  
//					'isCheckTn' => TRUE, //是否校验tn
//					'auth' => 0, //0不验证登录 1 验证登录
//					'authType' => 1, //1客户端 2web
//					'times'=200,//1-2分钟单个用户 token+ip 执行次数
//					'parameters' => array(
//						'userId' => array('isNull' => FALSE,'isUUid' => TRUE),
//						'userName' => array('isNull' => TRUE),
//						'userImg' => array('isNull' => TRUE),
//						'token' => array('isNull' => TRUE), //isUser 0不验证是否是用户名 1 验证是否是用户名
//						'slogan' => array('isNull' => TRUE),
//						'tn' => array('isNull' => TRUE),
//						'userId' => array('isNull' => FALSE, ','isUUid' => TRUE,value' => array('isNumber'),'isUserId' => TRUE),
//						'objectId' => array('isNull' => FALSE, 'value' => array('isNumber')),
//						'comment' => array('isNull' => FALSE, 'length' => array('min' => 0, 'max' => 256),'checkStop'=> TRUE),
//            			'email' => array('isNull' => FALSE, 'value' => array('isEmail')),
//            			'phone' => array('isNull' => FALSE, 'value' => array('isPhone')),
//            			'phone' => array('isNull' => FALSE, 'value' => array('isFloat')),
//					),
//					'return' => '{"statusCode":0,"message":"Success","timestamp":1478243405}',
//					'debug' => FALSE
//				)
//			)
//		),

/**************************************************国广项目开始************************************************************/
        'Cbn\UserController@verifyCode' => array(// 获取验证码，目前只用于登录
            'method' => 'get',
            'route' => '/cbn/verifyCode',
            'annotation' => '获取验证码(TV/M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>2,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'type' => array('isNull' => TRUE,'description'=>'1验证登录 2其他验证，默认1','value'=>array('1','2')),
                        'phone' => array('isNull' => FALSE, 'description'=>'用户手机号码','value' => array('isPhone')),
                    ),
                    'annotation' => '{"限制":"每个用户每日限制10条"}',
                    'return' => '{"statusCode":0,"message":"Success","timestamp":1479091904}',
                    'debug' => FALSE
                )
            )
        ),
        
        'Cbn\UserController@login' => array(
            'method' => 'post',
            'route' => '/cbn/login',
            'annotation' => '用户登录(TV/M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>10,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'code' => array('isNull' => TRUE,'description'=>'手机验证码','length' => array('min' => 6, 'max' => 6),),
                        'phone' => array('isNull' => FALSE, 'description'=>'用户手机号码','value' => array('isPhone')),
                    ),
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1513928109,"data":{"userId":"9f8db07c561902f4e87b4f697fba19af95","userName":"小D","userType":1,"userIcon":"","slogan":"","birthday":"","gender":"","isFollow":1,"tn":"698dbec94c7bbcbd4800590e38e70532"}}',
                    'debug' => FALSE
                )
            )
        ),
         
        'Cbn\UserController@logout' => array(// 
            'method' => 'post',
            'route' => '/cbn/logout',
            'annotation' => '用户登出(TV/M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>60,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                    ),
                    'annotation' => '{"用户登出":"只需调用接口，不用判断返回值，直接注销登录"}',
                    'return' => '{"statusCode":0,"message":"Success","timestamp":1479091904}',
                    'debug' => FALSE,
                )
            )
        ),
            
        'Cbn\UserController@updateUserInfo' => array(// 
            'method' => 'post',
            'route' => '/cbn/updateUserInfo',
            'annotation' => '更新用户信息(TV/M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'auth' => 0,//1必须登录状态
                    'authType' => 1, //1客户端 2web
                    'times'=>60,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'userIcon' => array('isNull' => TRUE),
                        'nickname' => array('isNull' => TRUE,'length' => array('min' => 1, 'max' => 100),),
                        'birthday' => array('isNull' => TRUE,'value' => array('isNumber'),'description' =>'时间戳'),
                        'slogan' => array('isNull' => TRUE,'length' => array('min' => 1, 'max' => 300),),
                        'gender' => array('isNull' => TRUE,'value' => array('0','1','2'),'description' =>'0:保密,1:男,2:女'),
                    ),
                    'annotation' => '{"注释":"修改什么字段传什么字段，不修改的不传"}',
                    'return' => '{"statusCode":0,"message":"Success","timestamp":1479091904}',
                    'debug' => FALSE,
                )
            )
        ),
            
        'Cbn\UserController@getUserInfo' => array(// 
            'method' => 'get',
            'route' => '/cbn/getUserInfo',
            'annotation' => '获取用户信息(TV/M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>60,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'uId' => array('isNull' => FALSE, 'isUUid' => TRUE,'description' =>'用户ID')
                    ),
//                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1513928635,"data":{"userId":"9f8db07c561902f4e87b4f697fba19af95","nickname":"猪猪侠","userType":1,"userIcon":"e79b588eb2ba6878fce199885dd955e698","slogan":"我的青春谁做主","birthday":1513928268,"gender":1,"isFollow":0}}',
                    'debug' => FALSE,
                )
            )
        ),
        'Cbn\UserController@syncUser' => array(
            'method' => 'post',
            'route' => '/cbn/syncUser',
            'annotation' => '同步用户信息(TV)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>20,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'uuid' => array('isNull' => FALSE,'description'=>'第三方平台uid，用户唯一标示',),
                        'phone' => array('isNull' => FALSE, 'description'=>'用户手机号码','value' => array('isPhone')),
                    ),
                    'annotation' => '{"userType":"1 注册用户 2平台大v编辑用户"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1512550249,"size":1,"data":[{"userId":"e79b588eb2ba6878fce199885dd955e698","nickname":"小F","userType":1,"userIcon":"","isFollow":0}]}',
                    'debug' => FALSE
                )
            )
        ),
        
        'Cbn\UserController@isFollow' => array(
            'method' => 'get',
            'route' => '/cbn/isFollow',
            'annotation' => '判断是否关注(TV/M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>30,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'uId' => array('isNull' => FALSE,'description'=>'uid，用户ID', 'isUUid' => TRUE),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1514170163,"data":{"status":0}}',
                    'debug' => FALSE
                )
            )
        ),
        
        'Cbn\UserController@follow' => array(
            'method' => 'post',
            'route' => '/cbn/follow',
            'annotation' => '关注人(TV/M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>30,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'uId' => array('isNull' => FALSE,'description'=>'uid，用户ID,多个用逗号隔开', 'isUUid' => TRUE),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1512627290}',
                    'debug' => FALSE
                )
            )
        ),
        
        'Cbn\UserController@deleteFollow' => array(
            'method' => 'post',
            'route' => '/cbn/deleteFollow',
            'annotation' => '取消关注人(TV/M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>30,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'uId' => array('isNull' => FALSE,'description'=>'uid，用户ID', 'isUUid' => TRUE),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1512627290}',
                    'debug' => FALSE
                )
            )
        ),
        
        'Cbn\UserController@deleteAllFollow' => array(
            'method' => 'post',
            'route' => '/cbn/deleteAllFollow',
            'annotation' => '取消全部关注的人(TV/M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>10,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1512627290}',
                    'debug' => FALSE
                )
            )
        ),
        'Cbn\UserController@attention' => array(
            'method' => 'post',
            'route' => '/cbn/attention',
            'annotation' => '关注(M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>30,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'type'=>array('isNull' => FALSE, 'description'=>'1话题 2 电影', 'value' =>  array('2','1')),
                        'id' => array('isNull' => FALSE, 'isUUid' => TRUE, 'description'=>'关注对象的id',),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1512627290}',
                    'debug' => FALSE
                )
            )
        ),
        
        'Cbn\UserController@deleteAttention' => array(
            'method' => 'post',
            'route' => '/cbn/deleteAttention',
            'annotation' => '取消关注(M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>30,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'type'=>array('isNull' => FALSE, 'description'=>'1话题 2 电影', 'value' =>  array('1','2','3')),
                        'id' => array('isNull' => FALSE, 'isUUid' => TRUE, 'description'=>'关注对象的id',),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1512627290}',
                    'debug' => FALSE
                )
            )
        ),
        'Cbn\UserController@deleteALLAttention' => array(
            'method' => 'post',
            'route' => '/cbn/deleteAllAttention',
            'annotation' => '取消全部关注(M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>10,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'type'=>array('isNull' => FALSE, 'description'=>'1话题 2 电影', 'value' =>  array('1','2','3')),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1512627290}',
                    'debug' => FALSE
                )
            )
        ),
        
        'Cbn\UserController@isFavarite' => array(
            'method' => 'get',
            'route' => '/cbn/isFavarite',
            'annotation' => '判断是否收藏(TV/M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>30,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'type'=>array('isNull' => FALSE, 'description'=>'1收藏内容 2 预留', 'value' =>  array('1','2','3')),
                        'id' => array('isNull' => FALSE,'description'=>'收藏对象的id','isUUid' => TRUE),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1512552154,"data":{"status":0}}',
                    'debug' => FALSE
                )
            )
        ),
        
        'Cbn\UserController@favarite' => array(
            'method' => 'post',
            'route' => '/cbn/favarite',
            'annotation' => '收藏(TV/M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>30,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'type'=>array('isNull' => FALSE, 'description'=>'1收藏内容 2 预留', 'value' =>  array('1','2','3')),
                        'id' => array('isNull' => FALSE,'description'=>'收藏对象的id','isUUid' => TRUE),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1512627290}',
                    'debug' => FALSE
                )
            )
        ),
        
        'Cbn\UserController@deleteFavarite' => array(
            'method' => 'post',
            'route' => '/cbn/deleteFavarite',
            'annotation' => '取消收藏(TV/M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>30,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'type'=>array('isNull' => FALSE, 'description'=>'1收藏内容 2 预留', 'value' =>  array('1','2','3')),
                        'id' => array('isNull' => FALSE,'description'=>'收藏对象的id','isUUid' => TRUE),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1512627290}',
                    'debug' => FALSE
                )
            )
        ),
        
        'Cbn\UserController@deleteAllFavarite' => array(
            'method' => 'post',
            'route' => '/cbn/deleteAllFavarite',
            'annotation' => '取消全部收藏(TV/M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>10,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'type'=>array('isNull' => FALSE, 'description'=>'1收藏内容 2 预留', 'value' =>  array('1','2','3')),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1512627290}',
                    'debug' => FALSE
                )
            )
        ),
        
        'Cbn\UserController@getFollowerList' => array(
            'method' => 'get',
            'route' => '/cbn/getFollowerList',
            'annotation' => '获取用户关注人的列表(TV/M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>30,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'uId' => array('isNull' => FALSE,'description'=>'uid，用户ID', 'isUUid' => TRUE),
                        'page' => array('isNull' => FALSE, 'value' => array('isNumber')),
                        'size' => array('isNull' => FALSE, 'value' => array('isNumber')),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1513929741,"size":2,"data":[{"userId":"e79b588eb2ba6878fce199885dd955e698","nickname":"小F","userType":1,"userIcon":"4cc0bc804c4510e1a87eb9394d079419","slogan":"","birthday":"","gender":"","isFollow":1},{"userId":"ea1c3b2b3b0ffce6916998240895c20f1","nickname":"科幻电影赏析","userType":2,"userIcon":"978a07f6d14d514791d747cdddf58951","slogan":"","birthday":"","gender":"","isFollow":1}]}',
                    'debug' => FALSE
                )
            )
        ),
        
        'Cbn\UserController@getFansList' => array(
            'method' => 'get',
            'route' => '/cbn/getFansList',
            'annotation' => '获取用户粉丝的列表(TV/M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>30,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'uId' => array('isNull' => FALSE,'description'=>'uid，用户ID', 'isUUid' => TRUE),
                        'page' => array('isNull' => FALSE, 'value' => array('isNumber')),
                        'size' => array('isNull' => FALSE, 'value' => array('isNumber')),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1513929741,"size":2,"data":[{"userId":"e79b588eb2ba6878fce199885dd955e698","nickname":"小F","userType":1,"userIcon":"4cc0bc804c4510e1a87eb9394d079419","slogan":"","birthday":"","gender":"","isFollow":1},{"userId":"ea1c3b2b3b0ffce6916998240895c20f1","nickname":"科幻电影赏析","userType":2,"userIcon":"978a07f6d14d514791d747cdddf58951","slogan":"","birthday":"","gender":"","isFollow":1}]}',
                    'debug' => FALSE
                )
            )
        ),
        
        'Cbn\UserController@getAttentionList' => array(
            'method' => 'get',
            'route' => '/cbn/getAttentionList',
            'annotation' => '获取用户关注的列表(M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>30,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'uId' => array('isNull' => FALSE,'description'=>'uid，用户ID', 'isUUid' => TRUE),
                        'type'=>array('isNull' => FALSE, 'description'=>'1话题 2 电影', 'value' =>  array('1','2','3')),
                        'page' => array('isNull' => FALSE, 'value' => array('isNumber')),
                        'size' => array('isNull' => FALSE, 'value' => array('isNumber')),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1514294598,"size":2,"data":[{"topicId":"d87f7d6d21164e50f196ecbe714deb68","title":"喜欢篮球的朋友,肯定都喜欢NBA,一起来侃球吧","img":"","introduction":"篮球作为一项大球运动,或许在国际上的地位比不上足球,单着并不能影响热血青年对它的喜爱","keywords":"篮球","fansTotal":999,"discussTotal":99,"isAttention":0},{"topicId":"d87f7d6d21164e50f196ecbe714deb68","title":"喜欢篮球,喜欢它的暴力,喜欢它的激情","img":"","introduction":"篮球相对于足球,优势是他对场地的要求更低,","keywords":"篮球","fansTotal":999,"discussTotal":99,"isAttention":0}]}',
                    'debug' => FALSE
                )
            )
        ),
        
        'Cbn\UserController@getFavariteList' => array(
            'method' => 'get',
            'route' => '/cbn/getFavariteList',
            'annotation' => '获取用户收藏的列表(TV/M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>30,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'uId' => array('isNull' => FALSE,'description'=>'uid，用户ID', 'isUUid' => TRUE),
                        'type'=>array('isNull' => FALSE, 'description'=>'1收藏内容 2 预留', 'value' =>  array('1','2','3')),
                        'page' => array('isNull' => FALSE, 'value' => array('isNumber')),
                        'size' => array('isNull' => FALSE, 'value' => array('isNumber')),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1513930399,"size":2,"data":[{"contentId":"ea1c3b2b3b0ffce6916998240895c20f1","content":"标准好莱坞爆米花商业电影，大场面，duangduang满天飞的特效，然而除此之外就神马也没有了。三傻的凤凰蛮失败，距离原版的法米克詹森差的不是一点半点。快银片段不如上部来的好，被消费过度。金刚狼打了个酱油，与三傻的对戏尴尬满满。大反派白内障患者天启，完全是被自己给作死的，智商感人。","contentImg":"22b8f25e1bf1223b5757ab228a68f36a","startTime":2,"endTime":"","releaseTime":1512725981,"topicId":"ea1c3b2b3b0ffce6916998240895c20f1","topicName":"智商感人","programId":"e12d35e1a38acbb908d8bfa8466d05e312","programName":"一代宗师","programUrl":"b48eebfae89ea0b1b100d07e7c104220","userId":"ea1c3b2b3b0ffce6916998240895c20f1","userName":"科幻电影赏析","userImg":"978a07f6d14d514791d747cdddf58951","userType":2},{"contentId":"0d0135cc4bb62bd14c32594862c57acd2","content":"继续消费快银时刻，而且还是全片最亮点。几个新加入的角色无论是表现还是戏份都不错，也算是给之后做铺垫了。剧本本身比较套路，反派太符号化了，也不讨喜，属于执念型，一门心思毁灭世界。天启四骑士除了万磁王都像打酱油的，晃了几下就都围观了。前传三部曲最平庸的一部，感觉记忆点不多。","contentImg":"22b8f25e1bf1223b5757ab228a68f36a","startTime":5,"endTime":"","releaseTime":1512725881,"topicId":"ea1c3b2b3b0ffce6916998240895c20f1","topicName":"智商感人","programId":"ea1c3b2b3b0ffce6916998240895c20f1","programName":"X战警：天启","programUrl":"b48eebfae89ea0b1b100d07e7c104220","userId":"ea1c3b2b3b0ffce6916998240895c20f1","userName":"科幻电影赏析","userImg":"978a07f6d14d514791d747cdddf58951","userType":2}]}',
                    'debug' => FALSE
                )
            )
        ),
            
        'Cbn\TopicController@getCategory' => array(
            'method' => 'get',
            'route' => '/cbn/getCategory',
            'annotation' => '获取分类(TV)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>30,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return'=> '{
                                    "statusCode": 0,
                                    "message": "成功",
                                    "timestamp": 1512552781,
                                    "data": {
                                        "data": [
                                            {
                                                "categoryId": 58,
                                                "name": "科技",
                                                "keywords": "科技视频"
                                            },
                                            {
                                                "categoryId": 59,
                                                "name": "汽车",
                                                "keywords": "汽车视频"
                                            },
                                            {
                                                "categoryId": 60,
                                                "name": "搞笑",
                                                "keywords": "搞笑视频"
                                            }
                                        ]
                                    }
                                }',
                    'debug' => FALSE
                )
            )
        ),
           
        'Cbn\TopicController@getCategory' => array(
            'method' => 'get',
            'route' => '/cbn/v2/getCategory',//最新为TV增加的一个接口
            'annotation' => '获取分类(TV)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>30,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return'=> '{
                                    "statusCode": 0,
                                    "message": "成功",
                                    "timestamp": 1512552781,
                                    "data": {
                                        "data": [
                                            {
                                                "categoryId": 58,
                                                "name": "科技",
                                                "keywords": "科技视频"
                                            },
                                            {
                                                "categoryId": 59,
                                                "name": "汽车",
                                                "keywords": "汽车视频"
                                            },
                                            {
                                                "categoryId": 60,
                                                "name": "搞笑",
                                                "keywords": "搞笑视频"
                                            }
                                        ]
                                    }
                                }',
                    'debug' => FALSE
                )
            )
        ),
        
        'Cbn\TopicController@getTopic' => array(
            'method' => 'get',
            'route' => '/cbn/getTopic',
            'annotation' => '获取电影话题(TV/M)，用于发布时候的列表',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>30,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'programId'=> array('isNull' => FALSE,'description'=>'节目id', 'isUUid' => TRUE),
                        'keywords' => array('isNull' => FALSE, 'description'=>'多个关键词，逗号隔开', 'length' => array('min' => 2, 'max' => 200)),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return'=> '{"statusCode":0,"message":"成功","timestamp":1512553157,"data":[{"title":"搞笑","img":"","introduction":"","keywords":"搞笑,视频"},{"title":"穿帮","img":"","introduction":"","keywords":"穿帮,视频"},{"title":"犀利","img":"","introduction":"","keywords":"犀利,视频"},{"title":"恶搞","img":"","introduction":"","keywords":"恶搞,视频"}]}',
                    'debug' => FALSE
                )
            )
        ),
        
        'Cbn\TopicController@getProgramTopicList' => array(
            'method' => 'get',
            'route' => '/cbn/getProgramTopicList',
            'annotation' => '获取节目详情页下的话题列表(TV/M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>30,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'programId'=> array('isNull' => FALSE,'description'=>'节目id', 'isUUid' => TRUE),
                        'page' => array('isNull' => FALSE, 'value' => array('isNumber')),
                        'size' => array('isNull' => FALSE, 'value' => array('isNumber')),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return'=> '{"statusCode":0,"message":"成功","timestamp":1512553157,"data":[{"title":"搞笑","img":"","introduction":"","keywords":"搞笑,视频"},{"title":"穿帮","img":"","introduction":"","keywords":"穿帮,视频"},{"title":"犀利","img":"","introduction":"","keywords":"犀利,视频"},{"title":"恶搞","img":"","introduction":"","keywords":"恶搞,视频"}]}',
                    'debug' => FALSE
                )
            )
        ),
        'Cbn\TopicController@getRecommendItemList' => array(// 
            'method' => 'get',
            'route' => '/cbn/getRecommendItemList',
            'annotation' => '获取推荐位(关注.发现.搜索页)(M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>60,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'tag' => array('isNull' => FALSE, 'description'=>'推荐位tag'),
                        'type' => array('isNull' => FALSE, 'description'=>'1 无链接、2 内容、3 话题，4 节目 5 用户', 'value' => array('isNumber')),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1514289750,"data":{"title":"优质UP主推荐","type":8,"data":[{"userId":"ea1c3b2b3b0ffce6916998240895c20f1","nickname":"科幻电影赏析","userType":2,"userIcon":"978a07f6d14d514791d747cdddf58951","slogan":"","birthday":"","gender":"","isFollow":0,"fansTotal":99,"followTotal":999},{"userId":"0d0135cc4bb62bd14c32594862c57acd2","nickname":"热血镜头","userType":2,"userIcon":"6c19aa8e70c4eb8a81b0c2242fb75049","slogan":"","birthday":"","gender":"","isFollow":0,"fansTotal":99,"followTotal":999},{"userId":"a3ecc272a48058f5c1822e6e4ee9833b3","nickname":"欧美电影大赏","userType":2,"userIcon":"f5c0dfee3c74cc40b67e577e40b99466","slogan":"","birthday":"","gender":"","isFollow":0,"fansTotal":99,"followTotal":999}],"size":3}}',
                    'debug' => FALSE,
                )
            )
        ),
            
        'Cbn\TopicController@getAllTopic' => array(// 
            'method' => 'get',
            'route' => '/cbn/getAllTopic',
            'annotation' => '获取全部话题(一期不带分类)(M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>60,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'page' => array('isNull' => FALSE, 'value' => array('isNumber')),
                        'size' => array('isNull' => FALSE, 'value' => array('isNumber')),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1514201502,"data":{"data":[{"topicId":"f94bc32cc3a1ee03fd98317b3dee4a3022","title":"真的英雄，都不会被人知晓","img":"","introduction":"","keywords":"","fansTotal":999,"discussTotal":99},{"topicId":"b8d73b338f2571a0de7cc04a262b517c23","title":"武林往事","img":"","introduction":"","keywords":"科技","fansTotal":999,"discussTotal":99}],"paging":{"size":2}}}',
                    'debug' => FALSE,
                )
            )
        ),
        
        'Cbn\ContentController@getUserContentList' => array(
            'method' => 'get',
            'route' => '/cbn/getUserContentList',
            'annotation' => '获取用户的内容列表(TV/M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>30,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'uId' => array('isNull' => FALSE,'description'=>'uid，用户ID', 'isUUid' => TRUE),
                        'page' => array('isNull' => FALSE, 'value' => array('isNumber')),
                        'size' => array('isNull' => FALSE, 'value' => array('isNumber')),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1513931757,"size":2,"data":[{"contentId":"e71e287e2fbabdd9e7c05a6e7c79f1c058","content":"发表的内容","contentImg":"3b10d3c09dd53c3a84fa18f0fe3fe64e","startTime":10,"endTime":30,"releaseTime":1513904647,"topicId":"e79b588eb2ba6878fce199885dd955e698","topicName":"话题名称","programId":"29e326012064de6b66841e7e30596ef50","programName":"西游记","programUrl":"www.baidu.com","userId":"9f8db07c561902f4e87b4f697fba19af95","userName":"猪猪侠","userImg":"e79b588eb2ba6878fce199885dd955e698","userType":1},{"contentId":"e1a6e365b07d242fa3a9d1882b4cb05357","content":"发表的内容","contentImg":"3b10d3c09dd53c3a84fa18f0fe3fe64e","startTime":10,"endTime":30,"releaseTime":1513904572,"topicId":"e79b588eb2ba6878fce199885dd955e698","topicName":"话题名称","programId":"29e326012064de6b66841e7e30596ef50","programName":"西游记","programUrl":"www.baidu.com","userId":"9f8db07c561902f4e87b4f697fba19af95","userName":"猪猪侠","userImg":"e79b588eb2ba6878fce199885dd955e698","userType":1}]}',
                    'debug' => FALSE
                )
            )
        ),
            
        'Cbn\ContentController@getContentList' => array(
            'method' => 'get',
            'route' => '/cbn/getContentList',
            'annotation' => '获取分类下的内容列表(TV)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>60,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'categoryId' => array('isNull' => FALSE,'description'=>'分类Id',),
                        'page' => array('isNull' => FALSE, 'value' => array('isNumber')),
                        'size' => array('isNull' => FALSE, 'value' => array('isNumber')),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1512553955,"size":4,"data":[{"contentId":"ea1c3b2b3b0ffce6916998240895c20f1","contentImg":"img","content":"这是一段视频评论,我也不知道要些啥","programUrl":"0ec55c269ddd1f2bd632a302e8fc20ebbase","startTime":10,"videoTime":10,"releaseTime":1512553422,"topicId":"ea1c3b2b3b0ffce6916998240895c20f1","topicName":"搞笑","programId":"ea1c3b2b3b0ffce6916998240895c20f1","programName":"成真恋爱学","userId":"9f8db07c561902f4e87b4f697fba19af95","userType":1,"userName":"小A","userImg":""},{"contentId":"ea1c3b2b3b0ffce6916998240895c20f1","contentImg":"img","content":"这是一段视频评论,我也不知道要些啥","programUrl":"0ec55c269ddd1f2bd632a302e8fc20ebbase","startTime":10,"videoTime":10,"releaseTime":1512553422,"topicId":"ea1c3b2b3b0ffce6916998240895c20f1","topicName":"搞笑","programId":"ea1c3b2b3b0ffce6916998240895c20f1","programName":"成真恋爱学","userId":"9f8db07c561902f4e87b4f697fba19af95","userType":1,"userName":"小A","userImg":""},{"contentId":"0d0135cc4bb62bd14c32594862c57acd2","contentImg":"img","content":"本系列以可可小爱一家来演绎公民道德、生态环保、文明礼仪、法制宣传、安全教育等公益主题。","programUrl":"07500afe077f812fac01c3df51fb325ebase","startTime":15,"videoTime":10,"releaseTime":1512552422,"topicId":"ea1c3b2b3b0ffce6916998240895c20f1","topicName":"搞笑","programId":"0d0135cc4bb62bd14c32594862c57acd2","programName":"可可小爱科普系列","userId":"9f8db07c561902f4e87b4f697fba19af95","userType":1,"userName":"小A","userImg":""},{"contentId":"0d0135cc4bb62bd14c32594862c57acd2","contentImg":"img","content":"本系列以可可小爱一家来演绎公民道德、生态环保、文明礼仪、法制宣传、安全教育等公益主题。","programUrl":"07500afe077f812fac01c3df51fb325ebase","startTime":15,"videoTime":10,"releaseTime":1512552422,"topicId":"ea1c3b2b3b0ffce6916998240895c20f1","topicName":"搞笑","programId":"0d0135cc4bb62bd14c32594862c57acd2","programName":"可可小爱科普系列","userId":"9f8db07c561902f4e87b4f697fba19af95","userType":1,"userName":"小A","userImg":""}]}',
                    'debug' => FALSE
                )
            )
        ),
        
        'Cbn\ContentController@getFollowerContentList' => array(
            'method' => 'get',
            'route' => '/cbn/getFollowerContentList',
            'annotation' => '获取关注人们的内容列表(TV/M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>60,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'page' => array('isNull' => FALSE, 'value' => array('isNumber')),
                        'size' => array('isNull' => FALSE, 'value' => array('isNumber')),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1514256402,"size":2,"data":[{"contentId":"e71e287e2fbabdd9e7c05a6e7c79f1c058","content":"发表的内容","contentImg":"3b10d3c09dd53c3a84fa18f0fe3fe64e","startTime":10,"endTime":30,"releaseTime":1513904647,"topicId":"e79b588eb2ba6878fce199885dd955e698","topicName":"话题名称","programId":"29e326012064de6b66841e7e30596ef50","programName":"西游记","programUrl":"www.baidu.com","userId":"e79b588eb2ba6878fce199885dd955e698","userName":"小F","userImg":"4cc0bc804c4510e1a87eb9394d079419","userType":1,"commentTotal":0,"praiseTotal":0,"isAttention":0},{"contentId":"e1a6e365b07d242fa3a9d1882b4cb05357","content":"发表的内容","contentImg":"3b10d3c09dd53c3a84fa18f0fe3fe64e","startTime":10,"endTime":30,"releaseTime":1513904572,"topicId":"e79b588eb2ba6878fce199885dd955e698","topicName":"话题名称","programId":"29e326012064de6b66841e7e30596ef50","programName":"西游记","programUrl":"www.baidu.com","userId":"e79b588eb2ba6878fce199885dd955e698","userName":"小F","userImg":"4cc0bc804c4510e1a87eb9394d079419","userType":1,"commentTotal":0,"praiseTotal":0,"isAttention":0}]}',
                    'debug' => FALSE
                )
            )
        ),
            
        'Cbn\ContentController@createContent' => array(
            'method' => 'post',
            'route' => '/cbn/createContent',
            'annotation' => '发布内容(TV/M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>10,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'topicId' => array('isNull' => FALSE,'description'=>'话题ID', 'isUUid' => TRUE),
                        'topic' => array('isNull' => FALSE,'description'=>'话题名称'),
                        'programId'=> array('isNull' => FALSE,'description'=>'节目id'),
                        'programName'=> array('isNull' => FALSE,'description'=>'节目名称'),
                        'url'=> array('isNull' => FALSE,'description'=>'节目链接'),
                        'starTime'=> array('isNull' => FALSE,'description'=>'短视频开始时间','value' => array('isNumber')),
                        'endTime'=> array('isNull' => FALSE,'description'=>'短视频结束时间','value' => array('isNumber')),
                        'img'=> array('isNull' => FALSE,'description'=>'视频截图'),
                        'content'=> array('isNull' => FALSE,'description'=>'内容'),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1513931924}',
                    'debug' => FALSE
                )
            )
        ),
            
        'Cbn\ContentController@getRecommendContentList' => array(// 
            'method' => 'get',
            'route' => '/cbn/getRecommendContentList',
            'annotation' => '获取推荐下内容列表(M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>60,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'page' => array('isNull' => FALSE, 'value' => array('isNumber')),
                        'size' => array('isNull' => FALSE, 'value' => array('isNumber')),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1514260594,"size":2,"data":[{"contentId":"6faf640fa54fa84021b69d61c9617f3c56","content":"非常优质的电影，看得让人如痴如醉……River有点像谍影重重里的马特代蒙，最厉害的武器是人！","contentImg":"3c17091d03bc8d6e5cc02eaed6235d01","startTime":10,"endTime":50,"releaseTime":1513838247,"topicId":"e12d35e1a38acbb908d8bfa8466d05e312","topicName":"人类创造了另一个“上帝”","programId":"843cc4e580095b243b6de1e51658800c6","programName":"异形：契约","programUrl":"b48eebfae89ea0b1b100d07e7c104220","userId":"a3ecc272a48058f5c1822e6e4ee9833b3","userName":"欧美电影大赏","userImg":"f5c0dfee3c74cc40b67e577e40b99466","userType":2,"commentTotal":0,"praiseTotal":0,"isAttention":0},{"contentId":"4942ca60672bc43565c1d248423c72cf55","content":"如此的电影,真好看","contentImg":"7488b0b3b57ca2663abf5ee4cb6203a5","startTime":316,"endTime":355,"releaseTime":1513680288,"topicId":"a3ecc272a48058f5c1822e6e4ee9833b3","topicName":"好久没看到如此好的影片了","programId":"157b08d08884fdb3cb205b57a729ed2215","programName":"奇门遁甲","programUrl":"b48eebfae89ea0b1b100d07e7c104220","userId":"843cc4e580095b243b6de1e51658800c6","userName":"国产大片","userImg":"5b24ee81a4589d87e68a2d6fcc664f32","userType":2,"commentTotal":0,"praiseTotal":0,"isAttention":0}]}',
                    'debug' => FALSE,
                )
            )
        ),
               
        'Cbn\ContentController@getSearchList' => array(// 
            'method' => 'get',
            'route' => '/cbn/getSearchList',
            'annotation' => '获取搜索结果(话题.用户.视频.内容)(M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>60,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'type' => array('isNull' => FALSE, 'description'=>'1:话题,2:用户,3:视频,4:内容', 'value' => array('1','2','3','4')),
                        'keywords' => array('isNull' => FALSE, 'description'=>'多个关键词，逗号隔开', 'length' => array('min' => 1, 'max' => 200)),
                        'page' => array('isNull' => FALSE, 'value' => array('isNumber')),
                        'size' => array('isNull' => FALSE, 'value' => array('isNumber')),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"Success","timestamp":1479091904}',
                    'debug' => FALSE,
                )
            )
        ),
        'Cbn\TopicController@getTopicDetail' => array(// 
            'method' => 'get',
            'route' => '/cbn/getTopicDetail',
            'annotation' => '获取话题详情(M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>60,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'topicId' => array('isNull' => FALSE,'description'=>'话题id', 'isUUid' => TRUE),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1514205985,"data":{"topicId":"c131532a6a14093821eaea9b1066cf2427","title":"优秀的科幻片是什么样的","img":"22ce4b3ee6293652d07d91e30ac0929b","introduction":"一起聊聊优秀的科幻片。","keywords":"科幻","fansTotal":999,"discussTotal":99,"isAttention":1}}',
                    'debug' => FALSE,
                )
            )
        ),
        'Cbn\ContentController@getTopicContentList' => array(// 
            'method' => 'get',
            'route' => '/cbn/getTopicContentList',
            'annotation' => '获取话题下的内容(不包含话题详情)(M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>60,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'topicId' => array('isNull' => FALSE,'description'=>'话题id', 'isUUid' => TRUE),
                        'page' => array('isNull' => FALSE, 'value' => array('isNumber')),
                        'size' => array('isNull' => FALSE, 'value' => array('isNumber')),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1513933981,"data":{"data":[{"contentId":"247562beae4c02e791871826f8b3ec0260","content":"这个地址不对吧老大","contentImg":"","startTime":0,"endTime":1530,"releaseTime":1513927857,"topicId":"97350ec8d5f84b2c031c26511a8945b224","topicName":"电影工匠","programId":"2ee95734077c3a94ddfaba2e46e10bc618","programName":"外部地址","programUrl":"www.baidu,com","userId":"843cc4e580095b243b6de1e51658800c6","userName":"国产大片","userImg":"5b24ee81a4589d87e68a2d6fcc664f32","userType":2},{"contentId":"560a53be4ff547f293521deb82ff93cd59","content":"这个传错了吧小编","contentImg":"","startTime":9,"endTime":105,"releaseTime":1513927633,"topicId":"97350ec8d5f84b2c031c26511a8945b224","topicName":"电影工匠","programId":"c3adc0fff1169905aa8bfeb3d02beff517","programName":"追龙","programUrl":"757ce5d6698915f23fd7fe055b44370c","userId":"843cc4e580095b243b6de1e51658800c6","userName":"国产大片","userImg":"5b24ee81a4589d87e68a2d6fcc664f32","userType":2}],"paging":{"size":2}}}',
                    'debug' => FALSE,
                )
            )
        ),
        'Cbn\ProgramController@getProgramDetail' => array(// 
            'method' => 'get',
            'route' => '/cbn/getProgramDetail',
            'annotation' => '获取电影详情(M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>60,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'programId' => array('isNull' => FALSE,'description'=>'电影id', 'isUUid' => TRUE),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1514275569,"data":{"programId":"ea1c3b2b3b0ffce6916998240895c20f1","programName":"X战警：天启","programUrl":"b48eebfae89ea0b1b100d07e7c104220","img":"22b8f25e1bf1223b5757ab228a68f36a","keywords":"","directors":"","scriptwriter":"","actors":"","area":"4","type":"","publishAt":"2017","introduction":"","isAttention":0}}',
                    'debug' => FALSE,
                )
            )
        ),
        'Cbn\ContentController@getProgramContentList' => array(// 
            'method' => 'get',
            'route' => '/cbn/getProgramContentList',
            'annotation' => '获取电影下的内容(不包含电影详情)(M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>60,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'programId' => array('isNull' => FALSE,'description'=>'电影id', 'isUUid' => TRUE),
                        'page' => array('isNull' => FALSE, 'value' => array('isNumber')),
                        'size' => array('isNull' => FALSE, 'value' => array('isNumber')),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1513936633,"data":{"data":[{"contentId":"5aa1fed7d7e1b44aa5c274394c847f6425","content":"我是有爱丽丝情节的人，熟悉的音乐一起来爱丽丝在海上乘风破浪的时候差点泪奔。特效特别棒，时间城堡很美，特别是满天挂钟那一幕，特别有意境。","contentImg":"a3764a2b7b1659e8cd818bc48025c2ef","startTime":5,"endTime":"","releaseTime":1512723582,"topicId":"64961a357546c3d275dfd83fc2510d7813","topicName":"爱丽丝情结患者的盛宴","programId":"47271b02101d2a5572e233390c210ee87","programName":"爱丽丝梦游仙境2：镜中奇遇记","programUrl":"b48eebfae89ea0b1b100d07e7c104220","userId":"2a86ff237ab7657b9d9b0f64055b90dc4","userName":"影视怪咖","userImg":"f8d2930244dcb3d3464691c95545ca73","userType":2},{"contentId":"93b7dfcaacbfea336399fc686aa9f65626","content":"时隔六年，我已经不是一个第一次自己拿钱买票进电影院的大学生，感谢迪斯尼再次给我圆了爱丽丝梦。让我想起小学一年级，爸爸妈妈带我到新华书店，买了第一本童话书：《爱丽丝梦游仙境》。","contentImg":"a3764a2b7b1659e8cd818bc48025c2ef","startTime":5,"endTime":"","releaseTime":1512723482,"topicId":"64961a357546c3d275dfd83fc2510d7813","topicName":"爱丽丝情结患者的盛宴","programId":"47271b02101d2a5572e233390c210ee87","programName":"爱丽丝梦游仙境2：镜中奇遇记","programUrl":"b48eebfae89ea0b1b100d07e7c104220","userId":"2a86ff237ab7657b9d9b0f64055b90dc4","userName":"影视怪咖","userImg":"f8d2930244dcb3d3464691c95545ca73","userType":2}],"paging":{"size":2}}}',
                    'debug' => FALSE,
                )
            )
        ),
           
        'Cbn\ContentController@getContentDetail' => array(// 
            'method' => 'get',
            'route' => '/cbn/getContentDetail',
            'annotation' => '获取内容详情(M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>60,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'contentId' => array('isNull' => FALSE,'description'=>'内容id', 'isUUid' => TRUE)
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1513937214,"data":{"contentId":"ea1c3b2b3b0ffce6916998240895c20f1","content":"标准好莱坞爆米花商业电影，大场面，duangduang满天飞的特效，然而除此之外就神马也没有了。三傻的凤凰蛮失败，距离原版的法米克詹森差的不是一点半点。快银片段不如上部来的好，被消费过度。金刚狼打了个酱油，与三傻的对戏尴尬满满。大反派白内障患者天启，完全是被自己给作死的，智商感人。","contentImg":"22b8f25e1bf1223b5757ab228a68f36a","startTime":2,"endTime":9,"releaseTime":1512725981,"topicId":"ea1c3b2b3b0ffce6916998240895c20f1","topicName":"智商感人","programId":"e12d35e1a38acbb908d8bfa8466d05e312","programName":"一代宗师","programUrl":"b48eebfae89ea0b1b100d07e7c104220","userId":"ea1c3b2b3b0ffce6916998240895c20f1","userName":"科幻电影赏析","userImg":"978a07f6d14d514791d747cdddf58951","userType":2}}',
                    'debug' => FALSE,
                )
            )
        ),
            
        'Cbn\ContentController@isPraise' => array(
            'method' => 'get',
            'route' => '/cbn/isPraise',
            'annotation' => '判断是否点赞(TV/M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>30,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'type'=>array('isNull' => FALSE, 'description'=>'1:内容 2 评论', 'value' =>  array('1','2','3')),
                        'itemId' => array('isNull' => FALSE,'description'=>'点赞id','isUUid' => TRUE),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1512552154,"data":{"status":0}}',
                    'debug' => FALSE
                )
            )
        ),
        'Cbn\ContentController@createPraise' => array(// 
            'method' => 'get',
            'route' => '/cbn/createPraise',
            'annotation' => '内容/评论点赞(M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>60,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'type' => array('isNull' => FALSE, 'description'=>'1:内容,2:评论','value' => array('1','2')),
                        'itemId' => array('isNull' => FALSE, 'isUUid' => TRUE,'description'=>'内容/评论id')
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1513937413}',
                    'debug' => FALSE,
                )
            )
        ),
            
        'Cbn\ContentController@cancelPraise' => array(// 
            'method' => 'get',
            'route' => '/cbn/cancelPraise',
            'annotation' => '内容/评论取消点赞(M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>60,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'type' => array('isNull' => FALSE, 'description'=>'1:内容,2:评论','value' => array('1','2')),
                        'itemId' => array('isNull' => FALSE, 'isUUid' => TRUE,'description'=>'内容/评论id')
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"Success","timestamp":1479091904}',
                    'debug' => FALSE,
                )
            )
        ),
            
        'Cbn\ProgramController@syncProgram' => array(
            'method' => 'post',
            'route' => '/cbn/syncProgram',
            'annotation' => '同步视频节目(M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>10,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'from' => array('isNull' => FALSE, 'description'=>'视频来源 1 国广 2 PASS'),
                        'programId'=> array('isNull' => FALSE,'description'=>'第三方平台唯一id'),
                        'programName'=> array('isNull' => TRUE,'description'=>'视频名称'),
                        'keywords' => array('isNull' => TRUE, 'description'=>'多个关键词，逗号隔开', 'length' => array('min' => 2, 'max' => 200)),
                        'img'=> array('isNull' => TRUE,'description'=>'视频截图或者海报'),
                        'programUrl'=> array('isNull' => FALSE,'description'=>'内容'),//
                        'directors'=> array('isNull' => TRUE,'description'=>'导演'),
                        'scriptwriter'=> array('isNull' => TRUE,'description'=>'编剧'),
                        'actors'=> array('isNull' => TRUE,'description'=>'主演'),
                        'area'=> array('isNull' => TRUE,'description'=>'地区'),
                        'type'=> array('isNull' => TRUE,'description'=>'类型'),
                        'publishAt'=> array('isNull' => TRUE,'description'=>'发布时间'),
                        'introduction'=> array('isNull' => TRUE,'description'=>'简介'),
//                        'curTime'=> array('isNull' => FALSE,'description'=>'当前播放时间'),
//                        'videoTime'=> array('isNull' => FALSE,'description'=>'总时长'),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1512552464,"data":{"programId":"ea1c3b2b3b0ffce6916998240895c20f1"}}',
                    'debug' => FALSE
                )
            )
        ),
        //集合在getAttentionList里type=2时
//        'Cbn\ProgramController@getUserProgramList' => array(// 
//            'method' => 'get',
//            'route' => '/cbn/getUserProgramList',
//            'annotation' => '获取关注的电影(M)',
//            'version' => array(
//                '1.0.0' => '1',
//                'max' => '1',
//            ),
//            'rules' => array(
//                '1' => array(
//                    'isStrict' => 1,
//                    'auth' => 0,
//                    'authType' => 1, //1客户端 2web
//                    'times'=>60,//1-2分钟单个用户 token+ip 执行次数
//                    'parameters' => array(
//                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
//                        'v' => array('isNull' => FALSE),
//                        'dk' => array('isNull' => FALSE),
//                        'tn' => array('isNull' => FALSE),
//                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
//                        'uId' => array('isNull' => FALSE, 'isUUid' => TRUE),
//                        'page' => array('isNull' => FALSE, 'value' => array('isNumber')),
//                        'size' => array('isNull' => FALSE, 'value' => array('isNumber')),
//                    ),
//                    'annotation' => '{"字段":"解释"}',
//                    'return' => '{"statusCode":0,"message":"成功","timestamp":1514164342,"data":{"data":[{"programId":"0d0135cc4bb62bd14c32594862c57acd2","programName":"冲出宁静号","programUrl":"b48eebfae89ea0b1b100d07e7c104220","img":"ca98bcb88e2f4f6c778e665c19038446","keywords":""},{"programId":"a3ecc272a48058f5c1822e6e4ee9833b3","programName":"搏击俱乐部","programUrl":"b48eebfae89ea0b1b100d07e7c104220","img":"2411c34d59a08b0a9aa3166cda4f4790","keywords":""},{"programId":"2a86ff237ab7657b9d9b0f64055b90dc4","programName":"速度与激情8","programUrl":"b48eebfae89ea0b1b100d07e7c104220","img":"15cc4962b8b0e88bdc3aa64b58631f77","keywords":""},{"programId":"f0c93781270e9c859140c3d0b706fec35","programName":"异星觉醒","programUrl":"b48eebfae89ea0b1b100d07e7c104220","img":"278673462980ce98c8fc7dc072b67106","keywords":""}],"paging":{"size":4}}}',
//                    'debug' => FALSE,
//                )
//            )
//        ),
        
        'Cbn\CommentController@getContentCommentList' => array(// 
            'method' => 'get',
            'route' => '/cbn/getContentCommentList',
            'annotation' => '获取内容下的评论(M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>60,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'contentId' => array('isNull' => FALSE, 'description'=>'内容id', 'isUUid' => TRUE),
                        'page' => array('isNull' => FALSE, 'value' => array('isNumber')),
                        'size' => array('isNull' => FALSE, 'value' => array('isNumber')),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1514165012,"data":{"data":[{"commentId":7,"contentId":"0d0135cc4bb62bd14c32594862c57acd2","content":"发表的评论","publishAt":1513910302,"userId":"9f8db07c561902f4e87b4f697fba19af95","userName":"猪猪侠","userIcon":"e79b588eb2ba6878fce199885dd955e698"},{"commentId":6,"contentId":"0d0135cc4bb62bd14c32594862c57acd2","content":"发表的评论","publishAt":1513910192,"userId":"9f8db07c561902f4e87b4f697fba19af95","userName":"猪猪侠","userIcon":"e79b588eb2ba6878fce199885dd955e698"}],"paging":{"size":2}}}',
                    'debug' => FALSE,
                )
            )
        ),
            
        'Cbn\CommentController@getUserCommentList' => array(// 
            'method' => 'get',
            'route' => '/cbn/getUserCommentList',
            'annotation' => '获取我的评论(M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>60,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'uId' => array('isNull' => FALSE,  'isUUid' => TRUE, 'description'=>'用户id'),
                        'page' => array('isNull' => FALSE, 'value' => array('isNumber')),
                        'size' => array('isNull' => FALSE, 'value' => array('isNumber')),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1514165012,"data":{"data":[{"commentId":7,"contentId":"0d0135cc4bb62bd14c32594862c57acd2","content":"发表的评论","publishAt":1513910302,"userId":"9f8db07c561902f4e87b4f697fba19af95","userName":"猪猪侠","userIcon":"e79b588eb2ba6878fce199885dd955e698"},{"commentId":6,"contentId":"0d0135cc4bb62bd14c32594862c57acd2","content":"发表的评论","publishAt":1513910192,"userId":"9f8db07c561902f4e87b4f697fba19af95","userName":"猪猪侠","userIcon":"e79b588eb2ba6878fce199885dd955e698"}],"paging":{"size":2}}}',
                    'debug' => FALSE,
                )
            )
        ),
            
        'Cbn\CommentController@createComment' => array(// 
            'method' => 'get',
            'route' => '/cbn/createComment',
            'annotation' => '发表评论(M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>60,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'contentId' => array('isNull' => FALSE, 'description'=>'内容id', 'isUUid' => TRUE),
                        'commentId' => array('isUUid' => TRUE,'description'=>'要评论的评论id'),
                        'type' => array('isNull' => FALSE, 'description'=>'1:内容,2:评论','value' => array('1','2')),
                        'content' => array('isNull' => FALSE,'description'=>'内容')
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"Success","timestamp":1479091904}',
                    'debug' => FALSE,
                )
            )
        ),
            
        'Cbn\CommentController@deleteComment' => array(// 
            'method' => 'get',
            'route' => '/cbn/deleteComment',
            'annotation' => '删除评论(M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>60,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'contentId' => array('isNull' => FALSE, 'isUUid' => TRUE,'description'=>'内容id'),
                        'commentId' => array('isNull' => FALSE, 'isUUid' => TRUE, 'description'=>'评论id'),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"Success","timestamp":1479091904}',
                    'debug' => FALSE,
                )
            )
        ),
            
        'Cbn\SystemController@getConfig' => array(
            'method' => 'get',
            'route' => '/cbn/getConfig',
            'annotation' => '获取配置(TV/M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>30,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1515061081,"data":{"aboutUs":"关于我们","qiNiuInfo":{"bucket":"oam4paas","qiNiuHost":"img.paas.onairm.cn","upToken":":MdG00mtuUuCszWSuhrShfCFySAU=:eyJDYWxsYmFja1VybCI6Im15YXBpLmNibi5vbmFpcm0uY25cL29hbVwvcWlOaXUiLCJDYWxsYmFja0JvZHkiOiJrZXk9JChrZXkpJmV0YWc9JChldGFnKSZmbmFtZT0kKGZuYW1lKSZmc2l6ZT0kKGZzaXplKSZtaW1lVHlwZT0kKG1pbWVUeXBlKSZleHQ9JChleHQpJmltYWdlSW5mbz0kKGltYWdlSW5mbykmYXZpbmZvPSQoYXZpbmZvKSIsInNjb3BlIjoib2FtNHBhYXMiLCJkZWFkbGluZSI6MTUxNTA2NDY3OX0="},"versionInfo":{"version":"1.1.11","mustUpdate":0,"description":"21212","url":"122554122"}}}',
                    'debug' => FALSE
                )
            )
        ),
        
        'Cbn\SystemController@createAccuse' => array(// 
            'method' => 'post',
            'route' => '/cbn/createAccuse',
            'annotation' => '用户举报(M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>60,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'itemId' => array('isNull' => FALSE, 'isUUid' => TRUE,'description'=>'1:内容,2:评论'),
                        'type' => array('isNull' => FALSE, 'description'=>'1:内容,2:评论', 'value' => array('1', '2')),
                        'content' => array('isNull' => FALSE,'description'=>'举报内容',  'length' => array('min' => 0, 'max' => 400)),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"Success","timestamp":1479091904}',
                    'debug' => FALSE,
                )
            )
        ),
            
        'Cbn\UserController@getQRcode' => array(// 
            'method' => 'get',
            'route' => '/cbn/getQRcode',
            'annotation' => '获取二维码信息(TV/M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>60,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"Success","timestamp":1479091904}',
                    'debug' => FALSE,
                )
            )
        ),
            
        'Cbn\UserController@getQRcodeStatus' => array(// 
            'method' => 'get',
            'route' => '/cbn/getQRcodeStatus',
            'annotation' => '获取二维码扫码状态(TV/M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>60,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'uniqueId' => array('isNull' => FALSE,'description'=>'唯一字符串'),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"Success","timestamp":1479091904}',
                    'debug' => FALSE,
                )
            )
        ),
            
        'Cbn\UserController@scanQRcode' => array(// 
            'method' => 'post',
            'route' => '/cbn/scanQRcode',
            'annotation' => '手机端扫码(M)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>60,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'uniqueId' => array('isNull' => FALSE,'description'=>'唯一字符串'),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"Success","timestamp":1479091904}',
                    'debug' => FALSE,
                )
            )
        ),
            
        'Cbn\ProgramController@getShareProgramDetail' => array(// 
            'method' => 'get',
            'route' => '/cbn/getShareProgramDetail',
            'annotation' => '电影分享页(web)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 0,
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>60,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'programId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"Success","timestamp":1479091904}',
                    'debug' => FALSE,
                )
            )
        ),
            
        'Cbn\ContentController@getShareContentDetail' => array(// 
            'method' => 'get',
            'route' => '/cbn/getShareContentDetail',
            'annotation' => '内容分享页(web)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 0,
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>60,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'contentId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"Success","timestamp":1479091904}',
                    'debug' => FALSE,
                )
            )
        ),
            
        'Cbn\TopicController@getShareTopicDetail' => array(// 
            'method' => 'get',
            'route' => '/cbn/getShareTopicDetail',
            'annotation' => '话题分享页(web)',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 0,
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>60,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'topicId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"statusCode":0,"message":"Success","timestamp":1479091904}',
                    'debug' => FALSE,
                )
            )
        ),
        'Cbn\ProgramController@TV_TO_APP' => array(
            'method' => '局域网传输',
            'route' => 'TV_TO_APP',
            'annotation' => 'TV和APP信息交互规则',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>10,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'msgType'=> array('isNull' => FALSE,'description'=>'playing:播放时,noplay:未播放'),
                        'from' => array('isNull' => FALSE, 'description'=>'视频来源 1 国广 2 PASS' ,'value' => array('1','2','3','4','5')),
                        'programId'=> array('isNull' => TRUE,'description'=>'第三方平台唯一id'),
                        'programName'=> array('isNull' => FALSE,'description'=>'视频名称'),
                        'keywords' => array('isNull' => FALSE, 'description'=>'多个关键词，逗号隔开', 'length' => array('min' => 2, 'max' => 200)),
                        'img'=> array('isNull' => FALSE,'description'=>'视频截图或者海报'),
                        'programUrl'=> array('isNull' => FALSE,'description'=>'内容'),//
                        'directors'=> array('isNull' => FALSE,'description'=>'导演'),
                        'scriptwriter'=> array('isNull' => FALSE,'description'=>'编剧'),
                        'actors'=> array('isNull' => FALSE,'description'=>'主演'),
                        'area'=> array('isNull' => FALSE,'description'=>'地区'),
                        'type'=> array('isNull' => FALSE,'description'=>'类型'),
                        'publishAt'=> array('isNull' => FALSE,'description'=>'发布时间,时间戳'),
                        'introduction'=> array('isNull' => FALSE,'description'=>'简介'),
                        'curTime'=> array('isNull' => FALSE,'description'=>'当前播放时间秒'),
                        'videoTime'=> array('isNull' => FALSE,'description'=>'总时长秒'),
                    ),
                    'annotation' => '{"字段":"解释"}',
                    'return' => '{"msgType":"playing","data":{"programId": "d1962f80766e7d1532af85f1c91062099","programName": "自取灭亡的完美诠释","programUrl": "","img": "","keywords": "关键词","directors": "李安","scriptwriter": "编剧","actors": "成龙","area": "大陆","type": "剧情","publishAt": "","introduction": "科技","curTime": 0,"videoTime": 0}}',
                    'debug' => FALSE
                )
            )
        ),
        
/**************************************************国广项目结束************************************************************/
         
/**************************************************写入redis实体start************************************************************/     
   'SysController@writeReidsObject' => array(// 获取接口文档
            'method' => 'post',
            'route' => '/writeReidsObject',
            'annotation' => '写入redis实体',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>30,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'table' => array('isNull' => FALSE, 'isUUid' => TRUE,'description'=>'要写入的数据库表'),
                        'id' => array('isNull' => TRUE,'description'=>'特定Id写入,写入所有的不写'),
                    ),
                    'return' => '{"statusCode":0,"message":"Success","timestamp":1479091904}',
                    'debug' => FALSE,
                )
            )
        ),
/**************************************************写入redis实体end************************************************************/
            
/**************************************************公共开始************************************************************/  
        'SysController@sendMessageToApi' => array(//后台发送消息至APi
            'method' => 'post',
            'route' => '/sendMessageToApi',
            'annotation' => '后台发送消息至API',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 0,
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>500,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'message'=>array('isNull' => TRUE,'description'=>'消息json {"type":"database","data":{"table":"user","action":"add","id":"1"}}')
                    ),
                    'return' => '{"statusCode":0,"message":"Success","timestamp":1479091904}',
                    'debug' => FALSE,
                )
            )
        ),
        
        'SysController@qiNiuUpToken' => array(// 获取七牛
            'method' => 'get',
            'route' => '/base/qiNiuUpToken',
            'annotation' => '获取七牛',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>10,//每分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                    ),
                    'return' => '{"statusCode":0,"message":"成功","timestamp":1514188150,"data":{"upToken":":w2LptCGv_zQHWzEwyAs8NVDCaFk=:eyJDYWxsYmFja1VybCI6Im15YXBpLmNibi5vbmFpcm0uY25cL29hbVwvcWlOaXUiLCJDYWxsYmFja0JvZHkiOiJrZXk9JChrZXkpJmV0YWc9JChldGFnKSZmbmFtZT0kKGZuYW1lKSZmc2l6ZT0kKGZzaXplKSZtaW1lVHlwZT0kKG1pbWVUeXBlKSZleHQ9JChleHQpJmltYWdlSW5mbz0kKGltYWdlSW5mbykmYXZpbmZvPSQoYXZpbmZvKSIsInNjb3BlIjoib2FtNHBhYXMiLCJkZWFkbGluZSI6MTUxNDE5MTc1MH0="}}',
                    'debug' => FALSE,
                )
            )
        ),
        'SysController@readLogs' => array(// 读取日志
            'method' => 'get',
            'route' => '/readLogs',
            'annotation' => '读取日志',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>20,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'debug' => array('isNull' => TRUE, 'value' => array('debug')),
                        'page' => array('isNull' => TRUE, 'value' => array('isNumber')),
                        'day' => array('isNull' => TRUE, 'value' => array('isNumber')),
                        'file' => array('isNull' => TRUE, 'value' => array('workman')),
                    ),
                    'return' => '{"statusCode":0,"message":"Success","timestamp":1479091904}',
                    'debug' => FALSE,
                    'ipWithe' => array('127.0.0.1', '36.110.27.82', '219.143.147.4', '219.142.86.67','103.233.54.2'), //白名单
                )
            )
        ),
        
        'SysController@initRouterAndController' => array(// 初始化路由控制器
            'method' => 'post',
            'route' => '/base/initRouterAndController',
            'annotation' => '初始化路由控制器',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 0,
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>30,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
//                        'debug' => array('isNull' => TRUE, 'value' => array('debug')),
//                        'page' => array('isNull' => TRUE, 'value' => array('isNumber')),
//                        'day' => array('isNull' => TRUE, 'value' => array('isNumber')),
//                        'file' => array('isNull' => TRUE, 'value' => array('workman')),
                    ),
                    'return' => '{"statusCode":0,"message":"Success","timestamp":1479091904}',
                    'debug' => FALSE,
                    'ipWithe' => array('127.0.0.1'), //白名单
                )
            )
        ),
        
        'SysController@syncDataFromTv' => array(// 数据统计接口
            'method' => 'get',
            'route' => '/syncDataFromTv',
            'annotation' => '数据统计接口',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 0,
                    'isCheckTn' => TRUE, //是否校验tn
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>600,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'ty' => array('isNull' => FALSE, 'value' => array('a','i','tv','web')),
                        'v' => array('isNull' => FALSE),
                        'dk' => array('isNull' => FALSE),
                        'tn' => array('isNull' => FALSE),
                        'userId' => array('isNull' => FALSE, 'isUUid' => TRUE),
                        'type'=>array('isNull' => FALSE, 'description'=>'1-9', 'value' => array('1','2','3','4','5','6','7','8','9')),
                        'sysV' => array('isNull' => TRUE),
                        'dI' => array('isNull' => TRUE),
                        'ch' => array('isNull' => TRUE),
                    ),
                    'return' => '{"statusCode":0,"message":"Success","timestamp":1479091904}',
                    'debug' => FALSE,
                )
            )
        ),
        
        'SysController@getApiDoc' => array(// 获取接口文档
            'method' => 'get',
            'route' => '/getApiDoc',
            'annotation' => '获取接口文档',
            'version' => array(
                '1.0.0' => '1',
                'max' => '1',
            ),
            'rules' => array(
                '1' => array(
                    'isStrict' => 1,
                    'auth' => 0,
                    'authType' => 1, //1客户端 2web
                    'times'=>30,//1-2分钟单个用户 token+ip 执行次数
                    'parameters' => array(
                        'epgId' => array('isNull' => TRUE, 'value' => array('isNumber')),
                    ),
                    'return' => '{"statusCode":0,"message":"Success","timestamp":1479091904}',
                    'debug' => FALSE,
                )
            )
        ),
        
        
/**************************************************公共结束************************************************************/        

        
	);

}
