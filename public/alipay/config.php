<?php
$config = array (	
		//应用ID,您的APPID。
		'app_id' => "2016082000294950",

		//商户私钥
		'merchant_private_key' => "MIIEpAIBAAKCAQEAsbawrHHVziK60oWOp4dEOXk1GSFr9ufKg/AArVLhY3CG9P5m3VdkHHMk+s24wl7vhzgv2RztM+nj6BABaVevdlMPJX7AzJUkdHej4h7dFvLbqiF6KMlOMyX340H2JcJ94nJ5Y2Spw4PmSekMX5ZSZcUKx52d8XIYlegOm+Y+GzXlsLAuOwe+t+gwfQn9n2sdkiwrBBwNkRifCBACfmNgFIcg2PnIoL1GUqJfk/ewH85aYYqbCLVL1ZKp3A0gkbqfBYQnVoWOboBnU6qrGse0TMxleBDJl3BfXqPfD54Do2t2GjZycuqGPH/KuPI/ODZRpyzxf0VLfREWjEVzQN3LiQIDAQABAoIBADyaTWNxKnUyWL1j+ELfGP/0eowIXrYDU4dst/5jTiNIA23Wt6Nxot0k0w6ISyJVl59oJMEIYUeb0jGYC7MtyNMz/e1ObVcW4SiqiAObfM/Pn+NDqVHxXC6LGx6vps43phAr84NWl6HqAAViaOrL62tzJ3rtIChvPT0ZtqmpebJzOnQdSuF88RcLYmH6FbUP/I48VMbQ1TeQZ8BedgXOC5KiIM+SFUHLT1flQGyTokf2mYJpXUm13WiJtenN6WiV/bUYWOulwfAv7yFRCJfJCUbFPlxKTXvw9qRSvi7HFt4d+5AYkp98DtgwKktcCd5H6zRumbxCzqAdK1Tbc7CNTHUCgYEA+pLTgt3W+eMA76up+ii3t7S8U3jf9PwbjR9sZh9m06ORLgjOquTtuOJcyw0K/D+5NYErxJH9/D8n+ZoJhaKFI6gf9M7WAjV0DoBdOq76XEnlYq74Q66Qk1QLz4+lMCMjpZ7Vtb6VKkzZYmna/AYfdJMcRbiwoefe7Hqig+P3cnsCgYEAtY/uKSN7556dQ0DqQ2nObH6jtV97gjNmNWXY81222qszGYt28qphuUXtuCcpvwCde7yoA3TtaOuvDl/1YzaZ5sYqnVstOLI3jN9Z1YiLzLDNCqvyk4+RkQkCQySYWQOfC5NZ1UwCFH2bf0IQKHIFKRw++AIMC0q4XKb/Az+EzMsCgYEAkJ+iGMx1wfD1n1+TUeGieBOHBx6jFMFWHZPfeunw7YJgzxMiRZywYJ+2MzQp/QHColEVs/UXhTnmlrgDevAcrEHjf+bJuqijfaM1LCPGOPIlxVl7zYS5yW8fCgO8shX2Ayez7Zw8eBe2+IhkbkYSbbRjAxLgT/KGxjLaDgnLznUCgYB1XItLZleCegIX5g9H8k3vzdCOviC9IfJwk9wwPCSlpsKKQZ9q7v3qYCxwvEf84i3qPSGx0lRgXPC0bwzUzAehp/kF7FkH7BOCaSHNqM0NTD/OJSXRgp1KzjL7Kd2ctH2LYv1Ppik2C5eHJDIDouh5EXwWnmQgrHUN1xjChb2XDwKBgQDz7uZmeSEViCWQGxM4lk22xEd/6fBy4fbDJ3DS41s7rNDSNVWl88r3Le8s85zlgeiK1xvVS6Bn8/6OeHHJhJEoPbcvNG4/fN9QUw57XHdwiigq+SV7vlpHvSa/mUwj3DRxq6WidyY/G7lzOgoXwXC84uG+ShI5l3RTJBvq6pzSjg==",
		
		//异步通知地址
		'notify_url' => "http://t.api.ray.onairm.cn/alipay/notify_url.php",
		
		//同步跳转
		'return_url' => "http://t.api.ray.onairm.cn/alipay/return_url.php",

		//编码格式
		'charset' => "UTF-8",

		//签名方式
		'sign_type'=>"RSA2",

		//支付宝网关
		'gatewayUrl' => "https://openapi.alipaydev.com/gateway.do",

		//支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
		'alipay_public_key' => "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsbawrHHVziK60oWOp4dEOXk1GSFr9ufKg/AArVLhY3CG9P5m3VdkHHMk+s24wl7vhzgv2RztM+nj6BABaVevdlMPJX7AzJUkdHej4h7dFvLbqiF6KMlOMyX340H2JcJ94nJ5Y2Spw4PmSekMX5ZSZcUKx52d8XIYlegOm+Y+GzXlsLAuOwe+t+gwfQn9n2sdkiwrBBwNkRifCBACfmNgFIcg2PnIoL1GUqJfk/ewH85aYYqbCLVL1ZKp3A0gkbqfBYQnVoWOboBnU6qrGse0TMxleBDJl3BfXqPfD54Do2t2GjZycuqGPH/KuPI/ODZRpyzxf0VLfREWjEVzQN3LiQIDAQAB",
);