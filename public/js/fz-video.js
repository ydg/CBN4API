var FZFVIDEO =
    '<div id="fz-videoBox">' +
    '<div id="videoWrapper">' +
    '<canvas style="width:  100%;height:  100%;"></canvas>' +
    '<video src="" poster="" id="fz-videoAct" x-webkit-airplay="allow" webkit-playsinline playsinline controls="controls" controlsList="nodownload" ></video>' +
    '</div>' +
    '<div id="replay_wrapper">' +
    '<div id="replay">' +
    '<img src="img/pause.png"/>'+
    '<p>重新播放</p>'+
    '</div>' +
    '</div>' +
    '</div>';

function createVideo(dn, obj) {
    //节点池
    var fzPools = {}

    //点击后停止现实缓冲动画标记
    var hcqStop = 0;

    //标记播放过程中点击video容器后长时间无操作状态
    var flag = 0;

    //获取节点
    this.D = function (dms) {
        var dom_ = document.getElementById(dms);
        return dom_;
    }

    //设置播放器地址
    this.setUrl = function (url) {
        this.D('fz-videoAct').src = url;
    }

    //设置poster
    this.setPoster = function (poster) {
        this.D('fz-videoAct').poster = poster;
    }


    //初始化播放器属性
    this.init = function () {
        //设置播放器地址
        this.setUrl(obj.url);
        //设置poster
        this.setPoster(obj.poster);

        //判断是否自动播放
        if (obj.autoplay) {
            this.videoDOM.autoplay = true;
            this.videoDOM.play();
        } else {
            this.videoDOM.autoplay = false;
        }




        //播放结束
        fzPools.VIDEO_DOM.onended=function(){
            this.pause();
            $('#currentTimeBox,#fullscreen,#playOrStop').hide();
            $("#replay_wrapper").show();
        }
        fzPools.replay.onclick=function () {
            console.log(fzPools.VIDEO_DOM.currentTime );
            $("#replay_wrapper").hide();
            fzPools.VIDEO_DOM.currentTime =0;
            fzPools.VIDEO_DOM.play();
            console.log("2"+fzPools.VIDEO_DOM.currentTime );
        }


    }

    //创建播放器
    this.createDom = function () {
        if (this.D("fz-videoBox") == null) {
            this.D(dn).innerHTML = FZFVIDEO;
            this.videoDOM = this.D("fz-videoAct");
            saveDom();
            this.init();
        } else {
            console.log("播放器节点已存在");
            return;
        }
    }

    //销毁实例
    this.overVideo = function () {
        var ovDom = document.getElementById(dn);
        var childs = ovDom.childNodes;
        var length = childs.length;
        for (var i = 0; i < length; i++) {
            ovDom.removeChild(childs[0]);
        }
        FZ_VIDEO = '';
        $('#fz-videoBox').remove();
    }
    /*--------------------------------------方法-------------------------------------*/
    //将节点存入节点池
    function saveDom() {
        fzPools.VIDEO_DOM = document.getElementById("fz-videoAct");
        fzPools.bfBtn = document.getElementById("bfBtn");
        fzPools.sptxt = document.getElementById("sptxt");
        fzPools.replay = document.getElementById("replay");
        fzPools.items = document.getElementsByClassName("item_Poster");
        fzPools.bs1 = document.getElementById("bs1");
        fzPools.bs2 = document.getElementById("bs2");
        fzPools.bs3 = document.getElementById("bs3");
        fzPools.bs4 = document.getElementById("bs4");
    }


    //打开或关闭加载
    function openLoading(states, msg) {
        if (states == "on") {
            fzPools.bfBtn.style.visibility = "visible";
            fzPools.bfBtn.style.opacity = "1";
            fzPools.sptxt.innerHTML = msg;
        } else {
            fzPools.bfBtn.style.visibility = "hidden";
            fzPools.bfBtn.style.opacity = "0";
            fzPools.sptxt.innerHTML = '';
        }
    }

    this.createDom();
}
