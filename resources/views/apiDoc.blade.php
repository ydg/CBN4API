<!DOCTYPE html>
<html>    
    <head>
        <title>彩云之端-在线API文档</title>
        <link href="/style/github-bf51422f4bb36427d391e4b75a1daa083c2d840e.css" media="all" rel="stylesheet" type="text/css"/>
        <link href="/style/github2-d731afd4f624c99a4b19ad69f3083cd6d02b81d5.css" media="all" rel="stylesheet" type="text/css"/>
        <link href="/css/zTreeStyle/zTreeStyle.css" media="all" rel="stylesheet" type="text/css"/>
        <style>
            pre {
                counter-reset: line-numbering;
                border: solid 1px #d9d9d9;
                border-radius: 0;
                background: #fff;
                padding: 0;
                line-height: 23px;
                margin-bottom: 30px;
                white-space: pre;
                overflow-x: auto;
                word-break: inherit;
                word-wrap: inherit;
            }

            pre a::before {
                content: counter(line-numbering);
                counter-increment: line-numbering;
                padding-right: 1em; /* space after numbers */
                width: 25px;
                text-align: right;
                opacity: 0.7;
                display: inline-block;
                color: #aaa;
                background: #eee;
                margin-right: 16px;
                padding: 2px 10px;
                font-size: 13px;
                -webkit-touch-callout: none;
                -webkit-user-select: none;
                -khtml-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }

            pre a:first-of-type::before {
                padding-top: 10px;
            }

            pre a:last-of-type::before {
                padding-bottom: 10px;
            }

            pre a:only-of-type::before {
                padding: 10px;
            }

            .highlight { background-color: #ffffcc } /* RIGHT */
        </style>
        
        <script>
            function syntaxHighlight(json) {
                json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
                return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
                    var cls = 'number';
                    if (/^"/.test(match)) {
                        if (/:$/.test(match)) {
                            cls = 'key';
                        } else {
                            cls = 'string';
                        }
                    } else if (/true|false/.test(match)) {
                        cls = 'boolean';
                    } else if (/null/.test(match)) {
                        cls = 'null';
                    }
                    return '<span class="' + cls + '">' + match + '</span>';
                });
            }
        </script>
    </head>
    <body>
        <div>
            <div style='width:25%;'>
                <ul id="tree" class="ztree" style='width:100%'>
                </ul>
            </div>

            <div id='readme' style='width:70%;margin-left:20%;'>
                <article class='markdown-body'>
                    <h1 id="-">彩云之端-在线API文档</h1>
                    <h2 id="-">项目介绍</h2>
                    <pre><blockquote>{{$information}}</blockquote></pre>
                    <h2 id="-">基础说明</h2>
                    <h3 id="-">Host说明</h3>
                    <pre><blockquote>测试HOST:  {{env('HOST_TEST')}} </blockquote><blockquote>线上HOST:  {{env('HOST_ONLINE')}} </blockquote></pre>
                    <h3 id="-">基础参数</h3>
                    <pre><blockquote>ty     String   //系统类型 a:android i:ios tv:电视端 web:网站</blockquote><blockquote>v      String   //软件版本号  1.0.0</blockquote><blockquote>dk     String   //设备的唯一标示</blockquote><blockquote>tn     String   //请求</blockquote><blockquote>userId String   //若用户登录时传入用户id，若没有登陆时项目前缀‘XXXX’+ 随机16位字符 </blockquote></pre>
                    <pre><blockquote>注：tn = 登陆时获取的用户token／随机16位字符包含数字 + md5(userId+ty+v+dk,32)，注意顺序不能改变，这样做的目的是为了每次接口都先校验请求的合法性。</blockquote><blockquote>例如：ty=tv&userId=#ray35ecb0dac7959d0f&dk=d29082835ecb0dac&v=1.0.0&tn=11128012923070a5555bb5af1e7aa031c014</blockquote></pre>
                    <h3 id="-">数据格式</h3>
                    <pre><code class="lang-json">{
    "statusCode": 0,
    "message": "成功", 
    "size": 10, //返回size数  注：if(size(请求时的size) > size(返回时的size)){ 加载完成 }else{ 未加载完成 }
    "timestamp": 12313123123,
    "data": {
    }
}</code></pre>
                    <h2 id="-">接口说明</h2>
                    @foreach($routeList as $key=>$route)
                    <h3 id="-">{{$route['annotation']}}</h3>
                    <h4 id="-">地址</h4>
                    <p><a href="{{$route['route']}}" target="blank">http://{{$_SERVER['HTTP_HOST']}}{{$route['route']}}</a></p>
                    <h4 id="-">请求</h4>
                    <ul>
                        <li>请求方式:{{$route['method']}}</li>
                        <li>
                        @if(isset($route['rules']['1']['isStrict']))
                        校验参数:
                            @if($route['rules']['1']['isStrict'] == '0')
                            不严格(可自己增加参数)
                            @elseif($route['rules']['1']['isStrict'] == '1')
                            严格(可以减少参数)
                            @elseif($route['rules']['1']['isStrict'] == '2')
                            非常严格(参数个数不能变)
                            @endif
                        @else
                            不严格(可自己增加参数)
                        @endif
                        </li>
                        <li>请求参数:</li>
                    </ul>
                    <table>
                        <thead>
                            <tr>
                                <th style="text-align:left">名称</th>
                                <th style="text-align:left">必填</th>
                                <th style="text-align:left">类型</th>
                                <th style="text-align:left">取值范围</th>
                                <th style="text-align:left">说明</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($route['rules']['1']['parameters']))
                            @foreach($route['rules']['1']['parameters'] as $key => $parameter)
                            <tr>
                                <td style="text-align:left">{{$key}}</td>
                                @if(isset($parameter['isNull']) && $parameter['isNull'])
                                    <td style="text-align:left">否</td>
                                @else
                                    <td style="text-align:left">是</td>
                                @endif
                                
                                @if(isset($parameter['value'][0]))
                                    @if($parameter['value'][0] == 'isNumber')
                                        <td style="text-align:left">number</td>
                                    @elseif($parameter['value'][0])
                                        <td style="text-align:left">string</td>
                                    @endif
                                @else
                                    <td style="text-align:left">string</td>
                                @endif
                                <td style="text-align:left">
                                @if(isset($parameter['value'][0]))
                                    @if($parameter['value'][0] == 'isNumber')
                                        @if(isset($parameter['length']))
                                        大小限制:{{$parameter['length']['min']}}～{{$parameter['length']['max']}}
                                        @endif
                                    @elseif($parameter['value'][0] == 'isEmail')
                                        邮箱地址
                                    @elseif($parameter['value'][0] == 'isPhone')
                                        手机号码
                                    @elseif($parameter['value'][0] == 'isFloat')
                                        浮点型
                                    @elseif(count($parameter['value'][0]) > 0)
                                        {{implode('、',$parameter['value'])}}
                                    @endif
                                @else
                                    @if(isset($parameter['length']))
                                        长度限制:{{$parameter['length']['min']}}～{{$parameter['length']['max'] }}
                                    @endif
                                @endif
                                </td>
                                <td style="text-align:left">
                                @if($key == 'ty'|| $key == 'v'||$key == 'dk'||$key == 'tn'|| $key == 'userId')
                                    见基础参数
                                @elseif($key == 'page')
                                    请求页码(从0开始)
                                @elseif($key == 'size')
                                    每页条数
                                @elseif($key == 'keywords')
                                    关键词(多个情况用逗号分隔)
                                @elseif(isset($parameter['description']))
                                    {{$parameter['description']}}
                                @else
                                    无
                                @endif
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                    <h4 id="-">注释</h4>
                    @if(isset($route['rules']['1']['annotation']))
                    <pre><code class="lang-json"><script type="text/javascript" > 
                        var str = '{{$route['rules']['1']['annotation']}}';
                        str = str.replace(/&quot;/g,'"');
                        var obj = JSON.parse(str);
                        str = JSON.stringify(obj, undefined, 4);
                        document.write(syntaxHighlight(str)); 
                        </script></code></pre>
                    @endif
                    <h4 id="-">响应</h4>
                    @if(isset($route['rules']['1']['return']))
                    <pre><code class="lang-json"><script type="text/javascript" > 
                        var str = '{{$route['rules']['1']['return']}}';
                        str = str.replace(/&quot;/g,'"');
                        var obj = JSON.parse(str);
                        str = JSON.stringify(obj, undefined, 4);
                        document.write(syntaxHighlight(str)); 
                        </script></code></pre>
                    @endif
                    <h4 id="-">缓存</h4>
                    @if(isset($route['rules']['1']['cache']['name']))
                    <pre><blockquote>缓存NAME:  {{$route['rules']['1']['cache']['name']}} </blockquote><blockquote>缓存参数:   {{implode('、',$route['rules']['1']['cache']['par'])}}  </blockquote></pre>
                    @endif
                    @endforeach
                    <h2 id="-">状态码</h2>
                    <table>
                        <thead>
                            <tr>
                                <th style="text-align:left">返回码</th>
                                <th style="text-align:left">中文说明(en)</th>
                                <th style="text-align:left">英文说明(zh-CN)</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($retrunCode as $key=>$value)
                            <tr>
                                <td style="text-align:left">{{$value['code']}}</td>
                                <td style="text-align:left">{{$value['name']['zh-CN']}}</td>
                                <td style="text-align:left">{{$value['name']['en']}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <h2 id="-">修改记录</h2>
                    @foreach($modificationRecord as $key=>$records)
                    <h3 id="-">{{$key}}</h3>
                    <pre>@foreach($records as $key1=>$record)<blockquote>{{$key1+1}}:{{$record}}</blockquote>@endforeach</pre>
                    @endforeach
                    <!--<h2 id="-">在此留言</h2>-->
                </article>

            </div>

        </div>
    </body>
</html>
<script type="text/javascript" src="/js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="/js/jquery.ztree.all-3.5.min.js"></script>
<script type="text/javascript" src="/js/ztree_toc.js"></script>
<script type="text/javascript" src="/js/toc_conf.js"></script>

<SCRIPT type="text/javascript" >
    $(document).ready(function () {
        var css_conf = eval(markdown_panel_style);
        $('#readme').css(css_conf)

        var conf = eval(jquery_ztree_toc_opts);
        $('#tree').ztree_toc(conf);
    });
    
    

    
    
</SCRIPT>