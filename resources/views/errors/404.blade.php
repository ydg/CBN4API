<!DOCTYPE html>
<html>    
   <head>
      <title>404页面</title>
      <style>
         html, body {
            height: 100%;
         }
         body {
            margin: 0;
            padding: 0;
            width: 100%;
            color: #B0BEC5;
            display: table;
            font-weight: 100;
         }
         .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
         }
         .content {
            text-align: center;
            display: inline-block;
         }
         .title {
            font-size: 72px;
            margin-bottom: 40px;
         }
         .home {
                padding-left:30px; background: url(images/img/index/homeicon.png); width: 66px
        }
        .c2 {
                height: 35px; text-align: center
        }
        .c2 A {
                display: inline-block; font-size: 14px; margin: 0px 4px; color: #626262; padding-top: 1px; height: 23px; text-align: left; text-decoration: none
        }
        .c2 A:hover {
                color: #626262; text-decoration: none
        }
        .c2 A.home {
                padding-left: 30px; background: url(images/img/index/homeicon.png); width: 66px
        }
        .c2 A.home:hover {
                background: url(images/img/index/homeicon.png) 0px -24px
        }
        .c2 A.home:active {
                background: url(images/img/index/homeicon.png) 0px -48px
        }
      </style>		
   </head>
   <body>
      <div class = "container">
         <div class = "content">
            <div class = "title">404 Not Found</div>
            <div class="c2">
                <a class="home" href="/">返回首页</a>
            </div>
         </div>
      </div>
		
   </body>
</html>