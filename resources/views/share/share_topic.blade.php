<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>分享页-话题</title>
    <link rel="stylesheet" href="/css/mobile-reset.css">
    <link rel="stylesheet" href="/css/commen.css?v0.0.2">
    <link rel="stylesheet" href="/css/fz-video.css?v0.0.2">
    <link rel="stylesheet" href="/css/share_topic.css?v0.0.2">
     <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico" />
    <script src="/js/base.js"></script>
    <script src="/js/jquery-1.12.3.min.js"></script>
    <script src="/js/fz-video.js?v0.0.2"></script>
</head>
<body>
<!--头部下载提示-->
<div class="top">
    <div class="top_logo float-left">
        <div class="logo_img">
            <img src="img/logo.png" class="logo_img_left"/>
            <img src="img/logoText.png" class="logo_img_right"/>
        </div>
    </div>
    <a class="top_download float-right" href="javascript:;" onclick="skip('downLoad',id,data)">下载</a>
</div>
<!--话题信息-->
<div class="topic_info clearfix">
    <h3>{{$topic['title']}}</h3>
    <div class="info_div">
        <div class="text float-left">
        <p class="comment_text">{{$topic['introduction']}}</p>
        @if($topic['userName'])<p class="creater">创建者 @<span>{{$topic['userName']}} </span></p>@endif
        </div>
        <div class="icon float-right">
            <img src="{!!show_img($topic['img'])!!}" alt="">
        </div>
    </div>
</div>
<!--话题评论内容-->
@if(count($contents) > 0)
<div class="topic_content">
    <ul>
        @foreach($contents as $content)
        <li >
            <div class="item_poster">
                <div class="poster_mark">
                    <div class="item_title" onclick="skip('topic',{{$content['topicName']}},data)">
                        <p>#{{$content['topicName']}}</p>
                    </div>
                    <div class="video_center">
                        <img class="played" src="/img/pause.png"/>
                        <p class="duration">{!!str_pad(floor(($content['endTime']-$content['startTime'])/60),2,'0',0) !!}:{!!str_pad(($content['endTime']-$content['startTime'])%60,2,'0',0) !!}</p>
                    </div>
                    <div class="item_src ">
                        <div class="src-left clearfix" onclick="skip('homePage',id,data)">
                            <img class="user_icon float-left" src="/img/gray.jpg" alt="">
                            <div class="float-left user_name">
                                <p>{{$content['userName']}}</p>
                            </div>
                        </div>
                        <div class="src_name">
                            <a  href="javascript:;" onclick="skip('src',{{$content['programName']}},data)">视频来自《{{$content['programName']}}》</a>
                        </div>
                    </div>
                </div>
                <img class="video_poster" src="{!!show_img($content['contentImg'])!!}" alt="">
                <div class="video_container" id="testBox" data-video-url="{!!show_img($content['programUrl'])!!}">
                </div>
            </div>
        </li>
        @endforeach
    </ul>
</div>
@endif
<script src="/js/jweixin-1.2.0.js"></script>
<script>
       //微信分享
    $(function () {
        var wx_appId ;
        var wx_timestamp ;
        var wx_nonceStr ;
        var wx_signature ;
        var url = '/getWxJsConfig';
        jQuery.ajax({
            url:url,
            data:{url :window.location.href},
            type:"GET",
            async:false,
            beforeSend:function(){
            },
            success:function(returnJson){
                var data = JSON.parse(returnJson);
                if(data.statusCode === 0){
                   wx_appId = data.data.appId;
                   wx_timestamp = data.data.timestamp;
                   wx_nonceStr = data.data.nonceStr;
                   wx_signature = data.data.signature;
                }else{

                }
            },
            error:function(){

            }
        });
        
        /*
        * 微信config
        */
        wx.config({
          debug: false,
          appId: wx_appId,
          timestamp: wx_timestamp,
          nonceStr: wx_nonceStr,
          signature: wx_signature,
          jsApiList: [
            'checkJsApi',
            'onMenuShareTimeline',
            'onMenuShareAppMessage',
            'onMenuShareQQ',
            'onMenuShareWeibo',
            'onMenuShareQZone',
          ]
        });
        
        window.shareData = {
            "url": "{{$shareUrl}}",
            "title":'{!!addslashes($shareTitle)!!}',
            "description": "{!!addslashes($shareDescription)!!}",
            "iconUrl": "{{$shareIcon}}"
        };
        document.addEventListener('WeixinJSBridgeReady', function onBridgeReady() {
            // 发送给好友
            WeixinJSBridge.on('menu:share:appmessage', function (argv) {
                WeixinJSBridge.invoke('sendAppMessage', {
                    "img_url": window.shareData.iconUrl,
                    "img_width": "401",
                    "img_height": "275",
                    "link": window.shareData.url,
                    "desc": window.shareData.description,
                    "title": window.shareData.title
                }, function (res) {
                    _report('send_msg', res.err_msg);
                });
            });
            // 分享到朋友圈
            WeixinJSBridge.on('menu:share:timeline', function (argv) {
                WeixinJSBridge.invoke('shareTimeline', {
                    "img_url": window.shareData.iconUrl,
                    "img_width": "401",
                    "img_height": "275",
                    "link": window.shareData.url,
                    "desc": window.shareData.description,
                    "title": window.shareData.title
                }, function (res) {
                    _report('send_msg', res.err_msg);
                });
            });
              // 分享到微博
            WeixinJSBridge.on('menu:share:weibo', function (argv) {
                WeixinJSBridge.invoke('shareWeibo', {
                    "img_url": window.shareData.iconUrl,
                    "img_width": "401",
                    "img_height": "275",
                    "link": window.shareData.url,
                    "desc": window.shareData.description,
                    "title": window.shareData.title
                }, function (res) {
                    _report('send_msg', res.err_msg);
                });
            });
            // 分享到QQ
            WeixinJSBridge.on('menu:share:qq', function(argv){
                WeixinJSBridge.invoke('shareQQ',{
                    "img_url": window.shareData.iconUrl,
                    "img_width": "401",
                    "img_height": "275",
                    "link": window.shareData.url,
                    "desc": window.shareData.description,
                    "title": window.shareData.title
                }, function (res) {
                    _report('send_msg', res.err_msg);
                });
            });
        }, false)
    });
    var videoPlayer = null;
    $('.topic_content > ul > li > .item_poster').each(function(){
        $(this).click(function(){
            if($(this).find('video').length > 0){
                return ;
            }
            if(videoPlayer){
                videoPlayer.overVideo();
            }
            var container = $(this).find('.video_container').first();
            var id = container.attr('id');
            var url = container.attr('data-video-url');
            var poster = container.attr('data-video-poster');
            videoPlayer = new createVideo(
                id,	//容器的id
                {
                    url 		: url, 	//视频地址
                    autoplay	: true, //是否自动播放
                    poster      : poster
                }
            );

        });
    });
//分享页面跳转函数
//    href代表要跳转到的页面，其中具体如下：
//          "downLoad"跳转到下载页面
//          "homePage"跳转到用户主页
//          "topic"跳转到话题页
//          "src"跳转到资源页
//          "detail"跳转到详情页
//    id代表对应元素的id,如用户id,话题id,节目id等
    var data="",id="";
    function skip(href,id,data) {
        switch (href){
            case "downLoad":
                window.open('url');
                break;
            case "homePage":

                break;
            case "topic":

                break;
            case "src":

                break;
            case "detail":

                break;
            default:

                break;
        }
    }


</script>
</body>
</html>