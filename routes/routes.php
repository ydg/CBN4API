<?php 
/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It is a breeze. Simply tell Lumen the URIs it should respond to
  | and give it the Closure to call when that URI is requested.
  |
 */

//$app->get('/', function () use ($app) {
//    return $app->version();
//});
//测试观察者
$app->get('/test/observer', 'TestController@testNotify');
//获取lumen日志
$app->get('/', function () use ($app) {
    echo 'Welcome to BeiJing !';
    exit();
});

//post 七牛回调
$app->post('/oam/qiNiu', 'SysController@qiNiu');

/**全局无参数校验路由组**/

/* * **********************************公共项目接口************************************************* */
$app->group(['middleware' => ['before', 'after']], function () use ($app) {
    
    //获取微信JsConfig
    $app->get('/getWxJsConfig', 'SysController@getWxJsConfig');
    //后台给API发送消息
    $app->post('/sendMessageToApi', 'SysController@sendMessageToApi');
    //上传log日志
    $app->get('/syncDataFromTv', 'SysController@syncDataFromTv');
    //获取lumen日志
    $app->get('/readLogs', 'SysController@readLogs');
    //七牛token
    $app->get('/base/qiNiuUpToken', 'SysController@qiNiuUpToken');
    //获取uuid
    $app->get('/base/getUuid', 'SysController@getUuid');
    //前端测试
    $app->get('/base/toastMessage', 'SysController@toastMessage');
    //生成路由控制器接口
    $app->get('/base/initRouterAndController', 'SysController@initRouterAndController');
    //获取接口文档
    $app->get('/getApiDoc','SysController@getApiDoc');
    //官网收集信息
    $app->post('/feedback','SysController@feedback');
/**公共项目路由组**/
    
}
);

/* * *********************************国广项目 开始************************************************* */
$app->group(['middleware' => ['before', 'after']], function () use ($app) {
    
    //同步用户信息(TV)
    $app->post('/cbn/syncUser','Cbn\UserController@syncUser');
    //是否关注(TV/Mobile)
    $app->get('/cbn/isFollow','Cbn\UserController@isFollow');
    //关注人(TV/Mobile)
    $app->post('/cbn/follow','Cbn\UserController@follow');
    //取消关注人(TV/Mobile)
    $app->post('/cbn/deleteFollow','Cbn\UserController@deleteFollow');
    //取消全部关注的人(TV/Mobile)
    $app->post('/cbn/deleteAllFollow','Cbn\UserController@deleteAllFollow');
    //关注电影话题(Mobile)
    $app->post('/cbn/attention','Cbn\UserController@attention');
    //取消关注电影话题(Mobile)
    $app->post('/cbn/deleteAttention','Cbn\UserController@deleteAttention');
    //取消全部关注电影话题(Mobile)
    $app->post('/cbn/deleteAllAttention','Cbn\UserController@deleteALLAttention');
    //是否收藏(TV/Mobile)
    $app->get('/cbn/isFavarite','Cbn\UserController@isFavarite');
    //收藏(TV/Mobile)
    $app->post('/cbn/favarite','Cbn\UserController@favarite');
    //取消收藏(TV/Mobile)
    $app->post('/cbn/deleteFavarite','Cbn\UserController@deleteFavarite');
    //取消全部收藏(TV/Mobile)
    $app->post('/cbn/deleteAllFavarite','Cbn\UserController@deleteAllFavarite');
    //获取用户关注的列表(TV/Mobile)
    $app->get('/cbn/getAttentionList','Cbn\UserController@getAttentionList');
    //获取用户收藏的列表(TV/Mobile)
    $app->get('/cbn/getFavariteList','Cbn\UserController@getFavariteList');
    //获取用户的内容列表(包含用户信息)(TV/Mobile)
    $app->get('/cbn/getUserContentList','Cbn\ContentController@getUserContentList');
    //获取分类(TV)
    $app->get('/cbn/getCategory','Cbn\TopicController@getCategory');
    //获取分类(TV)为TV增加的接口
    $app->get('/cbn/v2/getCategory','Cbn\TopicController@getCategoryV2');
    //获取分类下的内容列表(TV)
    $app->get('/cbn/getContentList','Cbn\ContentController@getContentList');
    //同步视频节目(TV)
    $app->post('/cbn/syncProgram','Cbn\ProgramController@syncProgram');
    //发布内容(TV/Mobile)
    $app->post('/cbn/createContent','Cbn\ContentController@createContent');
    //获取话题(TV/Mobile)
    $app->get('/cbn/getTopic','Cbn\TopicController@getTopic');
    //获取节目详情页的话题列表(TV/Mobile)
    $app->get('/cbn/getProgramTopicList','Cbn\TopicController@getProgramTopicList');
    //获取配置(TV/Mobile)
    $app->get('/cbn/getConfig','Cbn\SystemController@getConfig');
    //获取用户关注人的列表(TV/Mobile)
    $app->get('/cbn/getFollowerList','Cbn\UserController@getFollowerList');
    //获取用户粉丝的列表(TV/Mobile)
    $app->get('/cbn/getFansList','Cbn\UserController@getFansList');
    //获取关注人们的内容列表(TV/Mobile)
    $app->get('/cbn/getFollowerContentList','Cbn\ContentController@getFollowerContentList');
    
    //登录(Mobile)
    $app->post('/cbn/login','Cbn\UserController@login');
    //登出(Mobile)
    $app->post('/cbn/logout','Cbn\UserController@logout');
    //获取验证码(Mobile)
    $app->get('/cbn/verifyCode','Cbn\UserController@verifyCode');
    //更新用户信息(Mobile)
    $app->post('/cbn/updateUserInfo','Cbn\UserController@updateUserInfo');
    //获取用户信息(Mobile)
    $app->get('/cbn/getUserInfo','Cbn\UserController@getUserInfo');
    //获取关注的电影(Mobile)
//    $app->get('/cbn/getUserProgramList','Cbn\ProgramController@getUserProgramList');
    //获取推荐下内容列表(Mobile)
    $app->get('/cbn/getRecommendContentList','Cbn\ContentController@getRecommendContentList');
    //获取推荐话题(发现和搜索页)(Mobile)
    $app->get('/cbn/getRecommendItemList','Cbn\TopicController@getRecommendItemList');
    //获取全部话题(带分类)(Mobile)
    $app->get('/cbn/getAllTopic','Cbn\TopicController@getAllTopic');
    //获取搜索结果(话题.用户.视频.内容)(Mobile)
    $app->get('/cbn/getSearchList','Cbn\ContentController@getSearchList');
    //获取话题详情(Mobile)
    $app->get('/cbn/getTopicDetail','Cbn\TopicController@getTopicDetail');
    //获取话题下的内容(不包含话题详情)(Mobile)
    $app->get('/cbn/getTopicContentList','Cbn\ContentController@getTopicContentList');
    //获取电影详情(Mobile)
    $app->get('/cbn/getProgramDetail','Cbn\ProgramController@getProgramDetail');
    //获取电影下的内容(不包含电影详情)(Mobile)
    $app->get('/cbn/getProgramContentList','Cbn\ContentController@getProgramContentList');
    //获取内容下的评论(Mobile)
    $app->get('/cbn/getContentCommentList','Cbn\CommentController@getContentCommentList');
    //获取用户的评论(Mobile)
    $app->get('/cbn/getUserCommentList','Cbn\CommentController@getUserCommentList');
    //获取内容详情(Mobile)
    $app->get('/cbn/getContentDetail','Cbn\ContentController@getContentDetail');
    //内容评论点赞(Mobile)
    $app->post('/cbn/createPraise','Cbn\ContentController@createPraise');
    //内容评论取消点赞(Mobile)
    $app->post('/cbn/cancelPraise','Cbn\ContentController@cancelPraise');
    //发表评论(Mobile)
    $app->post('/cbn/createComment','Cbn\CommentController@createComment');
    //删除评论(Mobile)
    $app->post('/cbn/deleteComment','Cbn\CommentController@deleteComment');
    //举报(Mobile)
    $app->post('/cbn/createAccuse','Cbn\SystemController@createAccuse');
    //获取二维码信息(TV/M)
    $app->get('/cbn/getQRcode','Cbn\UserController@getQRcode');
    //获取二维码扫码状态(TV/M)
    $app->get('/cbn/getQRcodeStatus','Cbn\UserController@getQRcodeStatus');
    //移动端扫码(M)
    $app->post('/cbn/scanQRcode','Cbn\UserController@scanQRcode');
    //内容分享页
    $app->get('/cbn/getShareContentDetail','Cbn\ContentController@getShareContentDetail');
    //电影分享页
    $app->get('/cbn/getShareProgramDetail','Cbn\ProgramController@getShareProgramDetail');
    //话题分享页
    $app->get('/cbn/getShareTopicDetail','Cbn\TopicController@getShareTopicDetail');
    
/**国广项目路由组**/
}
);
/************************************国广项目 结束**************************************************/
